'use strict';
//Recording interval in milliseconds
var timeInterval = 20000;
var mediaSource = new MediaSource();
mediaSource.addEventListener('sourceopen', handleSourceOpen, false);

var mediaRecorder;
var recordedBlobs;
var sourceBuffer;

var gumVideo = document.querySelector('video#gum');
var recordedVideo = document.querySelector('video#recorded');

var recordButton = document.querySelector('button#record');
var playButton = document.querySelector('button#play');
var downloadButton = document.querySelector('button#download');
recordButton.onclick = toggleRecording;
playButton.onclick = play;
downloadButton.onclick = download;
recordedVideo.style.display="none";

// window.isSecureContext could be used for Chrome
var isSecureOrigin = location.protocol === 'https:' ||
location.host === 'localhost';
if (!isSecureOrigin) {
  alert('getUserMedia() must be run from a secure origin: HTTPS or localhost.' +
    '\n\nChanging protocol to HTTPS');
  location.protocol = 'HTTPS';
}

// Use old-style gUM to avoid requirement to enable the
// Enable experimental Web Platform features flag in Chrome 49

navigator.getUserMedia = navigator.getUserMedia ||
  navigator.webkitGetUserMedia || navigator.mozGetUserMedia;

var constraints = {
  audio: true
  //video: false
};

//navigator.getUserMedia(constraints, successCallback, errorCallback);


function successCallback(stream) {
  console.log('getUserMedia() got stream: ', stream);
  window.stream = stream;
  if (window.URL) {
    gumVideo.src = window.URL.createObjectURL(stream);
  } else {
    gumVideo.src = stream;
  }
////  startRecording();
}

function errorCallback(error) {
  console.log('navigator.getUserMedia error: ', error);
}

function handleSourceOpen(event) {
  console.log('MediaSource opened');
  sourceBuffer = mediaSource.addSourceBuffer('audio/wav; codecs="vp8"');
  console.log('Source buffer: ', sourceBuffer);
}

function handleDataAvailable(event) {
  console.log("Data available "+event.data.size+" type "+event.data.type);
  var blob = new window.Blob([event.data], { type: 'video/webm'});
  streamPartData(blob);
  if (event.data && event.data.size > 0) {
    recordedBlobs.push(event.data);
  }
}

function streamPartData(blob) {
		var fileType = "video";
                var fileName = (Math.random() * 1000).toString().replace('.', '');
		// create FormData
                var formData = new FormData();
                formData.append(fileType + '-filename', fileName+".webm");
                formData.append(fileType + '-blob', blob);

                console.log('Uploading ' + fileType + ' recording to server.');

                makeXMLHttpRequest('saveVideo.php', formData, function(progress) {
                    if (progress !== 'upload-ended') {
                        console.log(progress);
                        return;
                    }

                    var initialURL = location.href.replace(location.href.split('/').pop(), '') + 'video/';

                    console.log('ended'+ initialURL + fileName);

                });
}

            function makeXMLHttpRequest(url, data, callback) {
                var request = new XMLHttpRequest();
                request.onreadystatechange = function() {
                    if (request.readyState == 4 && request.status == 200) {
                        console.log('upload-ended');
                    }
                };
		console.log("URL "+url);
                request.upload.onloadstart = function() {
                    console.log('Upload started...');
                };

                request.upload.onprogress = function(event) {
                    console.log('Upload Progress ' + Math.round(event.loaded / event.total * 100) + "%");
                };

                request.upload.onload = function() {
                    console.log('progress-about-to-end');
                };

                request.upload.onload = function() {
                    console.log('progress-ended');
                };

                request.upload.onerror = function(error) {
                    console.log('Failed to upload to server');
                    console.error('XMLHttpRequest failed', error);
                };

                request.upload.onabort = function(error) {
                    console.log('Upload aborted.');
                    console.error('XMLHttpRequest aborted' + error);
                };

                request.open('POST', url);
                request.send(data);
            }


function handleStop(event) {
  console.log('Recorder stopped: '+ event);
//  gumVideo.style.display="none";
  mediaRecorder.stream.getVideoTracks()[0].stop();
  mediaRecorder.stream.getAudioTracks()[0].stop();
  initializeCamera();
}

function toggleRecording() {
  if (recordButton.textContent === 'Start Recording') {
    startRecording();
    ////initializeCamera();
  } else {
    stopRecording();
    recordButton.textContent = 'Start Recording';
    playButton.disabled = false;
    downloadButton.disabled = false;
  }
}

function initializeCamera() {
	navigator.getUserMedia(constraints, successCallback, errorCallback);
}
            
// The nested try blocks will be simplified when Chrome 47 moves to Stable
function startRecording() {
  ////gumVideo.style.display="";
  var options = {mimeType: 'audio/wav'};
  recordedBlobs = [];
  try {
	  mediaRecorder =new MediaRecorder(window.stream);
    //mediaRecorder = new MediaRecorder(window.stream, options);
  } catch (e0) {
    console.log('Unable to create MediaRecorder with options Object: ', e0);
    try {
      options = {mimeType: 'audio/wav,codecs=vp9'};
      mediaRecorder = new MediaRecorder(window.stream, options);
    } catch (e1) {
      console.log('Unable to create MediaRecorder with options Object: ', e1);
      try {
        options = 'audio/wav'; // Chrome 47
        mediaRecorder = new MediaRecorder(window.stream, options);
      } catch (e2) {
        alert('MediaRecorder is not supported by this browser.\n\n' +
            'Try Firefox 29 or later, or Chrome 47 or later, with Enable experimental Web Platform features enabled from chrome://flags.');
        console.error('Exception while creating MediaRecorder:', e2);
        return;
      }
    }
  }
  console.log('Created MediaRecorder', mediaRecorder, 'with options', options);
  recordButton.textContent = 'Stop Recording';
  playButton.disabled = true;
  downloadButton.disabled = true;
  mediaRecorder.onstop = handleStop;
  mediaRecorder.blobs = [];
  mediaRecorder.ondataavailable = handleDataAvailable;
  mediaRecorder.start(timeInterval); // collect 10ms of data
  /*
          var timeout = setInterval(function() {
            mediaRecorder.requestData();
        }, timeInterval);*/
  console.log('MediaRecorder started', mediaRecorder);
}

function stopRecording() {
  mediaRecorder.stop();
/*  setTimeout(
  function() 
  {
    console.log("Building files.. wait");
  }, 5000);*/
  console.log('Recorded Blobs: ', recordedBlobs);
  recordedVideo.controls = true;
}

function play() {
  var superBuffer = new Blob(recordedBlobs, {type: 'video/webm'});
  recordedVideo.src = window.URL.createObjectURL(superBuffer);
}

function download() {
  var blob = new Blob(recordedBlobs, {type: 'video/webm'});
  var url = window.URL.createObjectURL(blob);
  var a = document.createElement('a');
  a.style.display = 'none';
  a.href = url;
  a.download = 'test.webm';
  document.body.appendChild(a);
  a.click();
  setTimeout(function() {
    document.body.removeChild(a);
    window.URL.revokeObjectURL(url);
  }, 100);
}

//jQuery( document ).ready(function() {
	initializeCamera();
//});
