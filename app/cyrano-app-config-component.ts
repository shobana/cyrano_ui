import {Component,provide} from 'angular2/core';

import {RouteConfig, ROUTER_DIRECTIVES} from 'angular2/router';

import { LoginComponent } from '/app/login/login-component';
import { PasswordProtectedComponent } from '/app/common-components/passwordprotectedpage-component';


import { HomeComponent } from '/app/home-component/home-component';
import { ListComponent } from '/app/list/list-component';

import { CreatorHomeComponent } from './creator-components/creator-home-component';

import { ForgotPasswordComponent } from '/app/login/forgot-password-component';
/*Common-Component*/
import { ProfileManagementComponent } from '/app/common-components/profile-management-component';
 //Welcome Wizard Components
import { ResetPasswordComponent } from '/app/welcomewizard-components/reset-password-component';
import { WelcomePersonalProfileComponent } from '/app/welcomewizard-components/welcomewizard-personal-profile-component';
import { WelcomeProfessionalProfileComponent } from '/app/welcomewizard-components/welcomewizard-professional-profile-component';
import { WelcomeSocialProfileComponent } from '/app/welcomewizard-components/welcomewizard-social-profile-component';
import { WelcomeBioSettingsComponent } from '/app/welcomewizard-components/welcomewizard-biosettings-component';

//Guest Play Program
import { GuestPlayProgramComponent } from '/app/common-components/guest-playpropgram-component';

import {Http} from 'angular2/http';
import {TRANSLATE_PROVIDERS, TranslateService, TranslatePipe, TranslateLoader, TranslateStaticLoader} from 'ng2-translate/ng2-translate';

@Component({ 
  selector: 'cyrano-app',
  template: `<router-outlet></router-outlet>`,
  directives: [ROUTER_DIRECTIVES],
   providers: [TRANSLATE_PROVIDERS,TranslateService,provide(TranslateLoader, {
        useFactory: (http: Http) => new TranslateStaticLoader(http, 'locale', '.json'),
        deps: [Http]
    })],
   pipes: [TranslatePipe]
})

@RouteConfig([
  //{path: '/login', name: 'Login', component: LoginComponent,useAsDefault: true },
  //{path: '/**', name: 'Login', component: LoginComponent},
  
  /*Login Component*/
  {path: '/cyrano', name: 'Cyrano', component: PasswordProtectedComponent,useAsDefault: true },
  {path: '/**', name: 'Cyrano', component: PasswordProtectedComponent},  
  {path: '/login', name: 'Login', component: LoginComponent},
  /*Forgot Password Management*/
  {path: '/forgotPassword', name: 'ForgotPassword', component: ForgotPasswordComponent},
  /*Common-Profile Management*/
  { path: '/profileManagement/...', name: 'ProfileManagement', component: ProfileManagementComponent},
  //Welcome Wizard Files
  {path: '/resetPassword', name: 'ResetPassword', component: ResetPasswordComponent},
  {path: '/personalProfileDetails', name: 'PersonalProfileDetails', component: WelcomePersonalProfileComponent},
  {path: '/professionalProfileDetails', name: 'ProfessionalProfileDetails', component: WelcomeProfessionalProfileComponent},
  {path: '/socialProfileDetails', name: 'SocialProfileDetails', component: WelcomeSocialProfileComponent},
  {path: '/biosettingsDetails', name: 'BioSettingsDetails', component: WelcomeBioSettingsComponent},
  //Guest Play Program
  {path: '/guestPlayProgram', name: 'GuestPlayProgram', component: GuestPlayProgramComponent},
  //To Do
  { path: '/home/...', name: 'CreatorHome', component: CreatorHomeComponent},
  {path: '/list', name: 'ListComponent', component: ListComponent}
  //{path: '/home', name: 'HomeComponent', component: HomeComponent},
])
    
export class CyranoAppConfigComponent {
    constructor(public translate: TranslateService){
        var userLang = navigator.language.split('-')[0]; // use navigator lang if available
        userLang = /(fr|messages_properties_en-US)/gi.test(userLang) ? userLang : 'messages_properties_en-US';
        console.log(userLang);
         // this language will be used as a fallback when a translation isn't found in the current language
        translate.setDefaultLang('en');
         // the lang to use, if the lang isn't available, it will use the current loader to get them
        translate.use(userLang);
    }
}
