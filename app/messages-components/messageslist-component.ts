import {Component, EventEmitter, OnInit,ViewChild,AfterViewInit} from 'angular2/core';
import {CORE_DIRECTIVES, FORM_DIRECTIVES, NgClass, NgIf} from 'angular2/common';
import {PAGINATION_DIRECTIVES} from 'ng2-bootstrap/ng2-bootstrap';

import {NG_TABLE_DIRECTIVES} from '../../../../node_modules/ng2-table/ng2-table';

import {HTTP_PROVIDERS}    from 'angular2/http';
import {Http, Response} from 'angular2/http';
import {Headers, RequestOptions} from 'angular2/http';
import {HTTP_BINDINGS} from 'angular2/http';
import 'rxjs/add/operator/map';
import {Injectable} from 'angular2/core';

import { Router } from 'angular2/router';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from 'angular2/router';
import {NgTableSorting} from '../../../../node_modules/ng2-table/components/table/ng-table-sorting.directive';
import {CORE_DIRECTIVES, FORM_DIRECTIVES, NgClass, NgFor} from 'angular2/common';

import { MODAL_DIRECTIVES } from 'ng2-bs3-modal/ng2-bs3-modal';

import {DND_PROVIDERS, DND_DIRECTIVES} from 'ng2-dnd/ng2-dnd';

@Component({
    selector: 'ngTable, [ngTable]',
    inputs: ['rows', 'columns', 'config'],
    outputs: ['tableChanged'],
    templateUrl: '/views/message/messagesList.html',
    directives: [NG_TABLE_DIRECTIVES, PAGINATION_DIRECTIVES, NgClass, NgIf, CORE_DIRECTIVES, FORM_DIRECTIVES, ROUTER_DIRECTIVES, NgTableSorting, NgClass, MODAL_DIRECTIVES,DND_DIRECTIVES],
    providers: [HTTP_PROVIDERS, HTTP_BINDINGS]
})

export class MessagesListComponent implements OnInit,, AfterViewInit{
        
    @ViewChild('viewAttachmentFilesModal')
    viewAttachmentFilesModal: ModalComponent;
    
     public messagesList;
    
    // Table values
    constructor(private http: Http, private router: Router) {
        this.http = http;
        this.router = router;
        
        this.page = 1;
        this.loadMessagesListTable(this.page);
    }
    
     ngOnInit(){
    
     }
     ngAfterViewInit() {

     }
    
    loadMessagesListTable(page) {
        
        let body = JSON.stringify({ name });
        let headers = new Headers();
        let options = new RequestOptions({ headers: headers });
         return this.http
            .post('//' + $(location).attr('hostname') + ':4000/mock/messagesList/' + page,
            body, options)
            .map(response => response.json())
            .subscribe(
            data => this.checkResponse(data),
            this.logError,
            () => console.log('Data fetching complete')
            );
    }

    checkResponse(response) {
        this.messagesList = response.clientResponse.messagesList;
        this.length = response.total;
    }

    private columns: Array<any> = [
        { title: 'Subject', name: 'subject'},
        { title: 'Message', name: 'message' },
        { title: 'Status', name: 'status'},
        { title: 'Date', name: 'date' },
        { title: 'Receiver', name: 'to' },
        { title: 'Receiver Title', name: 'receiverTitle' },
        { title: 'Receiver Company', name: 'receiverCompany' },
        { title: '# Views', name: 'attachment' },
        { title: 'Action' }
    ];

    public config: any = {
        paging: true,
        sorting: { columns: [] },
    };

    // Outputs (Events)
    public tableChanged: EventEmitter<any> = new EventEmitter();

    onChangeTable(config: any, page: any = config.paging) {
        console.log(page);
        this.loadManageProgramContentsListTable(page.page);
    }

    changeSort(data: any, config: any) {
        console.log(this.config.sorting.columns);
    }
    
    viewAttachmentFiles(){
          this.viewAttachmentFilesModal.open('lg');  
    } 
    
    close(){
       $('#viewAttachmentFilesModal').modal('hide');
       $('.modal-backdrop').remove();
    }
    
    composeMessage(){
           this.router.navigate(['/CreatorHome','ComposeMessage']);
    }
    
    //View Message
    viewMessage(messageUUID){
        this.router.navigate(['/CreatorHome','ViewMessage',{messageUUID:messageUUID}]);
    }
    //Edit Message
    editMessage(messageUUID){
        this.router.navigate(['/CreatorHome','EditMessage',{messageUUID:messageUUID}]);
    }
    
    setPriority(){
          console.log("After DND"+JSON.stringify(this.messagesList));
    }
}