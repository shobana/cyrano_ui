import { Component,EventEmitter, OnInit } from 'angular2/core';
import { CORE_DIRECTIVES } from 'angular2/common';

import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from 'angular2/router';
import {Location, Router} from "angular2/router";

import {HTTP_PROVIDERS}    from 'angular2/http';
import {Http, Response} from 'angular2/http';
import {Headers, RequestOptions} from 'angular2/http';
import {HTTP_BINDINGS} from 'angular2/http';
import 'rxjs/add/operator/map';

@Component({
    templateUrl: 'views/message/externalUsers.html',
    directives: [ROUTER_DIRECTIVES],
    providers: [HTTP_PROVIDERS, HTTP_BINDINGS]
})

export class ExternalUsersComponent {
    
    constructor(private location: Location, private router: Router,private http: Http) {
        this.location = location;
        this.router = router;
        this.http=http;
        
        this.showAddNewUserOption=true;
        this.showBulkUploadOption=false;
    }
    
    addNewUser(){
        this.showAddNewUserOption=true;
        this.showBulkUploadOption=false;
    }
    
    bulkUpload(){
       
        this.userName="eva@gmail.com";
        
        this.showAddNewUserOption=false;
        this.showBulkUploadOption=true;
    }
    
    readUrl($event): void {
        this.postFile($event.target);
    }
    
    postFile(inputValue: any): void {
        var formData = new FormData();
        formData.append("name", "Name");
        formData.append("file", inputValue.files[0]);
        console.log(inputValue.files[0].name);
        this.filename = inputValue.files[0].name;
        if (inputValue.files && inputValue.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#blah')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(200);
            };

          reader.readAsDataURL(inputValue.files[0]);
        }
    }
    
}
