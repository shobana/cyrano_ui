import {Component, EventEmitter, OnInit, AfterViewInit,ViewChild } from 'angular2/core';

import {HTTP_PROVIDERS}    from 'angular2/http';
import {Http, Response} from 'angular2/http';
import {Headers, RequestOptions} from 'angular2/http';
import {HTTP_BINDINGS} from 'angular2/http';
import 'rxjs/add/operator/map';

import {PAGINATION_DIRECTIVES} from 'ng2-bootstrap/ng2-bootstrap';
import {NG_TABLE_DIRECTIVES} from '/node_modules/ng2-table/ng2-table';
import {NgTableSorting} from '/node_modules/ng2-table/components/table/ng-table-sorting.directive';

import { Router } from 'angular2/router';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from 'angular2/router';

import { MODAL_DIRECTIVES } from 'ng2-bs3-modal/ng2-bs3-modal';

import {TranslateService, TranslatePipe} from 'ng2-translate/ng2-translate';

@Component({
    selector: 'ngTable, [ngTable]',
    inputs: ['rows', 'columns', 'config'],
    outputs: ['tableChanged'],
    templateUrl: '/views/message/composeMessage.html',
    directives: [NG_TABLE_DIRECTIVES, PAGINATION_DIRECTIVES,NgTableSorting,ROUTER_DIRECTIVES, MODAL_DIRECTIVES],
    providers: [HTTP_PROVIDERS, HTTP_BINDINGS],
    pipes: [TranslatePipe]
})

export class ComposeMessageComponent implements OnInit, AfterViewInit {

    @ViewChild('selectProgramsModal')
    selectProgramsModal: ModalComponent;
    
     @ViewChild('playContentModal')
    playContentModal: ModalComponent;
    
    @ViewChild('recordContentModal')
    recordContentModal: ModalComponent;
    
    @ViewChild('showExternalLinkModal')
    showExternalLinkModal: ModalComponent;
    
    @ViewChild('invitationConfirmationAlertModal')
    invitationConfirmationAlertModal: ModalComponent;
    
    @ViewChild('showMediaOptionsModal')
    showMediaOptionsModal: ModalComponent;
    
    constructor(private http: Http, private router: Router) {
        this.http = http;
        this.router = router;
        this._tempSelectedDivIds = [];
    }

    ngOnInit() {
        CKEDITOR.replace('editor1');
        $(".textarea").wysihtml5();
        
        $("#testDivId1").addClass("carouselSelect");
    }

    AfterViewInit() {

    }
    
    cancel() {
        this.router.navigate(['MessagesList']);
    }
    
    showMediaOptions(){
        this.showMediaOptionsModal.open();
        this.showAudioRecordingBtn=true;
        

        this.showAudioRecordingBtn=true;
        this.showVideoRecordingBtn=false;
        this.showExternalLinkBtn=false;
        this.showBrowseBtn=false;
    }
        
    showAudioRecordOption() {
        this.showAudioRecordingBtn=true;
        this.showVideoRecordingBtn=false;
        this.showExternalLinkBtn=false;
        this.showBrowseBtn=false;
        
        this.showRecordDetails = false;
    }
    
    showVideoRecordOption() {
        this.showAudioRecordingBtn=false;
        this.showVideoRecordingBtn=true;
        this.showExternalLinkBtn=false;
        this.showBrowseBtn=false;
        
        this.showRecordDetails = false;
    }

    //External Lnk Functionalities-Start
    showExternalLinkOption() {
       this.showAudioRecordingBtn=false;
       this.showVideoRecordingBtn=false;
       this.showExternalLinkBtn=true;
       this.showBrowseBtn=false;
        
       this.showRecordDetails = false;
    }
    
    showExternalLnikModal(){
         this.showExternalLinkModal.open();
    }
    
    addExternalLink(){
       this.showExternalLinkModal.dismiss();
       this.showAudioRecordingBtn=false;
       this.showVideoRecordingBtn=false;
       this.showExternalLinkBtn=false;
       this.showBrowseBtn=false;
        
        this.showRecordDetails = true; 
    }

    //Browse Option Functionalities-Start
    showBrowseOption() {
       this.showAudioRecordingBtn=false;
       this.showVideoRecordingBtn=false;
       this.showExternalLinkBtn=false;
       this.showBrowseBtn=true;
        
       this.showRecordDetails = false;
    }
    
    readUrl($event): void {
        this.postFile($event.target);
    }

    postFile(inputValue: any): void {

        var formData = new FormData();
        formData.append("name", "Name");
        formData.append("file", inputValue.files[0]);
        console.log(inputValue.files[0].name);
        this.filename = inputValue.files[0].name;
        this.showRecordDetails = true;
        this.showBrowseBtn=true;
        if (inputValue.files && inputValue.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#blah')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(200);
            };

            reader.readAsDataURL(inputValue.files[0]);
        }

    }
    
    ok(){
       this.showRecordDetails = true;  
    }
    
    //Select Program Option For attachment file
    showProgramsList(){
         //this.showMediaOptionsType=true;
        
         this.selectProgramsModal.open('md');
        
         this.page = 1;
         this.loadManageProgramsListTable(this.page);
    }
    
    loadManageProgramsListTable(page) {
        let body = JSON.stringify({ name });
        let headers = new Headers();
        let options = new RequestOptions({ headers: headers });
        return this.http
            .post('//' + $(location).attr('hostname') + ':4000/mock/managePrograms/' + page,
            body, options)
            .map(response => response.json())
            .subscribe(
            data => this.checkResponse(data),
            this.logError,
            () => console.log('Data fetching complete')
            );
    }

    checkResponse(response) {
        this.programsList = response.myContentsList;
        this.length = response.total;
    }

    public config: any = {
        paging: true,
        sorting: { columns: [] },
    };
    
    // Outputs (Events)
    public tableChanged: EventEmitter<any> = new EventEmitter();

    onChangeTable(config: any, page: any = config.paging) {
        console.log(page);
        this.loadManageProgramsListTable(page.page);
    }
    
    //Submit with selected programs
    submitPrograms(){
       this.showSelectedProgramsDetails="true";   
       this.showAttachmentDetails=true;
    }
    
    //Adds contacts to To field in compose message
    addContacts(){
        this.router.navigate(['/CreatorHome','Contacts']);
    }
    
    //opens modal for recording content
    openRecordContentModal(){
        this.recordContentModal.open('lg');
        this.recordingUrl="https://rtc.ibotsystems.com/stream-recorder/";
    }
    
    saveRecord() {
        //CKEDITOR.instances.editor2.destroy();
        $('#recordContentModal').modal('hide');
        $('.modal-backdrop').remove();
        
        this.showRecordDetails = true;

        this.showUrl = false;
        this.showBrowse = false;
        this.showRecord = false;
    }
    
    loadInvitationConfirmationAlertModal(){
        this.invitationConfirmationAlertModal.open();
    }
    
    sendMessage(){
       $('#invitationConfirmationAlertModal').modal('hide');
       $('.modal-backdrop').remove();
        this.router.navigate(['/CreatorHome','MessagesList']);   
    }
    
    cancel(){
        this.router.navigate(['/CreatorHome','MessagesList']); 
    }
    
    //Recording Dialog
    open() {
        console.log("test");
        $('#showMediaOptionsModal').modal('hide');
        $('.modal-backdrop').remove();
        
        this.recordContentModal.open('lg');
        this.recordingUrl = "https://rtc.ibotsystems.com/stream-recorder/";
    }

    openplayContentModal() {
        this.playContentModal.open();
    }
    
    
    selectChannel(event){
    $("#testDivId1").removeClass("carouselSelect");         
    this.imgContentId = event.target.id;      
    this.added=false;
    this.checkFlag = false;
      
     if($.inArray( this.imgContentId, this._tempSelectedDivIds ) != -1){         
         this._tempSelectedDivIds.splice($.inArray(this.imgContentId, this._tempSelectedDivIds),1);          
         
         if($("#"+this.imgContentId).hasClass('carouselSelect')) {      
              $("#"+this.imgContentId).removeClass("carouselSelect");              
            }else{        
                    $("#"+this.imgContentId).addClass("carouselSelect");
                $.each(this._tempSelectedDivIds, function( index, value ) {                      
                    $("#"+value).removeClass("carouselSelect");          
                });
              
                this._tempSelectedDivIds.push(this.imgContentId)               
            }
         console.log("irukku..");
         this.added=true;  
    }
       
    if (!this.added) {
      console.log('illai');
      $("#"+this.imgContentId).addClass("carouselSelect");
      $.each(this._tempSelectedDivIds, function( index, value ) {
          $("#"+value).removeClass("carouselSelect");          
      });
      this._tempSelectedDivIds.push(this.imgContentId)
    }
  }   
  
}