import { Component,EventEmitter,ViewChild} from 'angular2/core';
import { CORE_DIRECTIVES } from 'angular2/common';

import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from 'angular2/router';
import {Location, Router} from "angular2/router";

import {NG_TABLE_DIRECTIVES} from '/node_modules/ng2-table/ng2-table';
import {PAGINATION_DIRECTIVES} from 'ng2-bootstrap/ng2-bootstrap';
import {NgTableSorting} from '/node_modules/ng2-table/components/table/ng-table-sorting.directive';

import {HTTP_PROVIDERS}    from 'angular2/http';
import {Http, Response} from 'angular2/http';
import {Headers, RequestOptions} from 'angular2/http';
import {HTTP_BINDINGS} from 'angular2/http';
import 'rxjs/add/operator/map';

import { MODAL_DIRECTIVES } from 'ng2-bs3-modal/ng2-bs3-modal';

@Component({
    selector: 'ngTable, [ngTable]',
    inputs: ['rows', 'columns', 'config'],
    outputs: ['tableChanged'],
    templateUrl: 'views/message/contacts.html',
    directives: [ROUTER_DIRECTIVES,NG_TABLE_DIRECTIVES, PAGINATION_DIRECTIVES,NgTableSorting,MODAL_DIRECTIVES],
    providers: [HTTP_PROVIDERS, HTTP_BINDINGS]
})

export class ContactsComponent {
    
    @ViewChild('addNewExternalUsersModal')
    addNewExternalUsersModal: ModalComponent;
    
    @ViewChild('bulkImportExternalUsersModal')
    bulkImportExternalUsersModal: ModalComponent;
    
    @ViewChild('invitationConfirmationAlertModal')
    invitationConfirmationAlertModal: ModalComponent;
    
    constructor(private location: Location, private router: Router,private http: Http) {
        this.location = location;
        this.router = router;
        this.http=http;
        
        this.page = 1;
        this.loadInternalUsersTable(this.page);
    }

    loadInternalUsersTable(page){
        let body = JSON.stringify({ name });
        let headers = new Headers();
        let options = new RequestOptions({ headers: headers });
        return this.http
            .post('//' + $(location).attr('hostname') + ':4000/mock/manageUsersList/' + page,
            body, options)
            .map(response => response.json())
            .subscribe(
            data => this.checkResponseCurrent(data),
            this.logError,
            () => console.log('Data fetching complete')
            );
    }

    checkResponseCurrent(response) {
        this.rows = response.UsersList;
        this.length = response.UsersList.length;
    }  
    
     private columns: Array<any> = [
        { title: 'Select', name: 'UsersUUID' },
        { title: 'Name', name: 'name' },
        { title: 'External ?', name: 'isExternal' },
        { title: 'Company/Organization', name: 'companyName' },
        { title: 'Email', name: 'email' },
        { title: 'Title', name: 'title' },
        { title: 'Phone', name: 'phone' }
    ];
   
    public config: any = {
        paging: true,
        sorting: { columns: [] }
    };

    // Outputs (Events)
    public tableChanged: EventEmitter<any> = new EventEmitter();

    onChangeTable(config: any, page: any = config.paging) {
        this.loadNewProgramsListTable(page.page);
    }
    
    getLinkStyle(path) {
        return this.location.path().indexOf(path) > -1;
    }

    close() {
        this.router.navigate(['ComposeMessage']);
    }
    
    add(){
        this.router.navigate(['ComposeMessage']);
    }
    
    //Add New External Users
    loadAddNewExternalUsersModal() {
        this.addNewExternalUsersModal.open();
    }
    
    addNewExternalUsers(){
       $('#addNewExternalUsersModal').modal('hide');
       $('.modal-backdrop').remove();
    }
    
    //Bulk Import External Users
    loadBulkImportExternalUsersModal() {
        this.bulkImportExternalUsersModal.open();
    }
    
     readUrl($event): void {
        this.postFile($event.target);
    }

    postFile(inputValue: any): void {

        var formData = new FormData();
        formData.append("name", "Name");
        formData.append("file", inputValue.files[0]);
        console.log(inputValue.files[0].name);
        this.filename = inputValue.files[0].name;
        this.showRecordDetails = true;
        if (inputValue.files && inputValue.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#blah')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(200);
            };

            reader.readAsDataURL(inputValue.files[0]);
        }

    }
    
    importBulkUploadUsers(){
        this.invitationConfirmationAlertModal.open();
    }
    
    submit(){
        console.log("success");
    }
    
}
