import { Component,EventEmitter, OnInit } from 'angular2/core';
import { CORE_DIRECTIVES } from 'angular2/common';

import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from 'angular2/router';
import {Location, Router} from "angular2/router";

import {NG_TABLE_DIRECTIVES} from '/node_modules/ng2-table/ng2-table';
import {PAGINATION_DIRECTIVES} from 'ng2-bootstrap/ng2-bootstrap';
//import {NG_TABLE_DIRECTIVES} from '/node_modules/ng2-easy-table/ng2-easy-table';
import {NgTableSorting} from '/node_modules/ng2-table/components/table/ng-table-sorting.directive';

import {HTTP_PROVIDERS}    from 'angular2/http';
import {Http, Response} from 'angular2/http';
import {Headers, RequestOptions} from 'angular2/http';
import {HTTP_BINDINGS} from 'angular2/http';
import 'rxjs/add/operator/map';

@Component({
    selector: 'ngTable, [ngTable]',
    inputs: ['rows', 'columns', 'config'],
    outputs: ['tableChanged'],
    templateUrl: 'views/message/internalUsers.html',
    directives: [ROUTER_DIRECTIVES,NG_TABLE_DIRECTIVES, PAGINATION_DIRECTIVES,NgTableSorting],
    providers: [HTTP_PROVIDERS, HTTP_BINDINGS]
})

export class InternalUsersComponent {
    
    constructor(private location: Location, private router: Router,private http: Http) {
        this.location = location;
        this.router = router;
        this.http=http;
        
        this.page = 1;
        this.loadInternalUsersTable(this.page);
    }
    
    loadInternalUsersTable(page){
        let body = JSON.stringify({ name });
        let headers = new Headers();
        let options = new RequestOptions({ headers: headers });
        return this.http
            .post('//' + $(location).attr('hostname') + ':4000/mock/manageUsersList/' + page,
            body, options)
            .map(response => response.json())
            .subscribe(
            data => this.checkResponseCurrent(data),
            this.logError,
            () => console.log('Data fetching complete')
            );
    }

    checkResponseCurrent(response) {
        this.rows = response.UsersList;
        this.length = response.UsersList.length;
    }  
    
     private columns: Array<any> = [
        { title: 'Select', name: 'UsersUUID' },
        { title: 'Name', name: 'name' },
        { title: 'Email', name: 'email' },
        { title: 'Title', name: 'title' },
        { title: 'Phone', name: 'phone' }
    ];
   
    public config: any = {
        paging: true,
        sorting: { columns: [] }
    };

    // Outputs (Events)
    public tableChanged: EventEmitter<any> = new EventEmitter();

    onChangeTable(config: any, page: any = config.paging) {
        this.loadNewProgramsListTable(page.page);
    }
    
}
