import {bootstrap} from 'angular2/platform/browser';
import { bind ,provide} from 'angular2/core';
import {ROUTER_PROVIDERS, LocationStrategy, HashLocationStrategy} from 'angular2/router';
import {CyranoAppConfigComponent} from './cyrano-app-config-component';
import {HTTP_PROVIDERS} from 'angular2/http';

import {DND_PROVIDERS, DND_DIRECTIVES} from 'ng2-dnd/ng2-dnd';

bootstrap(CyranoAppConfigComponent,[ROUTER_PROVIDERS,HTTP_PROVIDERS,DND_PROVIDERS,bind(LocationStrategy).toClass(HashLocationStrategy)]).then(
    success => console.log('Cyrano app bootstrapped!'),
    error => console.log(error)
);

