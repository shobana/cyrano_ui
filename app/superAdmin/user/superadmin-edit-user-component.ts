import {Component, EventEmitter, OnInit, AfterViewInit} from 'angular2/core';

import {HTTP_PROVIDERS}    from 'angular2/http';
import {Http, Response} from 'angular2/http';
import {Headers, RequestOptions} from 'angular2/http';
import {HTTP_BINDINGS} from 'angular2/http';
import 'rxjs/add/operator/map';
import {Injectable} from 'angular2/core';

import { Router , RouteParams} from 'angular2/router';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from 'angular2/router';

@Component({
    templateUrl: '/views/superAdmin/user/editUser.html',
    directives: [ROUTER_DIRECTIVES],
    providers: [HTTP_PROVIDERS, HTTP_BINDINGS]
})

export class SuperAdminEditUserComponent implements OnInit, AfterViewInit {
    // Table values
    constructor(private http: Http, private router: Router, private params: RouteParams) {
        this.http = http;
        this.router = router;

        this.contentPriority = 50;
        this.roleName = "Viewer";
        
         this.currentRole = params.get('roleName');
        
        this.zone=[{
                            "name": "UTC-11: Samoa Standard Time",
                            "abbreviation": "AL"
                        }, {
                            "name": "UTC-10: Hawaii-Aleutian Standard Time (HST)",
                            "abbreviation": "AK"
                        }, {
                            "name": "UTC-9: Alaska Standard Time (AKST)",
                            "abbreviation": "AS"
                        }, {
                            "name": "UTC-8: Pacific Standard (PST)",
                            "abbreviation": "AZ"
                        }, {
                            "name": "UTC-7: Mountain Standard Time (MST)",
                            "abbreviation": "AR"
                        }, {
                            "name": "UTC-6: Central Standard Time (CST)",
                            "abbreviation": "CA"
                        }, {
                            "name": "UTC-5: Eastern Standard Time (EST)",
                            "abbreviation": "CO"
                        }, {
                            "name": "UTC-4: Atlantic Standard Time (AST)",
                            "abbreviation": "CT"
                        }, {
                            "name": "UTC+10:Chamorro Standard Time",
                            "abbreviation": "DE"
                        }];   
    }

    ngOnInit() {

        $('#employmentStartDate')
            .datepicker({
                defaultDate: new Date(),
                format: 'mm/dd/yyyy',
                autoclose: true
            }).on('show', function(e) {
                var $popup = $('.datepicker');
                $popup.click(function() { return false; });
            });

        $('#inner-content-div-group').slimScroll({
            position: 'right',
            height: '100px',
            railVisible: true,
            alwaysVisible: true
        });

        $('select[name="additionalGroups[]"]').bootstrapDualListbox(
            {  nonSelectedListLabel: 'Available Channels',
               selectedListLabel: 'Selected Channels'}
        );

        $('select[name="additionalGroups1[]"]').bootstrapDualListbox(
            {  nonSelectedListLabel: 'Available Roles',
               selectedListLabel: 'Selected Roles'}
        );
        
        $('select[name="additionalGroups_publish[]"]').bootstrapDualListbox(
            {  nonSelectedListLabel: 'Available Channels',
               selectedListLabel: 'Selected Channels'}
        );

        this.getuserDetailsById();

        this.rolesVOList = [{ "name": "Content Creator" }, { "name": "Content Manager" }, { "name": "Admin" }, { "name": "Viewer" }];

        this.groupsList=[{"name":"Strategy & Operations","icon":"dist/img/channels_thumb/Change.png"},{"name":"Engagement","icon":"dist/img/channels_thumb/Engagement.png"},{"name":"Culture","icon":"dist/img/channels_thumb/Culture.png"},{"name":"Training & Development","icon":"dist/img/channels_thumb/Training.png"},{"name":"Wellness","icon":"dist/img/channels_thumb/Wellness.png"},{"name":"Performance Management","icon":"dist/img/channels_thumb/Performance.png"},{"name":"Recruiting & onboarding","icon":"dist/img/channels_thumb/Recruting.png"},{"name":"Change","icon":"dist/img/channels_thumb/Change.png"}];
    }

    ngAfterViewInit() {
        if (!SuperAdminEditUserComponent.chosenInitialized) {
            jQuery('#grouplist_dual').bootstrapDualListbox('refresh', true);
            jQuery('#grouplist_dual_roles').bootstrapDualListbox('refresh', true);
            jQuery('#grouplist_dual_publish').bootstrapDualListbox('refresh', true);
            SuperAdminEditUserComponent.chosenInitialized = true;
        }

    }

    getuserDetailsById() {
        let body = JSON.stringify({ name });
        let headers = new Headers();
        let options = new RequestOptions({ headers: headers });
        return this.http
            .post('//' + $(location).attr('hostname') + ':4000/mock/getCustomerUserDetails/',
            body, options)
            .map(response => response.json())
            .subscribe(
            data => this.checkResponse(data),
            this.logError,
            () => console.log('Data fetching complete')
            );
    }

    checkResponse(response) {
        this.customerVO = response.clientResponse.customerUserSummaryVO;
        this.customerUserUUID = response.clientResponse.customerUserSummaryVO.customerUserUUID;
        this.userLogo = response.clientResponse.customerUserSummaryVO.userLogo;
        this.userName = response.clientResponse.customerUserSummaryVO.userName;
        this.firstName = response.clientResponse.customerUserSummaryVO.firstName;
        this.lastName = response.clientResponse.customerUserSummaryVO.lastName;
        this.title = response.clientResponse.customerUserSummaryVO.title;
        this.companyName = response.clientResponse.customerUserSummaryVO.companyName;
        this.workPhone = response.clientResponse.customerUserSummaryVO.workPhone;
        this.contentPriority = response.clientResponse.customerUserSummaryVO.contentPriority;
        this.isActive = response.clientResponse.customerUserSummaryVO.isActive;
        this.joiningDate = response.clientResponse.customerUserSummaryVO.joiningDate;
    }
    
    readUrl($event): void {
        this.postFile($event.target);
    }
    
    postFile(inputValue: any): void {
        var formData = new FormData();
        formData.append("name", "Name");
        formData.append("file", inputValue.files[0]);
        console.log(inputValue.files[0].name);
        this.filename = inputValue.files[0].name;
        if (inputValue.files && inputValue.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#blah')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(200);
            };

          reader.readAsDataURL(inputValue.files[0]);
        }
    }
    
    cancel(){
         this.router.navigate(['/CreatorHome','SuperAdminManageUsers',{'roleName':this.currentRole}]);
    }
    
    cancel(){
        this.router.navigate(['/CreatorHome','SuperAdminManageUsers',{'roleName':this.currentRole}]);
    }
}