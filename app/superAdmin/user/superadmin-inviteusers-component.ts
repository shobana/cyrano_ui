import {Component, EventEmitter, OnInit, AfterViewInit,ViewChild} from 'angular2/core';

import {HTTP_PROVIDERS}    from 'angular2/http';
import {Http, Response} from 'angular2/http';
import {Headers, RequestOptions} from 'angular2/http';
import {HTTP_BINDINGS} from 'angular2/http';
import 'rxjs/add/operator/map';
import {Injectable} from 'angular2/core';

import { Router,RouteParams} from 'angular2/router';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from 'angular2/router';

import {PAGINATION_DIRECTIVES} from 'ng2-bootstrap/ng2-bootstrap';
import {NG_TABLE_DIRECTIVES} from '/node_modules/ng2-table/ng2-table';
import {NgTableSorting} from '/node_modules/ng2-table/components/table/ng-table-sorting.directive';

import { MODAL_DIRECTIVES } from 'ng2-bs3-modal/ng2-bs3-modal';

@Component({
    selector: 'ngTable, [ngTable]',
    inputs: ['rows', 'columns', 'config'],
    outputs: ['tableChanged'],
    templateUrl: '/views/superAdmin/user/inviteUser.html',
    directives: [NG_TABLE_DIRECTIVES, PAGINATION_DIRECTIVES,NgTableSorting, MODAL_DIRECTIVES,ROUTER_DIRECTIVES],
    providers: [HTTP_PROVIDERS, HTTP_BINDINGS]
})

export class SuperAdminInviteUserComponent implements OnInit, AfterViewInit {
    // Table values
    constructor(private http: Http, private router: Router, private params: RouteParams) {
        this.http = http;
        this.router = router;
        
        this.page = 1;
        this.loadManageProgramsListTable(this.page);
        this._tempSelectedDivIds = [];
        
       this.currentRole = params.get('roleName');
    }

    /*ngOnInit() {
    
    }*/

    ngAfterViewInit() {
    
    }
    
    loadManageProgramsListTable(page) {
        let body = JSON.stringify({ name });
        let headers = new Headers();
        let options = new RequestOptions({ headers: headers });
        return this.http
            .post('//' + $(location).attr('hostname') + ':4000/mock/managePrograms/' + page,
            body, options)
            .map(response => response.json())
            .subscribe(
            data => this.checkResponse(data),
            this.logError,
            () => console.log('Data fetching complete')
            );
    }

    checkResponse(response) {
        this.programsList = response.myContentsList;
        this.length = response.total;
    }

    public config: any = {
        paging: true,
        sorting: { columns: [] },
    };
    
    // Outputs (Events)
    public tableChanged: EventEmitter<any> = new EventEmitter();

    onChangeTable(config: any, page: any = config.paging) {
        console.log(page);
        this.loadManageProgramsListTable(page.page);
    }
    
     inviteUser(){
         console.log(this.currentRole);
          $('#invitationConfirmationAlertModal').modal('hide');
          $('.modal-backdrop').remove();
         if(this.currentRole=='SuperAdmin'){
             this.router.navigate(['/CreatorHome','SuperAdminManageUsers',{roleName:this.currentRole}]);
         }else{
             this.router.navigate(['/CreatorHome','SuperAdminManageUsers',{roleName:this.currentRole}]);
         }
    }
    
    cancel(){
         if(this.currentRole=='SuperAdmin'){
             this.router.navigate(['/CreatorHome','SuperAdminManageUsers',{roleName:this.currentRole}]);
         }else{
             this.router.navigate(['/CreatorHome','SuperAdminManageUsers',{roleName:this.currentRole}]);
         }
    }
    
    selectChannel(event){
    $("#testDivId1").removeClass("carouselSelect");         
    this.imgContentId = event.target.id;      
    this.added=false;
    this.checkFlag = false;
      
     if($.inArray( this.imgContentId, this._tempSelectedDivIds ) != -1){         
         this._tempSelectedDivIds.splice($.inArray(this.imgContentId, this._tempSelectedDivIds),1);          
         
         if($("#"+this.imgContentId).hasClass('carouselSelect')) {      
              $("#"+this.imgContentId).removeClass("carouselSelect");              
            }else{        
                    $("#"+this.imgContentId).addClass("carouselSelect");
                $.each(this._tempSelectedDivIds, function( index, value ) {                      
                    $("#"+value).removeClass("carouselSelect");          
                });
              
                this._tempSelectedDivIds.push(this.imgContentId)               
            }
         console.log("irukku..");
         this.added=true;  
    }
       
    if (!this.added) {
      console.log('illai');
      $("#"+this.imgContentId).addClass("carouselSelect");
      $.each(this._tempSelectedDivIds, function( index, value ) {
          $("#"+value).removeClass("carouselSelect");          
      });
      this._tempSelectedDivIds.push(this.imgContentId)
    }
  }
    
  ngOnInit() {
    $("#testDivId1").addClass("carouselSelect");
  }
     
}