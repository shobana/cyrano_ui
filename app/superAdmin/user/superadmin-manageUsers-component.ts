import {Component, EventEmitter, OnInit,ElementRef, ViewEncapsulation} from 'angular2/core';

import {AutocompleteContainer} from '../../../dist/angular2autocomplete/autocomplete-container';
import {Autocomplete} from '../../../dist/angular2autocomplete/autocomplete.component';
export const AUTOCOMPLETE_DIRECTIVES = [Autocomplete, AutocompleteContainer];

import {CORE_DIRECTIVES, FORM_DIRECTIVES, NgClass, NgIf} from 'angular2/common';
import {PAGINATION_DIRECTIVES} from 'ng2-bootstrap/ng2-bootstrap';

import {NG_TABLE_DIRECTIVES} from '../../../node_modules/ng2-table/ng2-table';

import {HTTP_PROVIDERS}    from 'angular2/http';
import {Http, Response} from 'angular2/http';
import {Headers, RequestOptions} from 'angular2/http';
import {HTTP_BINDINGS} from 'angular2/http';
import 'rxjs/add/operator/map';
import {Injectable} from 'angular2/core';

import { Router } from 'angular2/router';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS, RouteParams  } from 'angular2/router';
import {NgTableSorting} from '../../../node_modules/ng2-table/components/table/ng-table-sorting.directive';
import {CORE_DIRECTIVES, FORM_DIRECTIVES, NgClass, NgFor} from 'angular2/common';

import { MODAL_DIRECTIVES } from 'ng2-bs3-modal/ng2-bs3-modal';

//import {AutocompleteDirective} from "../../../dist/ng2-autocomplete/autocomplete";,AutocompleteDirective

@Component({
    selector:'ngTable, [ngTable]',
    inputs: ['rows', 'columns', 'config'],
    outputs: ['tableChanged'],
    templateUrl: '/views/superAdmin/user/manageUsers.html',
    directives: [AUTOCOMPLETE_DIRECTIVES,NG_TABLE_DIRECTIVES, PAGINATION_DIRECTIVES, NgClass, NgIf, CORE_DIRECTIVES, FORM_DIRECTIVES, ROUTER_DIRECTIVES, NgTableSorting, NgClass, MODAL_DIRECTIVES],
    providers: [HTTP_PROVIDERS, HTTP_BINDINGS]
})

export class SuperAdminManageUsersComponent implements OnInit {
    currentStatus: string;
    // Table values
    constructor(private http: Http, private router: Router, private params: RouteParams) {
        this.http = http;
        this.router = router;
        this.page = 1;
        this.currentRole = params.get('roleName');
        this.loadManageUsersListTable(this.page);
        this.loadUsersInvitationHistoryTable(this.page);
    }

    loadManageUsersListTable(page) {
        let body = JSON.stringify({ name });
        let headers = new Headers();
        let options = new RequestOptions({ headers: headers });
        return this.http
            .post('//' + $(location).attr('hostname') + ':4000/mock/manageCompanyUsersList/' + page,
            body, options)
            .map(response => response.json())
            .subscribe(
            data => this.checkResponse(data),
            this.logError,
            () => console.log('Data fetching complete')
            );
    }

    checkResponse(response) {
        this.usersVOList = response.clientResponse;
        this.length = response.total;
    }

    private columns: Array<any> = [
        { title: 'User Name', name: 'userName' },
        { title: 'Name', name: 'name' },
        { title: 'Title', name: 'title' },
        { title: 'Role(s)', name: 'roleNames' },
        { title: 'User Content Priority', name: 'contentPriority' },
        { title: 'Active?', name: 'active' },
        { title: 'Action', name: 'customerUserUUID' }
    ];

    public config: any = {
        paging: true,
        sorting: { columns: [] },
    };

    // Outputs (Events)
    public tableChanged: EventEmitter<any> = new EventEmitter();

    onChangeTable(config: any, page: any = config.paging) {
        console.log(page);
        this.loadManageUsersListTable(page.page);
    }

    changeSort(data: any, config: any) {
        console.log(this.config.sorting.columns);
    }
    
    //2nd table
    
    loadUsersInvitationHistoryTable(page) {
        let body = JSON.stringify({ name });
        let headers = new Headers();
        let options = new RequestOptions({ headers: headers });
        return this.http
            .post('//' + $(location).attr('hostname') + ':4000/mock/usersInvitationHistory/' + page,
            body, options)
            .map(response => response.json())
            .subscribe(
            data => this.checkResponseForUsershistory(data),
            this.logError,
            () => console.log('Data fetching complete')
            );
    }

    checkResponseForUsershistory(response) {
        this.bulkUsersList = response.clientResponseHistory;
        this.bulkUsersListTotal = response.total;
    }

    private columnsForUsersHistory: Array<any> = [
        { title: 'Uploaded Date', name: 'uploadedDate' },
        { title: 'Author', name: 'author' },
        { title: 'Status', name: 'titlstatus' },
        { title: '# Users', name: 'noOfUsers' },
        { title: 'Success Count', name: 'successCount' },
        { title: 'Failure Count', name: 'failureCount' },
        { title: 'Result', name: 'result' }
    ];

    public configForUsersHistory: any = {
        paging: true,
        sorting: { columns: [] },
    };

    // Outputs (Events)
    public tableChangedForUsersHistory: EventEmitter<any> = new EventEmitter();

    onChangeTableForUsersHistory(config: any, page: any = config.paging) {
        console.log(page);
        this.loadUsersInvitationHistoryTable(page.page);
    }

    changeSortForUsersHistory(data: any, config: any) {
        console.log(this.config.sorting.columns);
    }

    addNewUser() {
        console.log(this);
        if(this.currentRole=='Admin'){
            this.router.navigate(['CustomerAdminAddUsers']);
        }else{
            this.router.navigate(['SuperAdminAddUsers']);   
        }
    }

    editUser(userUUID) {
        this.router.navigate(['SuperAdminEditUser', { userUUID: userUUID,roleName:this.currentRole }]);
    }

    
    addBulkUpload() {
        if( this.currentRole=='SuperAdmin'){
            this.router.navigate(['SuperAdminBulkUploadUsers',{roleName:this.currentRole}]);
        }else{
             this.router.navigate(['SuperAdminBulkUploadUsers',{roleName:this.currentRole}]);
        }
    }
    
    /* auto complete list start*/
    
    private selectedCar:string = '';
    private asyncSelectedCar:string = '';
    private autocompleteLoading:boolean = false;
    private autocompleteNoResults:boolean = false;
    private carsExample1:Array<string> = ['Cyrano', 'IBOT','SMI','Chyrismo','VGS','IBM','General Motors','KEN','LEAMAN BROTHERS','MIANO'];
    private carsExample2:Array<any> = [
        {id: 1, name: 'IBOT'},
        {id: 2, name: 'SMI'},
        {id: 3, name: 'Chyrismo'},
        {id: 4, name: 'Chyrismo'},
        {id: 5, name: 'VGS'},
        {id: 6, name: 'IBM'},
        {id: 7, name: 'General Motors'},
        {id: 8, name: 'KEN'},
        {id: 9, name: 'LEAMAN BROTHERS'},
        {id: 10, name: 'MIANO'},
    ];
 
 
    private getCurrentContext() {
        return this;
    }
 
    private _cachedResult:any;
    private _previousContext:any;
 
    private getAsyncData(context:any):Function {
        if (this._previousContext === context) {
            return this._cachedResult;
        }
 
        this._previousContext = context;
        let f:Function = function ():Promise<string[]> {
            let p:Promise<string[]> = new Promise((resolve:Function) => {
                setTimeout(() => {
                    let query = new RegExp(context.asyncSelectedCar, 'ig');
                    return resolve(context.carsExample1.filter((state:any) => {
                        return query.test(state);
                    }));
                }, 500);
            });
            return p;
        };
        this._cachedResult = f;
        return this._cachedResult;
    }
 
    private changeAutocompleteLoading(e:boolean) {
        this.autocompleteLoading = e;
    }
 
    private changeAutocompleteNoResults(e:boolean) {
        this.autocompleteNoResults = e;
    }
 
    private autocompleteOnSelect(e:any) {
        console.log(`Selected value: ${e.item}`);
    }
    
    /* auto complete list end*/
    
    /*public customerName = "";
    
    //Auto Suggestion for Customer
     public serachCountry() {
        return (filter: string): Promise<Array<{ text: string, data: any }>> => {
            return new Promise<Array<{ text: string, data: any }>>((resolve, reject) => {
                this.http.get("mock/customerList" + filter)
                .map(res => res.json())
                .map(countries => countries.map(country => {
                    return {text: country.name, data: country};
                }))
                .subscribe(
                    countries => resolve(countries),
                    err => reject(err)
                );
            });
        };
    }

    public onCountrySelected(selected: { text: string, data: any }) {
        this.countryName = selected.text;
    }*/
}