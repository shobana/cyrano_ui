import {Component, EventEmitter, OnInit, AfterViewInit,ViewChild} from 'angular2/core';

import {HTTP_PROVIDERS}    from 'angular2/http';
import {Http, Response} from 'angular2/http';
import {Headers, RequestOptions} from 'angular2/http';
import {HTTP_BINDINGS} from 'angular2/http';
import 'rxjs/add/operator/map';
import {Injectable} from 'angular2/core';

import { Router ,RouteParams} from 'angular2/router';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from 'angular2/router';

import { MODAL_DIRECTIVES } from 'ng2-bs3-modal/ng2-bs3-modal';

@Component({
    templateUrl: '/views/superAdmin/user/bulkUploadUsers.html',
    directives: [ROUTER_DIRECTIVES,MODAL_DIRECTIVES],
    providers: [HTTP_PROVIDERS, HTTP_BINDINGS]
})

export class SuperAdminBulkUploadUsersComponent implements OnInit, AfterViewInit {
    
    @ViewChild('invitationConfirmationAlertModal')
    invitationConfirmationAlertModal: ModalComponent;
    
    // Table values
    constructor(private http: Http, private router: Router, private params: RouteParams) {
        this.http = http;
        this.router = router;
        
        this.currentRole = params.get('roleName');
        this.userName="eva@gmail.com";
    }

    ngOnInit() {

    }

    ngAfterViewInit() {

    }
    
    readUrl($event): void {
        this.postFile($event.target);
    }
    
    postFile(inputValue: any): void {
        var formData = new FormData();
        formData.append("name", "Name");
        formData.append("file", inputValue.files[0]);
        console.log(inputValue.files[0].name);
        this.filename = inputValue.files[0].name;
        if (inputValue.files && inputValue.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#blah')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(200);
            };

          reader.readAsDataURL(inputValue.files[0]);
        }
    }
    
   loadInvitationConfirmationAlertModal(){
       this.invitationConfirmationAlertModal.open();
   }
    
   importBulkUploadUsers(){
          $('#invitationConfirmationAlertModal').modal('hide');
          $('.modal-backdrop').remove();
       
        if(this.currentRole=='SuperAdmin'){
             this.router.navigate(['/CreatorHome','SuperAdminInviteUser',{roleName:this.currentRole}]);
         }else{
             this.router.navigate(['/CreatorHome','SuperAdminInviteUser',{roleName:this.currentRole}]);
         }
    }
    
    cancel(){
          $('#invitationConfirmationAlertModal').modal('hide');
          $('.modal-backdrop').remove();
        
         if(this.currentRole=='SuperAdmin'){
             this.router.navigate(['/CreatorHome','SuperAdminManageUsers',{roleName:this.currentRole}]);
         }else{
             this.router.navigate(['/CreatorHome','SuperAdminManageUsers',{roleName:this.currentRole}]);
         }
    }
}