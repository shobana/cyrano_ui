import {Component, EventEmitter, OnInit, AfterViewInit,ViewChild } from 'angular2/core';

import {HTTP_PROVIDERS}    from 'angular2/http';
import {Http, Response} from 'angular2/http';
import {Headers, RequestOptions} from 'angular2/http';
import {HTTP_BINDINGS} from 'angular2/http';
import 'rxjs/add/operator/map';

import { Router } from 'angular2/router';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from 'angular2/router';

@Component({
    templateUrl: '/views/superAdmin/customer/addNewCustomer.html',
    directives: [ROUTER_DIRECTIVES],
    providers: [HTTP_PROVIDERS, HTTP_BINDINGS]
})

export class SuperAdminAddNewCustomerComponent implements OnInit, AfterViewInit {

    constructor(private http: Http, private router: Router) {
        this.http = http;
        this.router = router;
    }

    ngOnInit() {
        this.expiryPeriod=30;
        this.archivePeriod=30;
        this.purgePeriod=180;
    }

    AfterViewInit() {

    }
    
    readUrl($event): void {
        this.postFile($event.target);
    }
    
    postFile(inputValue: any): void {
        var formData = new FormData();
        formData.append("name", "Name");
        formData.append("file", inputValue.files[0]);
        console.log(inputValue.files[0].name);
        this.filename = inputValue.files[0].name;
        if (inputValue.files && inputValue.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#blah')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(200);
            };

          reader.readAsDataURL(inputValue.files[0]);
        }
    }
        
    save(){
        this.router.navigate(['/CreatorHome','SuperAdminManageCustomers']);
    }
    
    cancel(){
        this.router.navigate(['/CreatorHome','SuperAdminManageCustomers']);
    }

}