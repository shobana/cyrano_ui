import {Component, EventEmitter, OnInit, AfterViewInit, ViewChild } from 'angular2/core';

import {HTTP_PROVIDERS}    from 'angular2/http';
import {Http, Response} from 'angular2/http';
import {Headers, RequestOptions} from 'angular2/http';
import {HTTP_BINDINGS} from 'angular2/http';
import 'rxjs/add/operator/map';

import { Router,RouteParams } from 'angular2/router';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from 'angular2/router';
import {ImageCropperComponent, CropperSettings} from '../../../node_modules/ng2-img-cropper/src/imageCropper';
import { MODAL_DIRECTIVES } from 'ng2-bs3-modal/ng2-bs3-modal';


@Component({
    selector: 'test-app',
    templateUrl: '/views/superAdmin/customer/editCustomer.html',
    directives: [ROUTER_DIRECTIVES,ImageCropperComponent, MODAL_DIRECTIVES],
    providers: [HTTP_PROVIDERS, HTTP_BINDINGS]
})

export class SuperAdminEditCustomerComponent implements OnInit, AfterViewInit {
        
    data: any;
    cropperSettings: CropperSettings;

    constructor(private http: Http, private router: Router, private params: RouteParams) {
        this.http = http;
        this.router = router;

        this.currentStatus = params.get('status');
        
        this.getCustomerDetailsById();
        
        this.cropperSettings = new CropperSettings();
        this.cropperSettings.width = 100;
        this.cropperSettings.height = 100;
        this.cropperSettings.croppedWidth =100;
        this.cropperSettings.croppedHeight = 100;
        this.cropperSettings.canvasWidth = 400;
        this.cropperSettings.canvasHeight = 300;
 
        this.data = {};
        
        this.isImgUploaded = true;
        this.isImgEditable = false;
    }
    
    changeBtnShow(){
        this.isImgUploaded = false;
        this.isImgEditable = true;     
     }

    getCustomerDetailsById() {
        let body = JSON.stringify({ name });
        let headers = new Headers();
        let options = new RequestOptions({ headers: headers });
        return this.http
            .post('//' + $(location).attr('hostname') + ':4000/mock/getCustomerDetails/',
            body, options)
            .map(response => response.json())
            .subscribe(
            data => this.checkResponse(data),
            this.logError,
            () => console.log('Data fetching complete')
            );
    }

    checkResponse(response) {
        this.customerVO = response.clientResponse.CustomerVO;
        this.customerName = response.clientResponse.CustomerVO.customerName;
        this.website = response.clientResponse.CustomerVO.website;
        this.addressLine1 = response.clientResponse.CustomerVO.addressLine1;
        this.addressLine2 = response.clientResponse.CustomerVO.addressLine2;
        this.city = response.clientResponse.CustomerVO.city;
        this.country = response.clientResponse.CustomerVO.country;
        this.state = response.clientResponse.CustomerVO.state;
        this.zip = response.clientResponse.CustomerVO.zip;
        this.expiryPeriod = response.clientResponse.CustomerVO.expiryPeriod;
    }

    ngOnInit() {
        this.expiryPeriod = 30;
        this.archivePeriod = 30;
        this.purgePeriod = 180;
    }

    AfterViewInit() {
        
    }

    readUrl($event): void {
        this.postFile($event.target);
    }

    postFile(inputValue: any): void {
        var formData = new FormData();
        formData.append("name", "Name");
        formData.append("file", inputValue.files[0]);
        console.log(inputValue.files[0].name);
        this.filename = inputValue.files[0].name;
        if (inputValue.files && inputValue.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#blah')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(200);
            };

            reader.readAsDataURL(inputValue.files[0]);
        }
    }

    save() {
        if(this.currentStatus=='SuperAdmin'){
            this.router.navigate(['/CreatorHome', 'SuperAdminManageCustomers']);
        }else{
            //his.router.navigate(['/CreatorHome', 'CustomerAdminManageCustomers']);
        }
    }

    cancel() {
        if(this.currentStatus=='SuperAdmin'){
             this.router.navigate(['/CreatorHome', 'SuperAdminManageCustomers']);
        }else{
             //this.router.navigate(['/CreatorHome', 'CustomerAdminManageCustomers']);
        }
    }
}