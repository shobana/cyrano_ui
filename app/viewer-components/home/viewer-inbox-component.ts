import { Component } from 'angular2/core';
import { CORE_DIRECTIVES } from 'angular2/common';

import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from 'angular2/router';
import {Location, Router} from "angular2/router";

import { ViewerNewProgramsComponent } from './viewer-newprograms-component';
import { ViewerCompletedProgramsComponent } from './viewer-completedprograms-component';

@Component({
    templateUrl: 'views/viewer/home/inbox.html',
    directives: [ROUTER_DIRECTIVES]
})

@RouteConfig([
    { path: '/newPrograms', name: 'NewPrograms', component: ViewerNewProgramsComponent },
    { path: '/completedPrograms', name: 'CompletedPrograms', component: ViewerCompletedProgramsComponent }
])

export class ViewerInboxComponent {

    constructor(private location: Location, private router: Router) {
        this.location = location;
        this.router = router;
    }

    getLinkStyle(path) {
        return this.location.path().indexOf(path) > -1;
    }

    close() {
        this.router.navigate(['CreatorMyPrograms']);
    }
}
