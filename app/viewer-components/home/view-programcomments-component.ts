import { Component,EventEmitter, OnInit } from 'angular2/core';
import { CORE_DIRECTIVES } from 'angular2/common';

import { Router, ROUTER_DIRECTIVES, ROUTER_PROVIDERS, RouteConfig,Location,RouteParams } from 'angular2/router';

import {HTTP_PROVIDERS}    from 'angular2/http';
import {Http, Response} from 'angular2/http';
import {Headers, RequestOptions} from 'angular2/http';
import {HTTP_BINDINGS} from 'angular2/http';
import 'rxjs/add/operator/map';

@Component({
    templateUrl: 'views/viewer/home/viewProgramComments.html',
    directives: [ROUTER_DIRECTIVES],
    providers: [HTTP_PROVIDERS, HTTP_BINDINGS]
})

export class ViewProgramCommentsComponent {
    currentStatus: string;
    
    constructor(private location: Location, private router: Router,private http: Http,private params: RouteParams) {
        this.location = location;
        this.router = router;
        this.http=http;

        this.url = "http://techslides.com/demos/sample-videos/small.mp4";
        this.description="Cyranno is a corporate marketing communications, messaging and training platform that allows companies to optimally deliver production quality content and information to their employees in a dynamic,  easy to use, flexible solution.";
        this.commentList=[{"description":"Lorem Ipsum is simply dummy text industry.Lorum ipsum has been the ..."},{"description":"Lorem Ipsum is simply dummy text industry.Lorum ipsum has been the ..."},{"description":"Lorem Ipsum is simply dummy text industry.Lorum ipsum has been the ..."}];
        
        this.showLikeFlag=false;
        
        this.showAddCommentFlag=false;
        //this.getProgramDetails();
        
        this.currentStatus = params.get('status');
        console.log(this.currentStatus);
    }
    
    //getting entire program details 
    getProgramDetails(){
        let body = JSON.stringify({ name });
        let headers = new Headers();
        let options = new RequestOptions({ headers: headers });
        return this.http
            .post('//' + $(location).attr('hostname') + ':4000/mock/getNewProgramDetails/',
            body, options)
            .map(response => response.json())
            .subscribe(
            data => this.checkResponse(data),
            this.logError,
            () => console.log('Data fetching complete')
            );
    }   
    
    checkResponse(response) {
        this.programVO = response.programVO;
    }  
    
    showNewCommentDiv(){
        this.showAddCommentFlag=true;
    }
    
    addComment(){
        this.showAddCommentFlag=false;
    }
    
    close(){
        if(this.currentStatus==='New'  ||  this.currentStatus==='New'){
             this.router.navigate(['/CreatorHome','Inbox','NewPrograms']);
        }else if(this.currentStatus==='My'){
             this.router.navigate(['/CreatorHome','CreatorMyPrograms']);
        }else if(this.currentStatus==='MY Scripted'){
             this.router.navigate(['/CreatorHome','MyScriptedPrograms']);
        }else if(this.currentStatus==='Completed'){
             this.router.navigate(['/CreatorHome','Inbox','CompletedPrograms']);
        }else if(this.currentStatus==='All'){
             this.router.navigate(['/CreatorHome','ManagerAllProgramsManagement']);
        }
    }
}
