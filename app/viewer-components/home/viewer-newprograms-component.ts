import { Component,EventEmitter, OnInit, AfterViewInit } from 'angular2/core';
import { CORE_DIRECTIVES } from 'angular2/common';

import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from 'angular2/router';
import {Location, Router} from "angular2/router";

import {NG_TABLE_DIRECTIVES} from '/node_modules/ng2-table/ng2-table';
import {PAGINATION_DIRECTIVES} from 'ng2-bootstrap/ng2-bootstrap';
//import {NG_TABLE_DIRECTIVES} from '/node_modules/ng2-easy-table/ng2-easy-table';
import {NgTableSorting} from '/node_modules/ng2-table/components/table/ng-table-sorting.directive';

import {HTTP_PROVIDERS}    from 'angular2/http';
import {Http, Response} from 'angular2/http';
import {Headers, RequestOptions} from 'angular2/http';
import {HTTP_BINDINGS} from 'angular2/http';
import 'rxjs/add/operator/map';

@Component({
    selector: 'ngTable, [ngTable]',
    inputs: ['rows', 'columns', 'config'],
    outputs: ['tableChanged'],
    templateUrl: 'views/viewer/home/newPrograms.html',
    directives: [ROUTER_DIRECTIVES,NG_TABLE_DIRECTIVES, PAGINATION_DIRECTIVES,NgTableSorting],
    providers: [HTTP_PROVIDERS, HTTP_BINDINGS]
})

export class ViewerNewProgramsComponent implements OnInit, AfterViewInit {
    
    constructor(private location: Location, private router: Router,private http: Http) {
        this.location = location;
        this.router = router;
        this.http=http;
        
        this.page = 1;
        this.loadNewProgramsListTable(this.page);
        this._tempSelectedDivIds = [];
        //this.loadManageGroupsListTable(this.page);
        
        $('#myCarousel').carousel({
            interval: 0;
        })
        
        $('#myCarouselMostViewed').carousel({
            interval: 0;
        })
        
        $('#myCarouselAll').carousel({
            interval: 0;
        })
        $('#myCarousel1').carousel({
            interval: 0;
        })
        $('#myCarousel2').carousel({
            interval: 0;
        })
        $('#myCarousel3').carousel({
            interval: 0;
        })
        $('#myCarousel4').carousel({
            interval: 0;
        })
        $('#myCarousel5').carousel({
            interval: 0;
        })
        $('#myCarousel6').carousel({
            interval: 0;
        })
        $('#myCarousel7').carousel({
            interval: 0;
        })
    }
    
    loadNewProgramsListTable(page){
        let body = JSON.stringify({ name });
        let headers = new Headers();
        let options = new RequestOptions({ headers: headers });
        return this.http
            .post('//' + $(location).attr('hostname') + ':4000/mock/viewer_completedProgramsList/' + page,
            body, options)
            .map(response => response.json())
            .subscribe(
            data => this.checkResponseCurrent(data),
            this.logError,
            () => console.log('Data fetching complete')
            );
    }

    checkResponseCurrent(response) {
        this.completedProgramsList = response.clientResponse.programList;
        this.length = response.total;
    }  
    
    private columns: Array<any> = [
        { title: 'Name', name: 'title'},
        { title: 'Channels', name: 'channel'},
        { title: 'Author', name: 'authorName' },
        { title: 'Published Date', name: 'publishOn' },
        { title: 'Expiry Date', name: 'expiredOn' },
        { title: 'Status', name: 'status' },
        { title: 'Play' }
    ];
    
    public config: any = {
        paging: true,
        sorting: { columns: [] }
    };

    // Outputs (Events)
    public tableChanged: EventEmitter<any> = new EventEmitter();

    onChangeTable(config: any, page: any = config.paging) {
        this.loadNewProgramsListTable(page.page);
    }
    
    goToLikeCommentPage(programUUID,status){
        this.router.navigate(['/CreatorHome','ViewProgramComments', {programUUID:programUUID,status:status}]);
    }
    
    selectChannel(event){
    $("#testDivId1").removeClass("carouselSelect");         
    this.imgContentId = event.target.id;      
    this.added=false;
    this.checkFlag = false;
      
     if($.inArray( this.imgContentId, this._tempSelectedDivIds ) != -1){         
         this._tempSelectedDivIds.splice($.inArray(this.imgContentId, this._tempSelectedDivIds),1);          
         
         if($("#"+this.imgContentId).hasClass('carouselSelect')) {      
              $("#"+this.imgContentId).removeClass("carouselSelect");              
            }else{        
                    $("#"+this.imgContentId).addClass("carouselSelect");
                $.each(this._tempSelectedDivIds, function( index, value ) {                      
                    $("#"+value).removeClass("carouselSelect");          
                });
              
                this._tempSelectedDivIds.push(this.imgContentId)               
            }
         console.log("irukku..");
         this.added=true;  
    }
       
    if (!this.added) {
      console.log('illai');
      $("#"+this.imgContentId).addClass("carouselSelect");
      $.each(this._tempSelectedDivIds, function( index, value ) {
          $("#"+value).removeClass("carouselSelect");          
      });
      this._tempSelectedDivIds.push(this.imgContentId)
    }
  }
    
  ngOnInit() {
    $("#testDivId1").addClass("carouselSelect");
  }
    
    
  ngAfterViewInit(){
    //$("#channelImgUrl").addClass("channelImgUrl");
    $('#channelImgUrl').css('width', '120px');
    $('.table .test').css('display', 'none'); 
    $('#selectedCategoryView').hide();
    $('#allChannelByCategoryView').show();     
  } 
   
  //vertical horizontal display starts
      
    toggleThumbNailView(){
        $('#listView').hide();
        $('#thumbnailView').show();    
    }
    
    toggleListView(){
        $('#listView').show();
        $('#thumbnailView').hide();    
    }
    
    public types: Type[] = [
        { "id": 0, "name": "Select" },
        { "id": 1, "name": "Most Viewed" },        
        { "id": 2, "name": "Strategy & Operations" },
        { "id": 3, "name": "Engagement" },
        { "id": 4, "name": "Culture" },
        { "id": 5, "name": "Training & Development" },
        { "id": 6, "name": "Wellness" },
        { "id": 7, "name": "Performance Management" },
        { "id": 8, "name": "Recruiting & onboarding" },
        { "id": 9, "name": "Change" }
    ];
    
    onSelect(productName) {        
        if(productName =='Select'){
            $('#selectedCategoryView').hide();
            $('#allChannelByCategoryView').show();    
        }else{
            $('#selectedCategoryView').show();
            $('#allChannelByCategoryView').hide();    
        }                      
    }
  
}

/* back up----

import { Component,EventEmitter, OnInit, AfterViewInit } from 'angular2/core';
import { CORE_DIRECTIVES } from 'angular2/common';

import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from 'angular2/router';
import {Location, Router} from "angular2/router";

import {NG_TABLE_DIRECTIVES} from '/node_modules/ng2-table/ng2-table';
import {PAGINATION_DIRECTIVES} from 'ng2-bootstrap/ng2-bootstrap';
//import {NG_TABLE_DIRECTIVES} from '/node_modules/ng2-easy-table/ng2-easy-table';
import {NgTableSorting} from '/node_modules/ng2-table/components/table/ng-table-sorting.directive';

import {HTTP_PROVIDERS}    from 'angular2/http';
import {Http, Response} from 'angular2/http';
import {Headers, RequestOptions} from 'angular2/http';
import {HTTP_BINDINGS} from 'angular2/http';
import 'rxjs/add/operator/map';

@Component({
    selector: 'ngTable, [ngTable]',
    inputs: ['rows', 'columns', 'config'],
    outputs: ['tableChanged'],
    templateUrl: 'views/viewer/home/newPrograms.html',
    directives: [ROUTER_DIRECTIVES,NG_TABLE_DIRECTIVES, PAGINATION_DIRECTIVES,NgTableSorting],
    providers: [HTTP_PROVIDERS, HTTP_BINDINGS]
})

export class ViewerNewProgramsComponent implements OnInit, AfterViewInit {
    
    constructor(private location: Location, private router: Router,private http: Http) {
        this.location = location;
        this.router = router;
        this.http=http;
        
        this.page = 1;
        this.loadNewProgramsListTable(this.page);
        this._tempSelectedDivIds = [];
        this.loadManageGroupsListTable(this.page);
        //this.showHonda=false;
        
        $('#myCarousel').carousel({
            interval: 0;
        })
        
         $('#myCarouselMostViewed').carousel({
            interval: 0;
        })
        
        $('#myCarousel_1').carousel({
            interval: 0;
        })
        $('#myCarousel_2').carousel({
            interval: 0;
        })
        $('#myCarousel_3').carousel({
            interval: 0;
        })
        $('#myCarousel_4').carousel({
            interval: 0;
        })
        $('#myCarousel_5').carousel({
            interval: 0;
        })
    }
    
    loadNewProgramsListTable(page){
        let body = JSON.stringify({ name });
        let headers = new Headers();
        let options = new RequestOptions({ headers: headers });
        return this.http
            .post('//' + $(location).attr('hostname') + ':4000/mock/viewer_newProgramsList/' + page,
            body, options)
            .map(response => response.json())
            .subscribe(
            data => this.checkResponseCurrent(data),
            this.logError,
            () => console.log('Data fetching complete')
            );
    }

    checkResponseCurrent(response) {
        this.newProgramsList = response.clientResponse.programList;
        this.length = response.total;
    }  
    
    private columns: Array<any> = [
        { title: 'Name', name: 'title'},
        { title: 'Channels', name: 'channel'},
        { title: 'Author', name: 'authorName' },
        { title: 'Published Date', name: 'publishOn' },
        { title: 'Expiry Date', name: 'expiredOn' },
        { title: 'Status', name: 'status' },
        { title: 'Play' }
    ];
    
    public config: any = {
        paging: true,
        sorting: { columns: [] }
    };

    // Outputs (Events)
    public tableChanged: EventEmitter<any> = new EventEmitter();

    onChangeTable(config: any, page: any = config.paging) {
        this.loadNewProgramsListTable(page.page);
    }
    
    goToLikeCommentPage(programUUID,status){
        this.router.navigate(['/CreatorHome','ViewProgramComments', {programUUID:programUUID,status:status}]);
    }
    
    selectChannel(event){
    $("#testDivId1").removeClass("carouselSelect");         
    this.imgContentId = event.target.id;      
    this.added=false;
    this.checkFlag = false;
      
     if($.inArray( this.imgContentId, this._tempSelectedDivIds ) != -1){         
         this._tempSelectedDivIds.splice($.inArray(this.imgContentId, this._tempSelectedDivIds),1);          
         
         if($("#"+this.imgContentId).hasClass('carouselSelect')) {      
              $("#"+this.imgContentId).removeClass("carouselSelect");              
            }else{        
                    $("#"+this.imgContentId).addClass("carouselSelect");
                $.each(this._tempSelectedDivIds, function( index, value ) {                      
                    $("#"+value).removeClass("carouselSelect");          
                });
              
                this._tempSelectedDivIds.push(this.imgContentId)               
            }
         console.log("irukku..");
         this.added=true;  
    }
       
    if (!this.added) {
      console.log('illai');
      $("#"+this.imgContentId).addClass("carouselSelect");
      $.each(this._tempSelectedDivIds, function( index, value ) {
          $("#"+value).removeClass("carouselSelect");          
      });
      this._tempSelectedDivIds.push(this.imgContentId)
    }
  }
    
  ngOnInit() {
    $("#testDivId1").addClass("carouselSelect");    
    //$("#channelImgUrl").addClass("channelImgUrl");
  }
    
  ngAfterViewInit(){
    //$("#channelImgUrl").addClass("channelImgUrl");
    $('#channelImgUrl').css('width', '120px');
    $('.table .test').css('display', 'none');      
  } 
   
  //vertical horizontal display starts
    
  loadManageGroupsListTable(page) {
        let body = JSON.stringify({ name });
        let headers = new Headers();
        let options = new RequestOptions({ headers: headers });
        return this.http
            .post('//' + $(location).attr('hostname') + ':4000/mock/manageInboxChannelsList/' + page,
            body, options)
            .map(response => response.json())
            .subscribe(
            data => this.checkResponseChannels(data),
            this.logError,
            () => console.log('Data fetching complete')
            );
    }

    checkResponseChannels(response) {
        this.rows = response.groupsList;
        this.length = response.total;
    }

    private channelColumns: Array<any> = [
        { title: 'Channel', name: 'channelImgUrl' },
        { title: 'Programs', name: 'groupUUID' }        
    ];    

    // Outputs (Events)
    public tableChanged: EventEmitter<any> = new EventEmitter();

    onChangeTable(config: any, page: any = config.paging) {
        console.log(page);
        this.loadManageGroupsListTable(page.page);
    }

    changeSort(data: any, config: any) {
        console.log(this.config.sorting.columns);
    }
    
    toggleThumbNailView(){
        $('#listView').hide();
        $('#thumbnailView').show();    
    }
    
    toggleListView(){
        $('#listView').show();
        $('#thumbnailView').hide();    
    }
  
}*/
