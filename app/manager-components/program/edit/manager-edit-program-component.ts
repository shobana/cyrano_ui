import { Component, OnInit } from 'angular2/core';
import { CORE_DIRECTIVES } from 'angular2/common';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS, RouteParams } from 'angular2/router';


import { ManagerEditProgramIntroComponent } from './manager-edit-programintro-component';
import { ManagerEditThemeHeaderComponent } from './manager-edit-theme-header-component';
import { ManagerEditContentComponent } from './manager-edit-content-component';
import { ManagerEditSurveyComponent } from './manager-edit-survey-component';
import { ManagerEditThemeFooterComponent } from './manager-edit-theme-footer-component';
import { ManagerEditPreviewComponent } from './manager-edit-preview-component';
import { ManagerEditPublishProgramComponent } from './manager-edit-publishprogram-component';

import {Location,Router} from "angular2/router";

@Component({ 
  templateUrl: 'views/content-manager/program/edit/managerEditProgram.html',
  directives: [ROUTER_DIRECTIVES]
})

@RouteConfig([
  { path: '/managereditprogramintro', name: 'ManagerEditProgramIntro', component: ManagerEditProgramIntroComponent},
  { path: '/managereditthemeheader', name: 'ManagerEditThemeHeader', component: ManagerEditThemeHeaderComponent},
  { path: '/managereditsegment', name: 'ManagerEditContent', component: ManagerEditContentComponent},
  { path: '/managereditsurvey', name: 'ManagerEditSurvey', component: ManagerEditSurveyComponent},
  { path: '/managereditthemefooter', name: 'ManagerEditThemeFooter', component: ManagerEditThemeFooterComponent},
  { path: '/managereditpreview', name: 'ManagerEditPreview', component: ManagerEditPreviewComponent}
  { path: '/managereditpublishprogram/:isPrivate', name: 'ManagerEditPublishProgram', component: ManagerEditPublishProgramComponent}
])
    
export class ManagerEditProgramComponent implements OnInit {
    
    constructor(private location:Location,private router:Router, private params: RouteParams) {
        this.location=location; 
        this.router=router;
        this.isPrivateVal = params.get('isPrivate');    
    }
    
    getLinkStyle(path) {
        return this.location.path().indexOf(path) > -1;
    }
      
    close(){
        this.router.navigate(['ManagerAllProgramsManagement']); 
    }
    
    ngOnInit() {
        console.log(this.isPrivateVal);
        
         if(this.isPrivateVal==='private'){           
           $('#publicDiv').hide();
           $('#privateDiv').show();             
         }else{           
           $('#publicDiv').show();
           $('#privateDiv').hide();
         }  
    }
}
