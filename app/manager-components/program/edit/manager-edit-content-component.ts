import { Component,ViewChild } from 'angular2/core';
import {Component, EventEmitter, OnInit} from 'angular2/core';
import {CORE_DIRECTIVES, FORM_DIRECTIVES, NgClass, NgIf} from 'angular2/common';

import {PAGINATION_DIRECTIVES} from 'ng2-bootstrap/ng2-bootstrap';
import {NG_TABLE_DIRECTIVES} from '../../../../node_modules/ng2-table/ng2-table';
import {NgTableSorting} from '../../../../node_modules/ng2-table/components/table/ng-table-sorting.directive';

import {HTTP_PROVIDERS}    from 'angular2/http';
import {Http, Response} from 'angular2/http';
import {Headers, RequestOptions} from 'angular2/http';
import {HTTP_BINDINGS} from 'angular2/http';
import 'rxjs/add/operator/map';

import { Router } from 'angular2/router';

import { MODAL_DIRECTIVES } from 'ng2-bs3-modal/ng2-bs3-modal';

@Component({
    selector: 'ngTable, [ngTable]',
    inputs: ['rows', 'columns', 'config'],
    outputs: ['tableChanged'],
    templateUrl: 'views/content-manager/program/edit/managerEditContent.html',
    directives: [NG_TABLE_DIRECTIVES, PAGINATION_DIRECTIVES, NgClass, NgIf, CORE_DIRECTIVES, FORM_DIRECTIVES, NgTableSorting, NgClass,MODAL_DIRECTIVES],
    providers: [HTTP_PROVIDERS, HTTP_BINDINGS]
})

export class ManagerEditContentComponent {
    @ViewChild('recordContentModal')
    recordContentModal: ModalComponent;

    @ViewChild('playContentModal')
    playContentModal: ModalComponent;
    
    @ViewChild('showExternalLinkModal')
    showExternalLinkModal: ModalComponent;
    
    constructor(private http: Http, private router: Router) {
        this.http = http;
        this.page = 1;
        this.loadManageProgramsListTable(this.page);
        this.loadManageProgramsListTableCurrent(this.page);
        
        this.showCurrentContentSec = false;
        this.showSelectContentSec = true;
        this.showAddNewContentSec = false;

        this.ckeditorContent="This is my textarea to be replaced with CKEditor.";
        
        this.showSaveBtnFlag=false;
        
        this.showRecordDetails = true;
        
        this.showAudioRecordingBtn=true;
        
        this.sharableReadFalse=true;
        this.sharableReadTrue=false;
    }
    
    url = "http://techslides.com/demos/sample-videos/small.mp4";

    provideCurrentContentSec() {
        this.showCurrentContentSec = true;
        this.showSelectContentSec = false;
        $('#showAddNewContentSecId').hide();
        this.showSaveBtnFlag=false;
    }

    provideSelectContentSec() {
        this.showCurrentContentSec = false;
        this.showSelectContentSec = true;
        $('#showAddNewContentSecId').hide();
        this.showSaveBtnFlag=false;
    }

    provideAddNewContentSec() {
        this.showCurrentContentSec = false;
        this.showSelectContentSec = false;
        $('#showAddNewContentSecId').show();
        $('#showAudioElementsId').show();
        this.showSaveBtnFlag=true;
    }

    showAudioRecordOption() {
        this.showAudioRecordingBtn=true;
        this.showVideoRecordingBtn=false;
        this.showExternalLinkBtn=false;
        this.showBrowseBtn=false;
        
         this.showRecordDetails = false;
    }
    
    showVideoRecordOption() {
        this.showAudioRecordingBtn=false;
        this.showVideoRecordingBtn=true;
        this.showExternalLinkBtn=false;
        this.showBrowseBtn=false;
        
         this.showRecordDetails = false;
    }

    //External Lnk Functionalities-Start
    showExternalLinkOption() {
       this.showAudioRecordingBtn=false;
       this.showVideoRecordingBtn=false;
       this.showExternalLinkBtn=true;
       this.showBrowseBtn=false;
        
         this.showRecordDetails = false;
    }
    
    showExternalLnikModal(){
         this.showExternalLinkModal.open();
    }
    
    addExternalLink(){
        this.showExternalLinkModal.dismiss();
        this.showRecordDetails = true; 
    }

    //Browse Option Functionalities-Start
    showBrowseOption() {
       this.showAudioRecordingBtn=false;
       this.showVideoRecordingBtn=false;
       this.showExternalLinkBtn=false;
       this.showBrowseBtn=true;
        
       this.showRecordDetails = false;
    }
    
    /* current content grid starts*/
    loadManageProgramsListTableCurrent(page) {
        let body = JSON.stringify({ name });
        let headers = new Headers();
        let options = new RequestOptions({ headers: headers });
        return this.http
            .post('//' + $(location).attr('hostname') + ':4000/mock/currentManageContent/' + page,
            body, options)
            .map(response => response.json())
            .subscribe(
            data => this.checkResponseCurrent(data),
            this.logError,
            () => console.log('Data fetching complete')
            );
    }

    checkResponseCurrent(response) {
        this.rowsCurrent = response.myContentsList;
        this.lengthCurrent = response.total;
    }
        
    // Outputs (Events)
    public tableChanged: EventEmitter<any> = new EventEmitter();

    onChangeTable(config: any, page: any = config.paging) {
        console.log(page);
        this.loadManageProgramsListTableCurrent(page.page);
    }
    /* current content grid ends*/
    /* select content grid start*/
    loadManageProgramsListTable(page) {
        let body = JSON.stringify({ name });
        let headers = new Headers();
        let options = new RequestOptions({ headers: headers });
        return this.http
            .post('//' + $(location).attr('hostname') + ':4000/mock/manageContent/' + page,
            body, options)
            .map(response => response.json())
            .subscribe(
            data => this.checkResponse(data),
            this.logError,
            () => console.log('Data fetching complete')
            );
    }

    checkResponse(response) {
        this.rows = response.myContentsList;
        this.length = response.total;
    }

    public config: any = {
        paging: true,
        sorting: { columns: [] },
    };
    
    // Outputs (Events)
    public tableChanged: EventEmitter<any> = new EventEmitter();

    onChangeTable(config: any, page: any = config.paging) {
        console.log(page);
        this.loadManageProgramsListTable(page.page);
    }
    /* select content grid ends*/    moceNextOrPrevTabSeg(result, tableFrom) {
        console.log("result --" + result);
        this.response = result;
        if (result == 'Ok') {
            if (tableFrom === 'THEME') {
                this.router.navigate(['/CreatorHome', 'ManagerEditProgram', 'ManagerEditProgramIntro']);
            }
            if (tableFrom === 'SURVEY') {
                this.router.navigate(['/CreatorHome', 'ManagerEditProgram', 'ManagerEditPreview']);
            }

        }
    }

    moveNextOrPreviousSeg(tableName) {
        let moveTableSeg = tableName;
        console.log(moveTableSeg);
        let body = JSON.stringify({ name });
        let headers = new Headers();
        let options = new RequestOptions({ headers: headers });
        this.http
            .post('//' + $(location).attr('hostname') + ':4000/mock/login',
            body, options)
            .map(response => response.json())
            .subscribe(
            data => this.moceNextOrPrevTabSeg(data.status, moveTableSeg),
            this.logError,
            () => console.log('next tab')
            );
    }

    ngOnInit() {
        CKEDITOR.replace('editor1');
        $(".textarea").wysihtml5();
    }
    
    saveRecord() {
        CKEDITOR.instances.editor2.destroy();
        
        $('#recordContentModal').modal('hide');
        $('.modal-backdrop').remove();

        this.showRecordDetails = true;

        this.showUrl = false;
        this.showBrowse = false;
        this.showRecord = false;
    }
    
     //Recording Dialog
    openRecordContentModal() {
        this.recordContentModal.open('lg');

        this.recordingUrl = "https://rtc.ibotsystems.com/stream-recorder/";
        
        this.ckeditorContent = " Happy Work Anniversary Message Sherry, this is Jason Johnson, CEO of ABC Company.  I want to congratulate you on 10 years of service with us at ABC Company.  While we have not had the opportunity to work directly together,  I want you to know that your dedication and commitment to upholding our company values is why we are a leader in the world.  Thank you for all that you do to make the SE field sales team the #1 team in the company.  Congratulations again on a great career so far with us and I look forward to meeting you face to face one day!";

        CKEDITOR.replace('editor2');
        $(".textarea").wysihtml5();

    }

    openplayContentModal() {
        this.playContentModal.open();
    }
    
    isPrivate(){
        if(document.getElementById("isPrivate").checked){
            this.sharableReadFalse=false;
            this.sharableReadTrue=true;
        }else{
            this.sharableReadTrue=false;
            this.sharableReadFalse=true;
        }
    }
}
