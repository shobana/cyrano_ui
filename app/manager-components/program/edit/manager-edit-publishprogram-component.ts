import { Component, View, Input, Output, EventEmitter, OnInit, AfterViewInit,ViewChild} from 'angular2/core';
import {CORE_DIRECTIVES, FORM_DIRECTIVES, NgClass, NgIf, NgFor} from 'angular2/common';
import {PAGINATION_DIRECTIVES} from 'ng2-bootstrap/ng2-bootstrap';
import {HTTP_PROVIDERS}    from 'angular2/http';
import {Http, Response} from 'angular2/http';
import {Headers, RequestOptions} from 'angular2/http';
import {HTTP_BINDINGS} from 'angular2/http';
import 'rxjs/add/operator/map';
import {DatePicker} from 'ng2-datepicker/ng2-datepicker';
import {Injectable} from 'angular2/core';

import { Router } from 'angular2/router';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS,RouteParams } from 'angular2/router';

import {NG_TABLE_DIRECTIVES} from '../../../../node_modules/ng2-table/ng2-table';
import {NgTableSorting} from '../../../../node_modules/ng2-table/components/table/ng-table-sorting.directive';

import { MODAL_DIRECTIVES } from 'ng2-bs3-modal/ng2-bs3-modal';

import {TranslateService, TranslatePipe} from 'ng2-translate/ng2-translate';

@Component({
    selector: 'ngTable, [ngTable]',
    inputs: ['rows', 'columns', 'config'],
    outputs: ['tableChanged'],
    templateUrl: 'views/content-manager/program/edit/managerEditPublishProgram.html',
    directives: [DatePicker, FORM_DIRECTIVES, MODAL_DIRECTIVES, NG_TABLE_DIRECTIVES, PAGINATION_DIRECTIVES, NgClass, NgIf, CORE_DIRECTIVES, ROUTER_DIRECTIVES, NgTableSorting, NgClass],
    providers: [HTTP_PROVIDERS, HTTP_BINDINGS],
    pipes: [TranslatePipe]
})


export class ManagerEditPublishProgramComponent implements OnInit, AfterViewInit {
    date: string;
    date1: string;
    
    @ViewChild('recordContentModal')
    recordContentModal: ModalComponent;
    
    constructor(private router: Router, private http: Http, private params: RouteParams) {
        this.router = router;
        this.http = http;
        this.page = 1;
        this.loadAddUsersTable(this.page);
        this.loadGroupUsersDetailsTable(this.page);
        this.visibleAddedUsersArea = false;
        //this.groupList = [{ "name": "Strategy & Operations" }, { "name": "Engagement" }, { "name": "Culture" }, { "name": "Training & Development" }, { "name": "Wellness" },{ "name": "Performance Management" },{ "name": "Recruiting & onboarding" }];
        this.showCurrentSettingsSec = true;
        this.showChangeSettingsSec = true;

        this.showUserSettingsSec = false;
        this.showUserInfoAlert = true;
        
        //this.groupsList=[{"name":"Strategy & Operations","icon":"dist/img/channels_thumb/Change.png"},{"name":"Engagement","icon":"dist/img/channels_thumb/Engagement.png"},{"name":"Culture","icon":"dist/img/channels_thumb/Culture.png"},{"name":"Training & Development","icon":"dist/img/channels_thumb/Training.png"},{"name":"Wellness","icon":"dist/img/channels_thumb/Wellness.png"},{"name":"Performance Management","icon":"dist/img/channels_thumb/Performance.png"},{"name":"Recruiting & onboarding","icon":"dist/img/channels_thumb/Recruting.png"},{"name":"Change","icon":"dist/img/channels_thumb/Change.png"}];
        this.groupsList=[{"name":"Strategy & Operations","icon":"dist/img/channels_thumb/Change.png","status":"open"},{"name":"Engagement","icon":"dist/img/channels_thumb/Engagement.png","status":"private"},{"name":"Culture","icon":"dist/img/channels_thumb/Culture.png","status":""},{"name":"Training & Development","icon":"dist/img/channels_thumb/Training.png","status":""},{"name":"Wellness","icon":"dist/img/channels_thumb/Wellness.png","status":""},{"name":"Performance Management","icon":"dist/img/channels_thumb/Performance.png","status":""},{"name":"Recruiting & onboarding","icon":"dist/img/channels_thumb/Recruting.png","status":""},{"name":"Change","icon":"dist/img/channels_thumb/Change.png","status":""}];
        this.usersList=[{"name":"Alexandar Pierce"},{"name":"Andy Monin"},{"name":"Gopal Koratana"},{"name":"Jessica Jones"},{"name":"John Martin"},{"name":"Steve Austin"},{"name":"William Wordsworth"} ];
        
        this.loadManageProgramsListTable(this.page);
        this.headerTitle = false;
        this.footerTitle = false;
        
        this.showUrl = false;
        this.showBrowse = false;
        this.showRecord=false;
        this.defaultAudioBtn = false;
        this.styledAudioBtn = true;
        this.defaultVideoBtn = true;
        this.styledVideoBtn = false;
        this.showRecordDetails = true;
        
        //this.disableForPrivate = false;         
        this.isPrivateVal = params.get('isPrivate');        
    }

    currentSettingsSec() {
        $('#showCurrentSettingsSecId').show();
        $('#showChangeSettingsSecId').hide();
        $('#publishGroupDetailsId').hide();
        this.showUserSettingsSec = false;
    }

    changeSettingsSec() {
        $('#showCurrentSettingsSecId').hide();
        $('#showChangeSettingsSecId').show();
        $('#publishGroupDetailsId').hide();
        this.showUserSettingsSec = false;
    }

    groupSettingsSec() {
        $('#showCurrentSettingsSecId').hide();
        $('#showChangeSettingsSecId').hide();
        $('#publishGroupDetailsId').show();
        this.showUserSettingsSec = false;
    }

    userSettingsSec() {
        $('#showCurrentSettingsSecId').hide();
        $('#showChangeSettingsSecId').hide();
        $('#publishGroupDetailsId').hide();
        this.showUserSettingsSec = true;
    }

    ngOnInit() {
        $('#priorityForPrivate').hide();
        $('#headerForPublic').show();
        $('#footerForPublic').show();
        
        $('#publishedDate')
            .datepicker({
                defaultDate: new Date(),
                format: 'mm/dd/yyyy',
                autoclose: true
            }).on('show', function(e) {
                var $popup = $('.datepicker');
                $popup.click(function() { return false; });
            });


        $('#expiredDate')
            .datepicker({
                defaultDate: new Date(),
                format: 'mm/dd/yyyy',
                autoclose: true
            }).on('show', function(e) {
                var $popup = $('.datepicker');
                $popup.click(function() { return false; });
            });

        $('select[name="duallistbox_demo1[]"]').bootstrapDualListbox(
            {nonSelectedListLabel: 'Available Channels',
             selectedListLabel: 'Selected Channels'}
        );

        $('#showCurrentSettingsSecId').show();
        
        $('#inner-content-div').slimScroll({
           position: 'right',
           height: '312px',
           railVisible: true,
           alwaysVisible: true
       });
        
         $('#inner-content-div-user').slimScroll({
           position: 'right',
           height: '312px',
           railVisible: true,
           alwaysVisible: true
       });
    }

    AfterViewInit() {
        $('#showChangeSettingsSecId').hide();
    }

    mocePrevTabPublish(result) {
        this.response = result;
        if (result == 'Ok') {
            this.router.navigate(['/CreatorHome', 'AddNewProgram', 'Preview']);
        }
    }


    movePreviousPublish() {
        let body = JSON.stringify({ name });
        let headers = new Headers();
        let options = new RequestOptions({ headers: headers });
        this.http
            .post('//' + $(location).attr('hostname') + ':4000/mock/login',
            body, options)
            .map(response => response.json())
            .subscribe(
            data => this.mocePrevTabPublish(data.status),
            this.logError,
            () => console.log('next tab')
            );
    }
    static chosenInitialized = false;

    ngAfterViewInit() {        
        
        if (!ManagerEditPublishProgramComponent.chosenInitialized) {
            jQuery('#grouplist_dual').bootstrapDualListbox('refresh', true);
            ManagerEditPublishProgramComponent.chosenInitialized = true;
        }

        $('#ex2').slider({
            formatter: function(value) {
                tooltip: 'always'
            }
        });

        $("#ex2Slider").css("width", "144px");
        $(".modal-dialog").css("width", "800px");
        
       if(this.isPrivateVal==="private"){
           $('#groupSettingsForPublic').hide();
           $('#groupSettingsForPrivate').show();
           $('#userSettingsForPublic').hide();
           $('#userSettingsForPrivate').show();
           
           $('#autoPublishForPrivate').show();
           $('#autoPublishForPublic').hide();
           $('#archivedForPublic').hide();
           $('#archivedForPrivate').show();
           $('#isPrivateForPublic').hide();
           $('#isPrivateForPrivate').show();
           $('#sharableForPublic').hide();
           $('#sharableForPrivate').show();
           
           $('#priorityForPublic').hide();           
           $('#priorityForPrivate').show();
           $('#headerForPublic').hide();
           $('#footerForPublic').hide();                      
       }else{
           $('#groupSettingsForPublic').show();
           $('#groupSettingsForPrivate').hide();
           $('#userSettingsForPublic').show();
           $('#userSettingsForPrivate').hide();
                      
           $('#autoPublishForPrivate').hide();
           $('#autoPublishForPublic').show();
           $('#archivedForPublic').show();
           $('#archivedForPrivate').hide();
           $('#isPrivateForPublic').show();
           $('#isPrivateForPrivate').hide();
           $('#sharableForPublic').show();
           $('#sharableForPrivate').hide();
                      
           $('#priorityForPublic').show();           
           $('#priorityForPrivate').hide();
           $('#headerForPublic').show();
           $('#footerForPublic').show();
       }     
       
    }
    
    /* table section starts*/
    loadAddUsersTable(page) {
        let body = JSON.stringify({ name });
        let headers = new Headers();
        let options = new RequestOptions({ headers: headers });
        return this.http
            .post('//' + $(location).attr('hostname') + ':4000/mock/manageUsersList/' + page,
            body, options)
            .map(response => response.json())
            .subscribe(
            data => this.checkResponseUsers(data),
            this.logError,
            () => console.log('Data fetching complete')
            );
    }

    checkResponseUsers(response) {
        console.log(response);
        this.rows = response.UsersList;
        this.length = response.UsersList.length;
    }

    private columns: Array<any> = [
        { title: 'Select', name: 'UsersUUID' },
        { title: 'Name', name: 'name', sort: 'asc' },
        { title: 'Email', name: 'email' },
        { title: 'Title', name: 'title' },
        { title: 'Phone', name: 'phone' },
        { title: 'Status', name: 'status' }
    ];

    public config: any = {
        paging: true,
        sorting: { columns: [] },
    };

    public tableChanged: EventEmitter<any> = new EventEmitter();

    onChangeTable(config: any, page: any = config.paging) {
        console.log(page);
        this.loadAddUsersTable(page.page);
    }
       
    /* group Users details starts*/  
    loadGroupUsersDetailsTable(page) {
        let body = JSON.stringify({ name });
        let headers = new Headers();
        let options = new RequestOptions({ headers: headers });
        return this.http
            .post('//' + $(location).attr('hostname') + ':4000/mock/groupUsersDetailsList/' + page,
            body, options)
            .map(response => response.json())
            .subscribe(
            data => this.checkResponseGroupUserDetails(data),
            this.logError,
            () => console.log('Data fetching complete')
            );
    }

    checkResponseGroupUserDetails(response) {
        console.log(response);
        this.groupRows = response.groupUsersList;
        this.groupLength = response.groupUsersList.length;
    }

    private groupColumns: Array<any> = [
        { title: 'Channels', name: 'group', sort: 'asc' },
        { title: 'Users', name: 'user', sort: 'asc' }
    ];

    public tableChanged: EventEmitter<any> = new EventEmitter();

    onChangeTable(config: any, page: any = config.paging) {
        console.log(page);
        this.loadGroupUsersDetailsTable(page.page);
    }
    /* group Users details end*/

    // Outputs (Events)
    changeSort(data: any, config: any) {
        console.log(this.config.sorting.columns);
    }

    addUsersListToDiv() {
        this.showUserInfoAlert = false;
        this.visibleAddedUsersArea = true;
        this.usersList = [{ "name": "Andy" }, { "name": "John" }, { "name": "Reed" }, { "name": "William" }];
    }

    publish() {
        this.router.navigate(['/CreatorHome', 'ManagePrograms']);
    }

    public States: Type[] = [
        { "id": 1, "name": "ApprovedByAuthor" },
        { "id": 2, "name": "ReadyToPublish" },
        { "id": 3, "name": "Live" },
        { "id": 4, "name": "Expired" },
        { "id": 5, "name": "Archived" }
    ];
    
    /* selected grid starts*/
    loadManageProgramsListTable(page) {
        let body = JSON.stringify({ name });
        let headers = new Headers();
        let options = new RequestOptions({ headers: headers });
        return this.http
            .post('//' + $(location).attr('hostname') + ':4000/mock/manageThemes/' + page,
            body, options)
            .map(response => response.json())
            .subscribe(
            data => this.checkResponse(data),
            this.logError,
            () => console.log('Data fetching complete')
            );
    }

    checkResponse(response) {
        this.rowsHeader = response.myThemesList;
        this.lengthheader = response.total;
    }

    public config: any = {
        paging: true,
        sorting: { columnsHead: [] },
    };

    // Outputs (Events)
    public tableChanged: EventEmitter<any> = new EventEmitter();

    onChangeTable(config: any, page: any = config.paging) {
        console.log(page);
        this.loadManageProgramsListTable(page.page);
    }
    /* selected grid ends*/
    
    showHeaderSec(checkLabel){
        if(checkLabel === 'Footer'){
            this.headerTitle = false;
            this.footerTitle = true;       
        }else{
            this.headerTitle = true;
            this.footerTitle = false;     
        }
        $('#showSelectHeaderSec').show();
        $('#hideSelectHeaderSec').hide();
        $('#selectThemeGrid').show();
        $('#showAddNewFooterSecId').hide();            
    }
    
    showChangeSetting() {
        $('#showSelectHeaderSec').hide();
        $('#hideSelectHeaderSec').show();
        $('#showCurrentSettingsSecId').hide();
        $('#showChangeSettingsSecId').show();
        $('#publishGroupDetailsId').hide();
        this.showUserSettingsSec = false;
    }
    
    addNewTheme(){
        this.headerTitle = false;
        this.footerTitle = false;
        $('#selectThemeGrid').hide();
        $('#showAddNewFooterSecId').show();
    }
    
    showAudioElement() {
        $('#showAudioElementsId').show();
        this.showOr = false;
        this.showVideo = true;
        $('#showVideoElementsId').hide();
        this.showAudioUrl = false;
        this.showAudioBrowse = false;
        
        this.defaultAudioBtn = false;
        this.styledAudioBtn = true;
        this.defaultVideoBtn = true;
        this.styledVideoBtn = false;
    }

    showVideoElement() {
        this.showAudio = true;
        $('#showAudioElementsId').hide();
        this.showOr = false;
        this.showVideo = true;
        $('#showVideoElementsId').show();
        
        //Btn Side CSS 
        this.defaultAudioBtn = true;
        this.styledAudioBtn = false;
        this.defaultVideoBtn = false;
        this.styledVideoBtn = true;
    }
    
    showUrlOption() {
        this.showUrl = true;
        this.showBrowse = false;
        this.showRecord=false;
         this.showRecordDetails = false;
    }
    
    showBrowseOption() {
        this.showUrl = false;
        this.showBrowse = true;
        this.showRecord=false;
         this.showRecordDetails = false;
    }

    showRecordOption() {
        this.showUrl = false;
        this.showBrowse = false;
        this.showRecord=true;
         this.showRecordDetails = false;
    }
    
    saveRecord() {
        $('#recordContentModal').modal('hide');
        $('.modal-backdrop').remove();

        this.showRecordDetails = true;

        this.showUrl = false;
        this.showBrowse = false;
        this.showRecord = false;
    }
    
    //Recording Dialog
     openRecordContentModal() {
         this.recordContentModal.open('lg');
         
         this.recordingUrl="https://rtc.ibotsystems.com/stream-recorder/";
         
         this.ckeditorContent = " Happy Work Anniversary Message Sherry, this is Jason Johnson, CEO of ABC Company.  I want to congratulate you on 10 years of service with us at ABC Company.  While we have not had the opportunity to work directly together,  I want you to know that your dedication and commitment to upholding our company values is why we are a leader in the world.  Thank you for all that you do to make the SE field sales team the #1 team in the company.  Congratulations again on a great career so far with us and I look forward to meeting you face to face one day!";
         
     }
    
    publish() {
        this.router.navigate(['/CreatorHome', 'ManagerAllProgramsManagement']);
    }
}
