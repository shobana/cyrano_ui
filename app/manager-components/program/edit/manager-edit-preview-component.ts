import { Component, View, Input, Output, EventEmitter, OnInit, AfterViewInit,ViewChild} from 'angular2/core';
import {CORE_DIRECTIVES, FORM_DIRECTIVES, NgClass, NgIf, NgFor} from 'angular2/common';
import {PAGINATION_DIRECTIVES} from 'ng2-bootstrap/ng2-bootstrap';
import {HTTP_PROVIDERS}    from 'angular2/http';
import {Http, Response} from 'angular2/http';
import {Headers, RequestOptions} from 'angular2/http';
import {HTTP_BINDINGS} from 'angular2/http';
import 'rxjs/add/operator/map';
import {DatePicker} from 'ng2-datepicker/ng2-datepicker';
import {Injectable} from 'angular2/core';

import { Router } from 'angular2/router';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from 'angular2/router';

import {NG_TABLE_DIRECTIVES} from '../../../../node_modules/ng2-table/ng2-table';
import {NgTableSorting} from '../../../../node_modules/ng2-table/components/table/ng-table-sorting.directive';

import { MODAL_DIRECTIVES } from 'ng2-bs3-modal/ng2-bs3-modal';

import {DND_PROVIDERS, DND_DIRECTIVES} from 'ng2-dnd/ng2-dnd';

@Component({
    selector: 'ngTable, [ngTable]',
    inputs: ['rows', 'columns', 'config'],
    outputs: ['tableChanged'],
    templateUrl: 'views/content-manager/program/edit/managerEditPreview.html',
    directives: [DatePicker, FORM_DIRECTIVES, MODAL_DIRECTIVES, NG_TABLE_DIRECTIVES, PAGINATION_DIRECTIVES, NgClass, NgIf, CORE_DIRECTIVES, ROUTER_DIRECTIVES, NgTableSorting, NgClass,DND_DIRECTIVES],
    providers: [HTTP_PROVIDERS,HTTP_BINDINGS]    
})

export class ManagerEditPreviewComponent {

    @ViewChild('recordContentModal')
    recordContentModal: ModalComponent;
    
    @ViewChild('playContentModal')
    playContentModal: ModalComponent;
    
    @ViewChild('showExternalLinkModal')
    showExternalLinkModal: ModalComponent;
    
    constructor(private router: Router, private http: Http) {
        this.router = router;
        this.http = http;
        
        this.commentList=[{"description":"Lorem Ipsum is simply dummy text industry.Lorum ipsum has been the ..."},{"description":"Lorem Ipsum is simply dummy text industry.Lorum ipsum has been the ..."},{"description":"Lorem Ipsum is simply dummy text industry.Lorum ipsum has been the ..."}];
        
        this.showLikeFlag=false;
        
        this.showAddCommentFlag=false;
    
        this.loadManageProgramsListTable(this.page);
        this.headerTitle = false;
        this.footerTitle = false;
        
        this.showAudioRecordingBtn=true;
        this.showRecordDetails = true;
        
        //feedback sec start
        this.showMultiSelect=false;
        this.showMultiSelectEdit = true;
        this.showFreetext=false;
        this.showFreetextEdit=false;
        this.loadManageSurveyTable(this.page);
        this.loadManageSurveyAddTable(this.page);   
        
        this.QuestionDescriptionEdit = 'Town Hall Anual Meet Question One'
        this.OptionEdit1 = 'Test Option One'
        this.OptionEdit2 = 'Sample Option two'
        this.OptionEdit3 = 'Both Option One & Option Two '
        this.OptionEdit4 = 'None of Above'
        this.QuestionType = "Multiple Choice";
    }

    programName = "About Cyrano";
    description = "Cyranno is a corporate marketing communications, messaging and training platform that allows companies to optimally deliver production quality content and information to their employees in a dynamic,  easy to use, flexible solution.";
    url = "http://techslides.com/demos/sample-videos/small.mp4";
    feedbackDescription="Cyranno is a corporate marketing communications, messaging and training platform that allows companies to optimally deliver production quality content and information to their employees in a dynamic,  easy to use, flexible solution.";
    SurveyTitle = "Test Company Feedback";


    moceNextOrPrevTabPreview(result, tableFrom) {
        this.response = result;
        if (result == 'Ok') {
            if (tableFrom === 'FOOTER') {
                this.router.navigate(['/CreatorHome', 'ManagerEditProgram', 'ManagerEditContent']);
            }
            if (tableFrom === 'PUBLISH') {
                this.router.navigate(['/CreatorHome', 'ManagerEditProgram', 'ManagerEditPublishProgram',{'isPrivate':'public'}]);
            }

        }
    }


    moveNextOrPreviousPreview(tableName) {
        let moveTablePublish = tableName;
        console.log(moveTablePublish);
        let body = JSON.stringify({ name });
        let headers = new Headers();
        let options = new RequestOptions({ headers: headers });
        this.http
            .post('//' + $(location).attr('hostname') + ':4000/mock/login',
            body, options)
            .map(response => response.json())
            .subscribe(
            data => this.moceNextOrPrevTabPreview(data.status, moveTablePublish),
            this.logError,
            () => console.log('next tab')
            );
    }
    
    showNewCommentDiv(){
        this.showAddCommentFlag=true;
    }
    
    addComment(){
        this.showAddCommentFlag=false;
    }
            
    
    /*updates part starts*/
    
    /* selected grid starts*/
    loadManageProgramsListTable(page) {
        let body = JSON.stringify({ name });
        let headers = new Headers();
        let options = new RequestOptions({ headers: headers });
        return this.http
            .post('//' + $(location).attr('hostname') + ':4000/mock/manageThemes/' + page,
            body, options)
            .map(response => response.json())
            .subscribe(
            data => this.checkResponse(data),
            this.logError,
            () => console.log('Data fetching complete')
            );
    }

    checkResponse(response) {
        this.rowsHeader = response.myThemesList;
        this.lengthheader = response.total;
    }

    public config: any = {
        paging: true,
        sorting: { columnsHead: [] },
    };

    // Outputs (Events)
    public tableChanged: EventEmitter<any> = new EventEmitter();

    onChangeTable(config: any, page: any = config.paging) {
        console.log(page);
        this.loadManageProgramsListTable(page.page);
    }
    /* selected grid ends*/
    
    
    showHeaderSec(checkLabel){
        if(checkLabel === 'Footer'){
            this.headerTitle = false;
            this.footerTitle = true;       
        }else{
            this.headerTitle = true;
            this.footerTitle = false;     
        }
        $('#showSelectHeaderSec').show();
        $('#showPreviewSec').hide();
        $('#showFeedbackSec').hide();        
        $('#selectThemeGrid').show();
        $('#showAddNewFooterSecId').hide();                    
    }
    
    showFeedbackActions() {
        if($("#previewFbChkId").prop('checked') == true) {
            $('#feedbackActionsId').show();
        } else {
            $('#feedbackActionsId').hide();
        }   
    }
    
    showFeedbackEdit() {
        $('#showSelectHeaderSec').hide();
        $('#showPreviewSec').hide();
        $('#showFeedbackSec').show();
        $('#feedbackAdd').hide();
        $('#feedbackEdit').show();        
    }
    
    showFeedbackAdd() {
        $('#showSelectHeaderSec').hide();
        $('#showPreviewSec').hide();
        $('#showFeedbackSec').show();
        $('#feedbackEdit').hide();
        $('#feedbackAdd').show();        
    }
    
    showPreviewPage(){
        $('#showSelectHeaderSec').hide();
        $('#showPreviewSec').show();
        $('#showFeedbackSec').hide();
    }
    
    showChangeSetting() {
        $('#showSelectHeaderSec').hide();
        $('#showPreviewSec').show();
        $('#showFeedbackSec').hide();        
    }
    
    addNewTheme(){
        this.headerTitle = false;
        this.footerTitle = false;
        $('#selectThemeGrid').hide();
        $('#showAddNewFooterSecId').show();
    }
    
    showAudioRecordOption() {
        this.showAudioRecordingBtn=true;
        this.showVideoRecordingBtn=false;
        this.showExternalLinkBtn=false;
        this.showBrowseBtn=false;
        
         this.showRecordDetails = false;
    }
    
    showVideoRecordOption() {
        this.showAudioRecordingBtn=false;
        this.showVideoRecordingBtn=true;
        this.showExternalLinkBtn=false;
        this.showBrowseBtn=false;
        
         this.showRecordDetails = false;
    }

    //External Lnk Functionalities-Start
    showExternalLinkOption() {
       this.showAudioRecordingBtn=false;
       this.showVideoRecordingBtn=false;
       this.showExternalLinkBtn=true;
       this.showBrowseBtn=false;
        
         this.showRecordDetails = false;
    }
    
    showExternalLnikModal(){
         this.showExternalLinkModal.open();
    }
    
    addExternalLink(){
        this.showExternalLinkModal.dismiss();
        this.showRecordDetails = true; 
    }

    //Browse Option Functionalities-Start
    showBrowseOption() {
       this.showAudioRecordingBtn=false;
       this.showVideoRecordingBtn=false;
       this.showExternalLinkBtn=false;
       this.showBrowseBtn=true;
        
       this.showRecordDetails = false;
    }
    
    saveRecord() {
        $('#recordContentModal').modal('hide');
        $('.modal-backdrop').remove();

        this.showRecordDetails = true;

        this.showUrl = false;
        this.showBrowse = false;
        this.showRecord = false;
    }
    
    //Recording Dialog
     openRecordContentModal() {
         this.recordContentModal.open('lg');
         
         this.recordingUrl="https://rtc.ibotsystems.com/stream-recorder/";
         
         this.ckeditorContent = " Happy Work Anniversary Message Sherry, this is Jason Johnson, CEO of ABC Company.  I want to congratulate you on 10 years of service with us at ABC Company.  While we have not had the opportunity to work directly together,  I want you to know that your dedication and commitment to upholding our company values is why we are a leader in the world.  Thank you for all that you do to make the SE field sales team the #1 team in the company.  Congratulations again on a great career so far with us and I look forward to meeting you face to face one day!";
         
     }
    
    
    /* feedback edit start*/
    
    loadManageSurveyTable(page){
    let body = JSON.stringify({ name });
    let headers = new Headers();
    let options = new RequestOptions({headers: headers });
    return this.http
        .post('//'+$(location).attr('hostname')+':4000/mock/manageSurveyList/'+page,
           body,options)
            .map(response => response.json())
            .subscribe(
               data => this.checkResponsefeedcack(data),
             this.logError,
            () => console.log('Data fetching complete')
    );
  } 
       
  checkResponsefeedcack(response){
     console.log(response);
     this.fbrows= response.QuestionVO;
     this.fblength =response.QuestionVO.length;
  }
        
   private fbcolumns:Array<any> = [
    {title: 'Type', name: 'type',sort: 'asc'},
    {title: 'Question', name: 'question'},
    {title: 'Correct Option', name: 'correctOption'},
    {title: 'Option 1', name: 'option1'},
    {title: 'Option 2', name: 'option2'},
    {title: 'Option 3', name: 'option3'},
    {title: 'Option 4', name: 'option4'},
    {title: 'Action'}
  ];
    
  public config:any = {
    paging: true,
    sorting: {columns: []},
  };

  // Outputs (Events)
  public tableChanged:EventEmitter<any> = new EventEmitter();

   onChangeTable(config:any, page:any = config.paging) {
        console.log(page);
        this.loadManageSurveyTable(page.page);
   }
    
   changeSort(data:any, config:any) {
       console.log(this.config.sorting.columns);
    }
    
    /*feedback edit ends*/
    
    /* feedback Add start*/
    
    loadManageSurveyAddTable(page){
    let body = JSON.stringify({ name });
    let headers = new Headers();
    let options = new RequestOptions({headers: headers });
    return this.http
        .post('//'+$(location).attr('hostname')+':4000/mock/manageSurveyAddList/'+page,
           body,options)
            .map(response => response.json())
            .subscribe(
               data => this.checkResponsefeedcackAdd(data),
             this.logError,
            () => console.log('Data fetching complete')
    );
  } 
       
  checkResponsefeedcackAdd(response){
     console.log(response);
     this.fbAddrows= response.QuestionVO;
     this.fbAddlength =response.QuestionVO.length;
  }
        
   private fbAddcolumns:Array<any> = [
    {title: 'Type', name: 'type',sort: 'asc'},
    {title: 'Question', name: 'question'},
    {title: 'Correct Option', name: 'correctOption'},
    {title: 'Option 1', name: 'option1'},
    {title: 'Option 2', name: 'option2'},
    {title: 'Option 3', name: 'option3'},
    {title: 'Option 4', name: 'option4'},
    {title: 'Action'}
  ];
    
  public config:any = {
    paging: true,
    sorting: {columns: []},
  };

  // Outputs (Events)
  public tableChanged:EventEmitter<any> = new EventEmitter();

   onChangeTable(config:any, page:any = config.paging) {
        console.log(page);
        this.loadManageSurveyAddTable(page.page);
   }
    
   changeSort(data:any, config:any) {
       console.log(this.config.sorting.columns);
    }
    
    /*feedback Add ends*/
    
     public types: Type[] = [
      { "id": 1, "name": "Select Type"},
      { "id": 2, "name": "Multiple Choice" },
      { "id": 3, "name": "Free Text" }      
    ];
    
    public editTypes: Type[] = [
      { "id": 1, "name": "Select Type"},
      { "id": 2, "name": "Multiple Choice" },
      { "id": 3, "name": "Free Text" }      
    ];
    
    onSelect(productName) {
        if (productName == 'Select Type') {
            this.showMultiSelect=false;
            this.showFreetext=false;
          } 
        if (productName == 'Multiple Choice') {
            this.showMultiSelect=true;
            this.showFreetext=false;
          }
        if(productName == 'Free Text'){
            this.showMultiSelect=false;
            this.showFreetext=true;
          }
    }
    
    onSelectEdit(productName) {
        if (productName == 'Select Type') {
            this.showMultiSelectEdit=false;
            this.showFreetextEdit=false;
          } 
        if (productName == 'Multiple Choice') {
            this.showMultiSelectEdit=true;
            this.showFreetextEdit=false;
          }
        if(productName == 'Free Text'){
            this.showMultiSelectEdit=false;
            this.showFreetextEdit=true;
          }
    }  
    
    //Recording Dialog
     openRecordingModal() {
         this.recordContentModal.open('lg');
       
         this.recordingUrl="https://rtc.ibotsystems.com/stream-recorder/";
     }
    
    openplayContentModal() {
        this.playContentModal.open();
    }
    
    setPriority(){
          console.log("After DND"+JSON.stringify(this.fbrows));
    }
}
