import { Component,View, Input, Output, EventEmitter} from 'angular2/core';
import { Router } from 'angular2/router';
import {HTTP_PROVIDERS}    from 'angular2/http';
import {Http, Response} from 'angular2/http';
import {Headers, RequestOptions} from 'angular2/http';
import {HTTP_BINDINGS} from 'angular2/http';
import 'rxjs/add/operator/map';

@Component({ 
  templateUrl: 'views/content-manager/program/edit/managerEditProgramIntro.html',
    providers: [HTTP_PROVIDERS,HTTP_BINDINGS]
})
    
export class ManagerEditProgramIntroComponent {
        
    constructor (private router:Router,private http:Http) {
        this.router=router;
        this.http=http;
        this.getProgramIntroDetails();
    }
    
    getProgramIntroDetails(){
         this.http
            .get('/app/mock/getManagerProgramIntroDetails.json')
            .map(response => response.json())
            .subscribe(
             data => this.checkProgramIntroResponse(data.programIntroVO),
             this.logError,
            () => console.log('GetManagerProgramIntroDetails Success')
         );
    }

    checkProgramIntroResponse(result) {
        this.programIntroVO=JSON.stringify(result);
        this.programName=result.programName;
        this.description=result.description;
        this.tag=result.tag;
        this.publisherName=result.publisherName;
        this.title=result.title;
        this.filename=result.filename;
        console.log(" GetManagerProgramIntroDetails--"+result.programName);
     }
    
    checkNextTab(result, tableFrom) {
        console.log("result --"+result);
        this.response=result;
         if(result=='Ok'){
            if(tableFrom==='THEME'){
                this.router.navigate(['/CreatorHome','ManagerEditProgram','ManagerEditContent']);    
            }
             
         }
     }

    moveToThemeTab(tableName) {
        let moveTableTo = tableName;
        console.log(moveTableTo);        
        let body = JSON.stringify({ name });
        let headers = new Headers();
        let options = new RequestOptions({ headers: headers });
        this.http
            .post('//'+$(location).attr('hostname')+':4000/mock/login',
             body,options)
            .map(response => response.json())
            .subscribe(
             data => this.checkNextTab(data.status, moveTableTo),
             this.logError,
            () => console.log('next tab')
         );
     }   
    
    readUrl($event): void {
        this.postFile($event.target);
    }
    
    postFile(inputValue: any): void {

    var formData = new FormData();
    formData.append("name", "Name");
    formData.append("file",  inputValue.files[0]);
    console.log(inputValue.files[0].name);
        this.filename=inputValue.files[0].name;
    if (inputValue.files && inputValue.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(200);
            };

            reader.readAsDataURL(inputValue.files[0]);
      }
      /*this.http.post(this.url +,
       formData ,
         {
            headers: this.headers

         });*/
     }
    
}
