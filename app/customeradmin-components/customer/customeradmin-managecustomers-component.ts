import {Component, EventEmitter, OnInit,ViewChild} from 'angular2/core';
import {CORE_DIRECTIVES, FORM_DIRECTIVES, NgClass, NgIf} from 'angular2/common';
import {PAGINATION_DIRECTIVES} from 'ng2-bootstrap/ng2-bootstrap';

import {NG_TABLE_DIRECTIVES} from '../../../node_modules/ng2-table/ng2-table';

import {HTTP_PROVIDERS}    from 'angular2/http';
import {Http, Response} from 'angular2/http';
import {Headers, RequestOptions} from 'angular2/http';
import {HTTP_BINDINGS} from 'angular2/http';
import 'rxjs/add/operator/map';
import {Injectable} from 'angular2/core';

import { Router } from 'angular2/router';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from 'angular2/router';
import {NgTableSorting} from '../../../node_modules/ng2-table/components/table/ng-table-sorting.directive';
import {CORE_DIRECTIVES, FORM_DIRECTIVES, NgClass, NgFor} from 'angular2/common';

import { MODAL_DIRECTIVES } from 'ng2-bs3-modal/ng2-bs3-modal';

@Component({
  selector: 'ngTable, [ngTable]',
  inputs: ['rows', 'columns', 'config'],
  outputs: ['tableChanged'],
  templateUrl: '/views/admin/customer/manageCustomers.html',  
  directives: [NG_TABLE_DIRECTIVES, PAGINATION_DIRECTIVES, NgClass, NgIf, CORE_DIRECTIVES, FORM_DIRECTIVES,ROUTER_DIRECTIVES,NgTableSorting, NgClass,MODAL_DIRECTIVES],
  providers: [HTTP_PROVIDERS,HTTP_BINDINGS]
})
   
export class CustomerAdminManageCustomersComponent implements OnInit {
   
   @ViewChild('groupDetailsModal')
   groupDetailsModal: GriupDetailsModalComponent;
    
   // Table values
  constructor(private http:Http,private router:Router) {
    this.http=http;
    this.router=router;
    this.page=1;
    this.loadManageCustomersListTable(this.page);
  }
  
  loadManageCustomersListTable(page){
    let body = JSON.stringify({ name });
    let headers = new Headers();
    let options = new RequestOptions({headers: headers });
    return this.http
        .post('//'+$(location).attr('hostname')+':4000/mock/adminManageCustomerList/'+page,
           body,options)
            .map(response => response.json())
            .subscribe(
               data => this.checkResponse(data),
             this.logError,
            () => console.log('Data fetching complete')
    );
  } 
       
  checkResponse(response){
     this.customersList= response.clientResponse.customersList;
     this.length =response.total;
  }
        
   private columns:Array<any> = [
    {title: 'Name', name: 'customerName'},
    {title: 'Start Date', name: 'startDate'},
    {title: '# Groups', name: 'groupCount'},
    {title: 'Expiry Period', name: 'expiryPeriod'},
    {title: 'Archive Period', name: 'archivePeriod'},
    {title: 'Purge Period', name: 'purgePeriod'},
    {title: 'Active', name: 'isActive'},
    {title: 'Action'}
  ];
    
  public config:any = {
    paging: true,
    sorting: {columns: []},
  };

  // Outputs (Events)
  public tableChanged:EventEmitter<any> = new EventEmitter();

   onChangeTable(config:any, page:any = config.paging) {
        this.loadManageCustomersListTable(page.page);
   }
    
   changeSort(data:any, config:any) {
       console.log(this.config.sorting.columns);
   }
  
  openGroupDetailsModal(){
        
        this.groupDetailsModal.open();
      
        this.customerName="SMI";
        this.groupNamesList= [
            "HR",
            "Technichal",
            "Admin","Sales","Fun"
        ];
       
  }
    
  editCustomer(customerUUID,status){
        this.router.navigate(['SuperAdminEditCustomer',{ customerUUID:customerUUID,status:status}]);
  }
    
}