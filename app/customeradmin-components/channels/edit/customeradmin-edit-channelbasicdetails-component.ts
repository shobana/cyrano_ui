import { Component,EventEmitter, OnInit,ViewChild , OnInit, AfterViewInit} from 'angular2/core';
import { CORE_DIRECTIVES } from 'angular2/common';

import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS,RouteParams } from 'angular2/router';
import {Location, Router} from "angular2/router";

import {HTTP_PROVIDERS}    from 'angular2/http';
import {Http, Response} from 'angular2/http';
import {Headers, RequestOptions} from 'angular2/http';
import {HTTP_BINDINGS} from 'angular2/http';
import 'rxjs/add/operator/map';

import { MODAL_DIRECTIVES } from 'ng2-bs3-modal/ng2-bs3-modal';

@Component({
    templateUrl: 'views/admin/channels/edit/channelDetails.html',
    directives: [ROUTER_DIRECTIVES,MODAL_DIRECTIVES],
    providers: [HTTP_PROVIDERS, HTTP_BINDINGS]
})

export class CustomerAdminEditChannelBasicDetailsComponent implements OnInit, AfterViewInit {
    
    @ViewChild('isPrivateModal')
    isPrivateModal: ModalComponent;
    
    constructor(private location: Location, private router: Router,private http: Http, private params: RouteParams) {
        this.location = location;
        this.router = router;
        this.http=http;
        
        this.channelUUID = params.get('channelUUID');
        this.isExternalFlag = params.get('isExternalFlag');
    }

    ngOnInit() {
       if( this.isExternalFlag=='Yes'){
           $("input").attr('disabled', true);
           $("textarea").attr('disabled', true);
       }
    }
    
    //update channel reg basic details
    addChannelBasicDetails(){
        this.router.navigate(['CustomerAdminEditChannelSubscriberDetails',{channelUUID:this.channelUUID,isExternalFlag:this.isExternalFlag}]);    
    }
    
    isPrivate(value){
        if (document.getElementById('isPrivate').checked == true) {
           $("#isExternalSubscription").attr("disabled", true);
           this.isPrivateModal.open();
        }
        else if (document.getElementById('isPrivate').checked == false){
           $("#isExternalSubscription").removeAttr("disabled");
           $('#isPrivateModal').modal('hide');
        }
    }
    
    //Upload Group Logo
    readUrl($event): void {
        this.postFile($event.target);
    }
    
    postFile(inputValue: any): void {

    var formData = new FormData();
    formData.append("name", "Name");
    formData.append("file",  inputValue.files[0]);
    console.log(inputValue.files[0].name);
        this.filename=inputValue.files[0].name;
    if (inputValue.files && inputValue.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(200);
            };

            reader.readAsDataURL(inputValue.files[0]);
      }
     }
}
