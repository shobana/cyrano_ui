import { Component } from 'angular2/core';
import { CORE_DIRECTIVES } from 'angular2/common';

import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS,RouteParams} from 'angular2/router';
import {Location, Router} from "angular2/router";

import { CustomerAdminEditChannelBasicDetailsComponent } from './customeradmin-edit-channelbasicdetails-component';
import { CustomerAdminEditChannelSubscriberDetailsComponent } from './customeradmin-edit-channelsubscribers-component';
import { CustomerAdminEditChannelPublisherDetailsComponent } from './customeradmin-edit-channelpublishers-component';

@Component({
    templateUrl: 'views/admin/channels/edit/editChannels.html',
    directives: [ROUTER_DIRECTIVES]
})

@RouteConfig([
    { path: '/editBasicdetails/:channelUUID/:isExternalFlag/', name: 'CustomerAdminEditChannelBasicDetails', component: CustomerAdminEditChannelBasicDetailsComponent},
    { path: '/editSubscriberDetails/:channelUUID/:isExternalFlag/', name: 'CustomerAdminEditChannelSubscriberDetails', component: CustomerAdminEditChannelSubscriberDetailsComponent},  
    { path: '/editPublisherDetails/:channelUUID/:isExternalFlag/', name: 'CustomerAdminEditChannelPublisherDetails', component: CustomerAdminEditChannelPublisherDetailsComponent}
])

export class CustomerAdminEditChannelsComponent {

    constructor(private location: Location, private router: Router, private params: RouteParams) {
        this.location = location;
        this.router = router;
        
        this.currentChannelUUID = params.get('channelUUID');
        this.isExternalFlag = params.get('isExternalFlag');
    }

    getLinkStyle(path) {
        return this.location.path().indexOf(path) > -1;
    }

    close() {
        this.router.navigate(['CustomerAdminManageChannels']);
    }
}
