import { Component,EventEmitter, OnInit,ViewChild } from 'angular2/core';
import { CORE_DIRECTIVES } from 'angular2/common';

import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from 'angular2/router';
import {Location, Router} from "angular2/router";

import {HTTP_PROVIDERS}    from 'angular2/http';
import {Http, Response} from 'angular2/http';
import {Headers, RequestOptions} from 'angular2/http';
import {HTTP_BINDINGS} from 'angular2/http';
import 'rxjs/add/operator/map';

import { MODAL_DIRECTIVES } from 'ng2-bs3-modal/ng2-bs3-modal';
import {ImageCropperComponent, CropperSettings} from '../../../node_modules/ng2-img-cropper/src/imageCropper';

import {TruncatePipe} from '../../truncate/truncate';

@Component({
    templateUrl: 'views/admin/channels/channelDetails.html',
    directives: [ROUTER_DIRECTIVES,MODAL_DIRECTIVES, ImageCropperComponent],
    providers: [HTTP_PROVIDERS, HTTP_BINDINGS],
    pipes: [TruncatePipe]
})

export class CustomerAdminChannelBasicDetailsComponent implements OnInit {
        
    data: any;
    cropperSettings: CropperSettings;
    
    @ViewChild('isPrivateModal')
    isPrivateModal: ModalComponent;
    
    constructor(private location: Location, private router: Router,private http: Http) {
        this.location = location;
        this.router = router;
        this.http=http;
        
        this.cropperSettings = new CropperSettings();
        this.cropperSettings.width = 100;
        this.cropperSettings.height = 100;
        this.cropperSettings.croppedWidth =100;
        this.cropperSettings.croppedHeight = 100;
        this.cropperSettings.canvasWidth = 400;
        this.cropperSettings.canvasHeight = 300;
 
        this.data = {};
        
        this.isImgUploaded = true;
        this.isImgEditable = false;
        this.channelName = 'Performance & Management';                        
    }
    
    changeBtnShow(){
        this.isImgUploaded = false;
        this.isImgEditable = true;           
     }
    
    addChannelBasicDetails(){
        this.router.navigate(['CustomerAdminChannelSubscriberDetails']);    
    }
    
    cancel(){
        this.router.navigate(['CustomerAdminManageChannels']);
    }
    
    isPrivate(value){
        if (document.getElementById('isPrivate').checked == true) {
           $("#isExternalSubscription").attr("disabled", true);
           this.isPrivateModal.open();
        }
        else if (document.getElementById('isPrivate').checked == false){
           $("#isExternalSubscription").removeAttr("disabled");
           $('#isPrivateModal').modal('hide');
        }
    }
    
    //Upload Group Logo
    readUrl($event): void {
        this.postFile($event.target);
    }
    
    postFile(inputValue: any): void {

    var formData = new FormData();
    formData.append("name", "Name");
    formData.append("file",  inputValue.files[0]);
    console.log(inputValue.files[0].name);
        this.filename=inputValue.files[0].name;
    if (inputValue.files && inputValue.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(200);
            };

            reader.readAsDataURL(inputValue.files[0]);
      }
     }
    
    ngOnInit() {
        /* this.titleImg = '/dist/img/default-channel.jpg';
         let canvas = document.getElementById('memecanvas');
         let ctx = canvas.getContext('2d');
         
          let deviceWidth = window.innerWidth;
          let canvasWidth = Math.min(400, deviceWidth-20);
          let canvasHeight = Math.min(400, deviceWidth-20);
         
          canvas.width = canvasWidth;
          canvas.height = canvasHeight;
        
         let img = document.getElementById('start-image');
         img.onload = function() {            
           let x = canvas.width/2 - img.width/2;
           let y = canvas.height/2 - img.height/2;         
            
            ctx.drawImage(img, x, y);
          }
        
           
        let scale = document.getElementById('scale');
        scale.addEventListener('change', this.doTransform, false);*/
    }
    
   /* doTransform() {
        let canvas = document.getElementById('memecanvas');
        let ctx = canvas.getContext('2d');
        let img = document.getElementById('start-image');
        console.log(img);
        let x = canvas.width/2 - img.width/2;
        let y = canvas.height/2 - img.height/2; 
        ctx.save();
     
        // Clear canvas
        ctx.clearRect(0, 0, canvas.width, canvas.height);
     
        // Translate to center so transformations will apply around this point
        ctx.translate(canvas.width/2, canvas.height/2);
     
        // Perform scale
        var val = document.getElementById('scale').value;
        ctx.scale(val, val);
     
        // Perform rotation
        val = document.getElementById('rotate').value;
        ctx.rotate(val*Math.PI/180);
     
        // Reverse the earlier translation
        ctx.translate(-canvas.width/2, -canvas.height/2);
     
        // Finally, draw the image
        ctx.drawImage(img, x, y);
     
        ctx.restore();
        
        ctx.lineWidth  = 5;
        ctx.font = '20pt sans-serif';
        ctx.strokeStyle = 'black';
        ctx.fillStyle = 'yellow';
        ctx.textAlign = 'center';
        ctx.lineJoin = 'round';
     
        // Draw the text
        var text = document.getElementById('custom-text').value;
        text = text.toUpperCase();
        x = canvas.width/2;
        y = canvas.height - canvas.height/4.5;
        ctx.strokeText(text, x, y);
        ctx.fillText(text, x, y);        
        
    }
    
imageLoader() {
        let canvas = document.getElementById('memecanvas');
        let ctx = canvas.getContext('2d')
               
        var reader = new FileReader();
        reader.onload = function(event) {
          let img = new Image();
          img.onload = function(){
            ctx.drawImage(img,0,0);
          }
          img.src = reader.result;
          this.titleImg = img.src; 
        }
        reader.readAsDataURL(fileInput.files[0]);
    }

saveTitledImage(){
        let download = document.getElementById('img-download');
        let canvas = document.getElementById('memecanvas');
        var data = canvas.toDataURL();         
        download.href = data;    
    }
titledImageContentShow(){
        let download = document.getElementById('img-download');
        let canvas = document.getElementById('memecanvas');
        var data = canvas.toDataURL();
        this.titiledImage = data;

        this.isImgUploaded = false;
        this.isImgEditableBtn = true;
        this.isImgEditableDiv = false;
        this.isTitleonImg = true;
        this.isTitledonImage = true; 
    }*/
}

    