import { Component } from 'angular2/core';
import { CORE_DIRECTIVES } from 'angular2/common';

import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from 'angular2/router';
import {Location, Router} from "angular2/router";

import { CustomerAdminChannelBasicDetailsComponent } from './customeradmin-channelbasicdetails-component';
import { CustomerAdminChannelSubscriberDetailsComponent } from './customeradmin-channelsubscribers-component';
import { CustomerAdminChannelPublisherDetailsComponent } from './customeradmin-channelpublishers-component';

@Component({
    templateUrl: 'views/admin/channels/addChannels.html',
    directives: [ROUTER_DIRECTIVES]
})

@RouteConfig([
    { path: '/basicdetails', name: 'CustomerAdminChannelBasicDetails', component: CustomerAdminChannelBasicDetailsComponent},
    { path: '/subscriberDetails', name: 'CustomerAdminChannelSubscriberDetails', component: CustomerAdminChannelSubscriberDetailsComponent},  
    { path: '/publisherDetails', name: 'CustomerAdminChannelPublisherDetails', component: CustomerAdminChannelPublisherDetailsComponent}
])

export class CustomerAdminAddChannelsComponent {

    constructor(private location: Location, private router: Router) {
        this.location = location;
        this.router = router;
    }

    getLinkStyle(path) {
        return this.location.path().indexOf(path) > -1;
    }

    close() {
        this.router.navigate(['CustomerAdminManageChannels']);
    }
    
    add(){
        this.router.navigate(['/CreatorHome','CustomerAdminAddChannels','CustomerAdminChannelBasicDetails']);
    }
}
