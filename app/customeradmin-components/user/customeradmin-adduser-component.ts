import {Component, EventEmitter, OnInit, AfterViewInit} from 'angular2/core';

import {HTTP_PROVIDERS}    from 'angular2/http';
import {Http, Response} from 'angular2/http';
import {Headers, RequestOptions} from 'angular2/http';
import {HTTP_BINDINGS} from 'angular2/http';
import 'rxjs/add/operator/map';
import {Injectable} from 'angular2/core';

import { Router } from 'angular2/router';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from 'angular2/router';
import {ImageCropperComponent, CropperSettings} from '../../../node_modules/ng2-img-cropper/src/imageCropper';
import { MODAL_DIRECTIVES } from 'ng2-bs3-modal/ng2-bs3-modal';

@Component({
    selector: 'test-app',
    templateUrl: '/views/admin/user/addUsers.html',
    directives: [ROUTER_DIRECTIVES, ImageCropperComponent, MODAL_DIRECTIVES],
    providers: [HTTP_PROVIDERS, HTTP_BINDINGS]
})

export class CustomerAdminAddUsersComponent implements OnInit, AfterViewInit {
        
    data: any;
    cropperSettings: CropperSettings;
    // Table  values
    constructor(private http: Http, private router: Router) {
        this.http = http;
        this.router = router;

        this.contentPriority = 50;
        this.roleName = "Viewer";
        
        this.cropperSettings = new CropperSettings();
        this.cropperSettings.width = 100;
        this.cropperSettings.height = 100;
        this.cropperSettings.croppedWidth =100;
        this.cropperSettings.croppedHeight = 100;
        this.cropperSettings.canvasWidth = 400;
        this.cropperSettings.canvasHeight = 300; 
        this.data = {};        
        this.isImgUploaded = true;
        this.isImgEditable = false;
    }

    ngOnInit() {
        $('#inner-content-div-group').slimScroll({
            position: 'right',
            height: '100px',
            railVisible: true,
            alwaysVisible: true
        });

        $('select[name="additionalGroups[]"]').bootstrapDualListbox(
            {nonSelectedListLabel: 'Available Channels',
             selectedListLabel: 'Selected Channels'}
        );

        $('select[name="additionalGroups1[]"]').bootstrapDualListbox(
            {nonSelectedListLabel: 'Available Roles',
            selectedListLabel: 'Selected Roles',
             selectedValues:'Viewer'
        }
        );
        
        $('select[name="additionalGroups_publish[]"]').bootstrapDualListbox(
            {nonSelectedListLabel: 'Available Channels',
             selectedListLabel: 'Selected Channels'}    
        );

        //this.getuserDetailsById();

        this.rolesVOList = [{ "name": "Content Creator" }, { "name": "Content Manager" }, { "name": "Admin" }, { "name": "Viewer" }];

        this.groupsList=[{"name":"Strategy & Operations","icon":"dist/img/channels_thumb/Change.png"},{"name":"Engagement","icon":"dist/img/channels_thumb/Engagement.png"},{"name":"Culture","icon":"dist/img/channels_thumb/Culture.png"},{"name":"Training & Development","icon":"dist/img/channels_thumb/Training.png"},{"name":"Wellness","icon":"dist/img/channels_thumb/Wellness.png"},{"name":"Performance Management","icon":"dist/img/channels_thumb/Performance.png"},{"name":"Recruiting & onboarding","icon":"dist/img/channels_thumb/Recruting.png"},{"name":"Change","icon":"dist/img/channels_thumb/Change.png"}];
        
        this.roleName='Viewer';
    }

    ngAfterViewInit() {
        if (!CustomerAdminAddUsersComponent.chosenInitialized) {
            jQuery('#grouplist_dual').bootstrapDualListbox('refresh', true);
            jQuery('#grouplist_dual_roles').bootstrapDualListbox('refresh', true);
            jQuery('#grouplist_dual_publish').bootstrapDualListbox('refresh', true);
            CustomerAdminAddUsersComponent.chosenInitialized = true;
        }
    }

    readUrl($event): void {
        this.postFile($event.target);
    }

    postFile(inputValue: any): void {
        var formData = new FormData();
        formData.append("name", "Name");
        formData.append("file", inputValue.files[0]);
        console.log(inputValue.files[0].name);
        this.filename = inputValue.files[0].name;
        if (inputValue.files && inputValue.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#blah')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(200);
            };

            reader.readAsDataURL(inputValue.files[0]);
        }
    }
    
    next(){
         this.router.navigate(['/CreatorHome','SuperAdminInviteUser',{roleName:'Admin'}]);
    }
    
    cancel(){
        this.router.navigate(['/CreatorHome','SuperAdminManageUsers',{roleName:'Admin'}]);
    }
    
    getRoleName(){
           console.log("Selected Rolename :"+roleName);    
    }
    
    changeBtnShow(){
        this.isImgUploaded = false;
        this.isImgEditable = true;     
     }
     
}