import { Component, View, Input, Output, EventEmitter,provide } from 'angular2/core';
import { Router } from 'angular2/router';
import {HTTP_PROVIDERS}    from 'angular2/http';
import {Http, Response} from 'angular2/http';
import {Headers, RequestOptions} from 'angular2/http';
import {HTTP_BINDINGS} from 'angular2/http';
import 'rxjs/add/operator/map';
import {Injectable} from 'angular2/core';

//import {FORM_DIRECTIVES,CORE_DIRECTIVES,FormBuilder, Validators } from 'angular2/common';

import {TranslateService, TranslatePipe} from 'ng2-translate/ng2-translate';

@Component({
    selector: 'login-selector',
    templateUrl: 'views/common/login.html',
    providers: [HTTP_PROVIDERS, HTTP_BINDINGS],
    pipes: [TranslatePipe]
})

export class LoginComponent {

    username = "testcreator@cyranosystems.com";
    password = "test1234";

    constructor(private router: Router, private http: Http) {
        this.router = router;
        this.http = http;
        this.getVersion();
        
        //this.loginForm = fb.group({
           // name: ["", Validators.required],
            //password: ["", Validators.required],fb: FormBuilder
        //});
    }

    getVersion() {
        //Get  Call
        this.http
            .get('/locale/env_properties.json')
            .map(response => response.json())
            .subscribe(
            data => this.checkVersion(data.reportPath),
            this.logError,
            () => console.log('Version Check Complete')
            );
    }

    checkVersion(result) {
        this.version = result.version;
        console.log("version --" + this.version);
    }

    onSubmit() {
        //http://localhost/cyrano_ui_v01/cyrano-ui
        //POST Examplehttp://staging.cyranosystems.com:4000/mock/login
        let body = JSON.stringify({ name });
        let headers = new Headers();
        let options = new RequestOptions({ headers: headers });
        this.http
            .post('//' + $(location).attr('hostname') + ':4000/mock/login',
            body, options)
            .map(response => response.json())
            .subscribe(
            data => this.checkLogin(data),
            this.logError,
            () => console.log('Authentication Complete')
            );
    }

    checkLogin(result) {
        console.log("result --" + result);
        this.response = result;
        
        this.router.navigate(['CreatorHome', 'CreatorMyPrograms']);
        /*if (result.status == 'Ok' && result.switchWelcomeWizard=='false') {
            this.router.navigate(['CreatorHome', 'CreatorMyPrograms']);
        }else if (result.status == 'Ok' && result.switchWelcomeWizard=='true') {
            this.router.navigate(['ResetPassword']);
        }*/
    }

    loadForgotPassword() {
        this.router.navigate(['ForgotPassword']);
    }
    
    loadWelcomeWizard(){
         //http://localhost/cyrano_ui_v01/cyrano-ui
        //POST Examplehttp://staging.cyranosystems.com:4000/mock/login
        let body = JSON.stringify({ name });
        let headers = new Headers();
        let options = new RequestOptions({ headers: headers });
        this.http
            .post('//' + $(location).attr('hostname') + ':4000/mock/welcomeWizard',
            body, options)
            .map(response => response.json())
            .subscribe(
            data => this.checkWelcomeWizard(data),
            this.logError,
            () => console.log('Authentication Complete')
            );
    }
    
    checkWelcomeWizard(result) {
        console.log("result --" + result);
        this.response = result;
        if (result.status == 'Ok' && result.switchWelcomeWizard=='false') {
            this.router.navigate(['CreatorHome', 'CreatorMyPrograms']);
        }else if (result.status == 'Ok' && result.switchWelcomeWizard=='true') {
            this.router.navigate(['ResetPassword']);
        }
    }
    
    //guest play program
    playProgram(){
        this.router.navigate(['GuestPlayProgram']);
    }
}
