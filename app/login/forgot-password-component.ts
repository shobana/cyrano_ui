import { Component, View, Input, Output, EventEmitter } from 'angular2/core';
import { Router } from 'angular2/router';
import {HTTP_PROVIDERS}    from 'angular2/http';
import {Http, Response} from 'angular2/http';
import {Headers, RequestOptions} from 'angular2/http';
import {HTTP_BINDINGS} from 'angular2/http';
import 'rxjs/add/operator/map';
import {Injectable} from 'angular2/core';

@Component({
    selector: 'login-selector',    
    templateUrl: 'views/common/forgotPassword.html',
    providers: [
    HTTP_PROVIDERS,HTTP_BINDINGS]
})

export class ForgotPasswordComponent {

    username = "testcreator@cyranosystems.com";
    password = "test1234";  

    constructor (private router:Router,private http:Http) {
        this.router=router;
        this.http=http;        
    }
    
    checkLogin(result) {
        console.log("result --"+result);
        this.response=result;
         if(result=='Ok'){
             this.router.navigate(['CreatorHome','CreatorMyPrograms']);
         }
     }
    
    onForgotPwdSubmit() {  
        console.log("1");      
        let body = JSON.stringify({ name });
        let headers = new Headers();
        let options = new RequestOptions({ headers: headers });
        this.http
            .post('//' + $(location).attr('hostname') + ':4000/mock/login',
            body, options)
            .map(response => response.json())
            .subscribe(
            data => this.checkLogin(data),
            this.logError,
            () => console.log('Authentication Complete')
            );
     }
    
    checkLogin(result) {
        console.log("result --" + result);
        this.response = result;
        if (result.status == 'Ok' && result.switchWelcomeWizard=='false') {
            this.router.navigate(['CreatorHome', 'CreatorMyPrograms']);
        }else if (result.status == 'Ok' && result.switchWelcomeWizard=='true') {
            this.router.navigate(['ResetPassword']);
        }
    }
    
    loadLoginPage(){
           this.router.navigate(['Login']);    
    }    
}
