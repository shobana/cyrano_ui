import { Component } from 'angular2/core';
import { Router,RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from 'angular2/router';
import {NG_TABLE_DIRECTIVES} from 'ng2-table/ng2-table';
import {Http, Response} from 'angular2/http';
//import {HTTP_PROVIDERS}    from 'angular2/http';
//import {Headers, RequestOptions} from 'angular2/http';
//import {HTTP_BINDINGS} from 'angular2/http';

@Component({
    templateUrl: 'views/common/creatorlandingpage.html',
    directives: [ROUTER_DIRECTIVES,NG_TABLE_DIRECTIVES]
})

export class HomeComponent implements OnInit {
    public TableData='';
    public rows:Array<any> = [];
    public columns:Array<any> = [
    {title: 'Name', name: 'programName'},
    {title: 'Description', name: 'description', sort: false},
    {title: 'Published On', name: 'publishedOn', sort: 'asc'},
    {title: 'Expired On', name: 'expiredOn', sort: 'desc'}
  ];
  public page:number = 1;
  public itemsPerPage:number = 10;
  public maxSize:number = 5;
  public numPages:number = 1;
  public length:number = 0;

  public config:any = {
    paging: true,
    sorting: {columns: []}
    //filtering: {filterString: '', columnName: 'position'}
   };

    checkResponse(result) {
        this.TableData=JSON.stringify(result);
        console.log("TableData--"+this.TableData);
        this.length = this.TableData.length;
    }
    
    constructor (private router:Router,private http:Http) {
        this.router=router;
        this.http=http;
        
        this.http
            .get('/app/manageProgramsList.json')
            .map(response => response.json())
            .subscribe(
             data => this.checkResponse(data.programsList),
             this.logError,
        );
    }

   console.log("Before assigning val to data variable--"+this.TableData);
   private data:Array<any> = this.TableData;

    ngOnInit() {
      console.log("ngOnInit");
      this.onChangeTable(this.config, null);
    }

 /* changePage(page:any, data:Array<any> = this.data):Array<any> {
    console.log(page);
    let start = (page.page - 1) * page.itemsPerPage;
    let end = page.itemsPerPage > -1 ? (start + page.itemsPerPage) : data.length;
    return data.slice(start, end);
  }*/

  /*changeSort(data:any, config:any) {
    if (!config.sorting) {
      return data;
    }

    // simple sorting
    return data.sort((previous:any, current:any) => {
      let columns = this.config.sorting.columns || [];
      for (let i = 0; i < columns.length; i++) {
        let columnName = columns[i].name;

        if (previous[columnName] > current[columnName]) {
          return columns[i].sort === 'desc' ? -1 : 1;
        }
        if (previous[columnName] < current[columnName]) {
          return columns[i].sort === 'asc' ? -1 : 1;
        }
      }
      return 0;
    });
  }
*/
  changeFilter(data:any, config:any):any {
    if (!config.filtering) {
      return data;
    }

    let filteredData:Array<any> = data.filter((item:any) =>
      item[config.filtering.columnName].match(this.config.filtering.filterString));

    return filteredData;
  }

  onChangeTable(config:any, page:any = config.paging) {
    console.log("test-2");
    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
    }
    if (config.sorting) {
      Object.assign(this.config.sorting, config.sorting);
    }

      console.log("this.data--"+this.data);
    let filteredData = this.changeFilter(this.data, this.config);
    //let sortedData = this.changeSort(filteredData, this.config);
    //this.rows = page && config.paging ? this.changePage(page, sortedData) : sortedData;
    this.rows = page && config.paging ? this.changePage(page, filteredData) : filteredData;
    this.length = filteredData.length;
    console.log("Length"+this.length);
  }
}
