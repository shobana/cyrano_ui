import { Component,View, Input, Output, EventEmitter} from 'angular2/core';

import { Router } from 'angular2/router';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS, RouteParams  } from 'angular2/router';

import {HTTP_PROVIDERS}    from 'angular2/http';
import {Http, Response} from 'angular2/http';
import {Headers, RequestOptions} from 'angular2/http';
import {HTTP_BINDINGS} from 'angular2/http';
import 'rxjs/add/operator/map';

import { MODAL_DIRECTIVES } from 'ng2-bs3-modal/ng2-bs3-modal';

@Component({ 
  templateUrl: 'views/common/mergeAccountHome.html',
    directives: [ROUTER_DIRECTIVES, MODAL_DIRECTIVES],
    providers: [HTTP_PROVIDERS,HTTP_BINDINGS]
})
    
export class MergeAccountComponent {
        
    constructor (private router:Router,private http:Http, private params: RouteParams) {
        this.router=router;
        this.http=http;
        this.currentFunctionality = params.get('function');
    }
    
    save(){
        if(this.newPassword!=this.confirmPassword){
            console.log("Password Mismatch");
        }else{
            console.log("Password Changed");
        }
    }
    
    cancel(){
       this.router.navigate(['/CreatorHome','CreatorMyPrograms']);    
    }
    
    redirectToHome(){
       $('#acknowledgeMergeModal').modal('hide');
       $('.modal-backdrop').remove();
       //this.router.navigate(['CreatorMyPrograms']);
       this.router.navigateByUrl('/login');    
    }
    
    switchToOtherUserLogin(){
       this.router.navigateByUrl('/login'); 
    }
    
}
