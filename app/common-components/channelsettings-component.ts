import { Component,View, Input, Output, EventEmitter, OnInit, AfterViewInit} from 'angular2/core';

import { Router, RouteParams } from 'angular2/router';

import {HTTP_PROVIDERS}    from 'angular2/http';
import {Http, Response} from 'angular2/http';
import {Headers, RequestOptions} from 'angular2/http';
import {HTTP_BINDINGS} from 'angular2/http';
import 'rxjs/add/operator/map';
import {CORE_DIRECTIVES, FORM_DIRECTIVES, NgClass, NgIf, NgFor} from 'angular2/common';

import {TranslateService, TranslatePipe} from 'ng2-translate/ng2-translate';

@Component({ 
  templateUrl: 'views/common/channelsettings.html',
    directives: [FORM_DIRECTIVES, CORE_DIRECTIVES, NgFor],
    providers: [HTTP_PROVIDERS,HTTP_BINDINGS],
    pipes: [TranslatePipe]
})
    
export class ChannelSettingsComponent implements OnInit, AfterViewInit{
        
    constructor (private router:Router,private http:Http, private params: RouteParams) {
        this.router=router;
        this.http=http;
        this.from = params.get('from');
    }   
    
    
    ngOnInit() {
         $('#inner-content-div-group').slimScroll({
           position: 'right',
           height: '190px',
           railVisible: true,
           alwaysVisible: true
       });
        
        $('select[name="additionalGroups[]"]').bootstrapDualListbox(
            {nonSelectedListLabel: 'Available Channels',
             selectedListLabel: 'Selected Channels'} 
        );
        
        this.groupName="Viewer";
         this.email="eva@gmail.com";
        
        this.contentPriority=50;
        
        this.groupsList=[{"name":"Strategy & Operations","icon":"dist/img/channels_thumb/Change.png"},{"name":"Engagement","icon":"dist/img/channels_thumb/Engagement.png"},{"name":"Culture","icon":"dist/img/channels_thumb/Culture.png"},{"name":"Training & Development","icon":"dist/img/channels_thumb/Training.png"},{"name":"Wellness","icon":"dist/img/channels_thumb/Wellness.png"},{"name":"Performance Management","icon":"dist/img/channels_thumb/Performance.png"},{"name":"Recruiting & onboarding","icon":"dist/img/channels_thumb/Recruting.png"},{"name":"Change","icon":"dist/img/channels_thumb/Change.png"}];
        
    }
    
     ngAfterViewInit() {
        if (!ChannelSettingsComponent.chosenInitialized) {
            jQuery('#grouplist_dual').bootstrapDualListbox('refresh', true);
            ChannelSettingsComponent.chosenInitialized = true;
        }
         
        // this.getProfessionalProfileDetails();
    }
    
    next(){
          this.router.navigate(['/CreatorHome','CreatorMyPrograms']);
    }
    
    back(){
        this.router.navigate(['/CreatorHome','ProfileManagement','BioSettings']);
    }
    
    nextFromWW(){
            this.router.navigate(['/CreatorHome','CreatorMyPrograms']);
           //this.router.navigate(['Login']);            
          //this.router.navigate(['/CreatorHome','CreatorMyPrograms'}]);
    }
    
    backFromWW(){
        this.router.navigate(['BioSettings', {"from":"welcomeWizard"}]);
    }
}
