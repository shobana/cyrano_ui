import { Component,View, Input, Output, EventEmitter} from 'angular2/core';

import { Router } from 'angular2/router';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from 'angular2/router';

import {HTTP_PROVIDERS}    from 'angular2/http';
import {Http, Response} from 'angular2/http';
import {Headers, RequestOptions} from 'angular2/http';
import {HTTP_BINDINGS} from 'angular2/http';
import 'rxjs/add/operator/map';

@Component({ 
    templateUrl: 'views/common/permissionMatrix.html',
    directives: [ROUTER_DIRECTIVES],
    providers: [HTTP_PROVIDERS,HTTP_BINDINGS]
})
    
export class RolePermissionsMatrixComponent {
        
    constructor (private router:Router,private http:Http) {
        this.router=router;
        this.http=http;
        
        this.getRolesList();
        this.getPermissionsList();
    }
    
    //Get all roles
    getRolesList(){
        this.rolesList=[{"roleName":"SUPERADMIN"},{"roleName":"CUSTOMERADMIN"},{"roleName":"CONTENTCREATOR"},{"roleName":"CONTENTMANAGER",{"roleName":"VIEWER"}];
    }
    
    //Get all Permissions
    getPermissionsList(){
        let body = JSON.stringify({ name });
        let headers = new Headers();
        let options = new RequestOptions({ headers: headers });
        this.http
            .post('//' + $(location).attr('hostname') + ':4000/mock/permissionsList',
            body, options)
            .map(response => response.json())
            .subscribe(
            data => this.checkResponse(data),
            this.logError,
            () => console.log('Authentication Complete')
            );
    }

    checkResponse(result) {
       this.permissionsList=result.permissionsList;
    }

    save(){
          this.router.navigate(['/CreatorHome','SuperAdminDashboardCharts']);
    }
    
    cancel(){
          this.router.navigate(['/CreatorHome','SuperAdminDashboardCharts']);
    }
    
}
