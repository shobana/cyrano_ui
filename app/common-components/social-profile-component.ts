import { Component,View, Input, Output, EventEmitter} from 'angular2/core';

import { Router, RouteParams } from 'angular2/router';

import {HTTP_PROVIDERS}    from 'angular2/http';
import {Http, Response} from 'angular2/http';
import {Headers, RequestOptions} from 'angular2/http';
import {HTTP_BINDINGS} from 'angular2/http';
import 'rxjs/add/operator/map';

@Component({ 
  templateUrl: 'views/common/socialProfile.html',
    providers: [HTTP_PROVIDERS,HTTP_BINDINGS]
})
    
export class SocialProfileComponent {
        
    constructor (private router:Router,private http:Http, private params: RouteParams) {
        this.router=router;
        this.http=http;
        this.from = params.get('from');
    }
    
    save(){
        this.router.navigate(['/CreatorHome','ProfileManagement','BioSettings']);
    }
    
    back(){
        this.router.navigate(['/CreatorHome','ProfileManagement','ProfessionalProfile']);
    }
        
    saveFromWW(){
        this.router.navigate(['BioSettings', {"from":"welcomeWizard"}]);
    }
    
    backFromWW(){
        this.router.navigate(['ProfessionalProfile', {"from":"welcomeWizard"}]);
    }
}
