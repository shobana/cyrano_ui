import { Component,View, Input, Output, EventEmitter} from 'angular2/core';

import { Router } from 'angular2/router';

import {HTTP_PROVIDERS}    from 'angular2/http';
import {Http, Response} from 'angular2/http';
import {Headers, RequestOptions} from 'angular2/http';
import {HTTP_BINDINGS} from 'angular2/http';
import 'rxjs/add/operator/map';

@Component({ 
  templateUrl: 'views/common/changePassword.html',
    providers: [HTTP_PROVIDERS,HTTP_BINDINGS]
})
    
export class ChangePasswordComponent {
        
    constructor (private router:Router,private http:Http) {
        this.router=router;
        this.http=http;
    }
    
    save(){
        if(this.newPassword!=this.confirmPassword){
            console.log("Password Mismatch");
        }else{
            console.log("Password Changed");
        }
    }
    
    cancel(){
       this.router.navigate(['/CreatorHome','CreatorMyPrograms']);    
    }
}
