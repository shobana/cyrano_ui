import { Component } from 'angular2/core';
import { CORE_DIRECTIVES } from 'angular2/common';

import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS, RouteParams } from 'angular2/router';
import {Location,Router} from "angular2/router";

@Component({ 
  templateUrl: 'views/common/guestPlayProgram.html',
  directives: [ROUTER_DIRECTIVES]
})

export class GuestPlayProgramComponent {
    
    constructor(private location:Location,private router:Router, private params: RouteParams) {
        this.location=location; 
        this.router=router; 
        
        this. url = "http://techslides.com/demos/sample-videos/small.mp4";
    }
    
    close(){
        this.router.navigateByUrl('/login');
    }
}