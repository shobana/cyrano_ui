import { Component, View, Input, Output, EventEmitter,provide } from 'angular2/core';

import { Router } from 'angular2/router';

import {HTTP_PROVIDERS}    from 'angular2/http';
import {Http, Response} from 'angular2/http';
import {Headers, RequestOptions} from 'angular2/http';
import {HTTP_BINDINGS} from 'angular2/http';
import 'rxjs/add/operator/map';
import {Injectable} from 'angular2/core';

@Component({
    templateUrl: 'views/common/passwordProtectedPage.html',
    providers: [HTTP_PROVIDERS, HTTP_BINDINGS]
})

export class PasswordProtectedComponent {

    constructor(private router: Router, private http: Http) {
        this.router = router;
        this.http = http;
        
        this.showErrorDiv=false;
    }

    onSubmit() {
        if(this.tempPassword=='test1234'){
           this.showErrorDiv=false;
           this.router.navigate(['Login']);
        }else{
            this.showErrorDiv=true;
        }
    }
}

