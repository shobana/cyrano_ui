import { Component } from 'angular2/core';
import { CORE_DIRECTIVES } from 'angular2/common';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS, RouteParams } from 'angular2/router';

import {PersonalProfileComponent } from './personal-profile-component';
import { ProfessionalProfileComponent } from './professional-profile-component';
import { SocialProfileComponent } from './social-profile-component';
import { BioSettingsComponent } from './biosettings-component';
import { ChannelSettingsComponent } from './channelsettings-component';

import {Location,Router} from "angular2/router";

@Component({ 
  templateUrl: 'views/common/profileManagement.html',
  directives: [ROUTER_DIRECTIVES]
})

@RouteConfig([
  { path: '/personalProfile', name: 'PersonalProfile', component: PersonalProfileComponent},
  { path: '/professionalProfile', name: 'ProfessionalProfile', component: ProfessionalProfileComponent},
  { path: '/socialProfile', name: 'SocialProfile', component: SocialProfileComponent},
  { path: '/bioSettings', name: 'BioSettings', component: BioSettingsComponent}
  { path: '/channelSettings', name: 'ChannelSettings', component: ChannelSettingsComponent}
])
    
export class ProfileManagementComponent {
    
    constructor(private location:Location,private router:Router, private params: RouteParams) {
        this.location=location; 
        this.router=router;
        this.showSubMenusPublish=true;   
        this.from = params.get('from');
    }
    
    getLinkStyle(path) {
        return this.location.path().indexOf(path) > -1;
    }
      
    close(){
        this.router.navigate(['CreatorMyPrograms']); 
    }
}
