import { Component,View, Input, Output, EventEmitter,ViewChild,OnInit, AfterViewInit} from 'angular2/core';

import { Router } from 'angular2/router';

import {HTTP_PROVIDERS}    from 'angular2/http';
import {Http, Response} from 'angular2/http';
import {Headers, RequestOptions} from 'angular2/http';
import {HTTP_BINDINGS} from 'angular2/http';
import 'rxjs/add/operator/map';

import { MODAL_DIRECTIVES } from 'ng2-bs3-modal/ng2-bs3-modal';

@Component({ 
  templateUrl: 'views/welcomeWizard/biosettings.html',
    directives: [ MODAL_DIRECTIVES],
    providers: [HTTP_PROVIDERS,HTTP_BINDINGS]
})
    
export class WelcomeBioSettingsComponent implements OnInit, AfterViewInit {
    
    @ViewChild('recordContentModal')
    modal: ModalComponent;
    
    @ViewChild('playContentModal')
    playContentModal: ModalComponent;
    
    @ViewChild('showExternalLinkModal')
    showExternalLinkModal: ModalComponent;
    
    constructor (private router:Router,private http:Http) {
        this.router=router;
        this.http=http;
        
        this.showAudioElements = true;
        this.showAudio = true;
        this.showVideo = true;
        this.showVideoElements = false;
        this.showUrl = false;
        this.showBrowse = false;
        this.showRecord = false;
        
        this.ckeditorContent = " Happy Work Anniversary Message Sherry, this is Jason Johnson, CEO of ABC Company.  I want to congratulate you on 10 years of service with us at ABC Company.  While we have not had the opportunity to work directly together,  I want you to know that your dedication and commitment to upholding our company values is why we are a leader in the world.  Thank you for all that you do to make the SE field sales team the #1 team in the company.  Congratulations again on a great career so far with us and I look forward to meeting you face to face one day!";

        this.showRecordDetails = false;
        
        this.showSubMenusPublish=true;
        
        this.showAudioRecordingBtn=true;
    }
    
    ngOnInit() {
        CKEDITOR.replace('editor1');
        $(".textarea").wysihtml5();
    }

    
    showAudioRecordOption() {
        this.showAudioRecordingBtn=true;
        this.showVideoRecordingBtn=false;
        this.showExternalLinkBtn=false;
        this.showBrowseBtn=false;
        
         this.showRecordDetails = false;
    }
    
    showVideoRecordOption() {
        this.showAudioRecordingBtn=false;
        this.showVideoRecordingBtn=true;
        this.showExternalLinkBtn=false;
        this.showBrowseBtn=false;
        
         this.showRecordDetails = false;
    }

    //External Lnk Functionalities-Start
    showExternalLinkOption() {
       this.showAudioRecordingBtn=false;
       this.showVideoRecordingBtn=false;
       this.showExternalLinkBtn=true;
       this.showBrowseBtn=false;
        
         this.showRecordDetails = false;
    }
    
    showExternalLnikModal(){
         this.showExternalLinkModal.open();
    }
    
    addExternalLink(){
        this.showExternalLinkModal.dismiss();
        this.showRecordDetails = true; 
    }

    //Browse Option Functionalities-Start
    showBrowseOption() {
       this.showAudioRecordingBtn=false;
       this.showVideoRecordingBtn=false;
       this.showExternalLinkBtn=false;
       this.showBrowseBtn=true;
        
       this.showRecordDetails = false;
    }
    
    readUrl($event): void {
        this.postFile($event.target);
    }

    postFile(inputValue: any): void {

        var formData = new FormData();
        formData.append("name", "Name");
        formData.append("file", inputValue.files[0]);
        console.log(inputValue.files[0].name);
        this.filename = inputValue.files[0].name;
        this.showRecordDetails = true;
        if (inputValue.files && inputValue.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#blah')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(200);
            };

            reader.readAsDataURL(inputValue.files[0]);
        }

    }

    saveRecord() {
        //CKEDITOR.instances.editor2.destroy();
        
        $('#recordContentModal').modal('hide');
        $('.modal-backdrop').remove();

        this.showRecordDetails = true;

        this.showUrl = false;
        this.showBrowse = false;
        this.showRecord = false;
    }
    
     readUrl($event): void {
        this.postFile($event.target);
    }
    
     //Recording Dialog
     open() {
         this.modal.open('lg');
       
         this.recordingUrl="https://rtc.ibotsystems.com/stream-recorder/";
     }
    
    openplayContentModal(){
        this.playContentModal.open();
    }
    
    next(){
       this.router.navigate(['/CreatorHome', 'CreatorMyPrograms']);
    }
    
    close(){
        this.router.navigate(['CreatorHome', 'CreatorMyPrograms']);
    }
    
    back(){
        this.router.navigate(['SocialProfileDetails']);
    }
    
}
