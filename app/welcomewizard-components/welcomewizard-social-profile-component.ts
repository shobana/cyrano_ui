import { Component,View, Input, Output, EventEmitter} from 'angular2/core';

import { Router } from 'angular2/router';

import {HTTP_PROVIDERS}    from 'angular2/http';
import {Http, Response} from 'angular2/http';
import {Headers, RequestOptions} from 'angular2/http';
import {HTTP_BINDINGS} from 'angular2/http';
import 'rxjs/add/operator/map';

@Component({ 
  templateUrl: 'views/welcomeWizard/socialProfile.html',
    providers: [HTTP_PROVIDERS,HTTP_BINDINGS]
})
    
export class WelcomeSocialProfileComponent {
        
    constructor (private router:Router,private http:Http) {
        this.router=router;
        this.http=http;
        
        this.showSubMenusPublish=true;
    }
    
    next(){
          this.router.navigate(['BioSettingsDetails']);
    }
    
    close(){
        this.router.navigate(['CreatorHome', 'CreatorMyPrograms']);
    }
    
    back(){
        this.router.navigate(['ProfessionalProfileDetails']);
    }
}
