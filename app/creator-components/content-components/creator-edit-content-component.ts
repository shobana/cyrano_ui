import {Component, EventEmitter, OnInit,ViewChild} from 'angular2/core';

import {HTTP_PROVIDERS}    from 'angular2/http';
import {Http, Response} from 'angular2/http';
import {Headers, RequestOptions} from 'angular2/http';
import {HTTP_BINDINGS} from 'angular2/http';
import 'rxjs/add/operator/map';

import { Router } from 'angular2/router';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from 'angular2/router';

import { MODAL_DIRECTIVES } from 'ng2-bs3-modal/ng2-bs3-modal';

@Component({
    templateUrl: '/views/content-creator/contents/editContent.html',
    directives: [ROUTER_DIRECTIVES,MODAL_DIRECTIVES],
    providers: [HTTP_PROVIDERS, HTTP_BINDINGS]
})

export class CreatorEditContentComponent implements OnInit,,AfterViewInit {

    @ViewChild('recordContentModal')
    recordContentModal: ModalComponent;
    
    @ViewChild('playContentModal')
    playContentModal: ModalComponent;
    
    @ViewChild('showExternalLinkModal')
    showExternalLinkModal: ModalComponent;
    
    constructor(private http: Http, private router: Router) {
        this.http = http;
        this.router = router;

        this.ckeditorContent="Happy Work Anniversary Message Sherry, this is Jason Johnson, CEO of ABC Company.  I want to congratulate you on 10 years of service with us at ABC Company.  While we have not had the opportunity to work directly together,  I want you to know that your dedication and commitment to upholding our company values is why we are a leader in the world.  Thank you for all that you do to make the SE field sales team the #1 team in the company.  Congratulations again on a great career so far with us and I look forward to meeting you face to face one day!";
        
        this.getContentDetails();
        
        this.showAudioRecordingBtn=true;
        
        this.showRecordDetails = true;
        
        this.sharableReadFalse=true;
        this.sharableReadTrue=false;
    }

    getContentDetails(){
         this.http
            .get('app/mock/getContentDetails.json')
            .map(response => response.json())
            .subscribe(
             data => this.checkResponse(data),
             this.logError,
            () => console.log('Success')
         );
    }

    checkResponse(result) {
        this.contentVO=result.contentVO;
        this.contentUUID=this.contentVO.contentUUID;
        this.contentName=this.contentVO.name;
        this.description=this.contentVO.description;
     }
    
    showAudioRecordOption() {
        this.showAudioRecordingBtn=true;
        this.showVideoRecordingBtn=false;
        this.showExternalLinkBtn=false;
        this.showBrowseBtn=false;
        
         this.showRecordDetails = false;
    }
    
    showVideoRecordOption() {
        this.showAudioRecordingBtn=false;
        this.showVideoRecordingBtn=true;
        this.showExternalLinkBtn=false;
        this.showBrowseBtn=false;
        
         this.showRecordDetails = false;
    }

    showExternalLinkOption() {
       this.showAudioRecordingBtn=false;
       this.showVideoRecordingBtn=false;
       this.showExternalLinkBtn=true;
       this.showBrowseBtn=false;
        
         this.showRecordDetails = false;
    }
    
    showExternalLnikModal(){
         this.showExternalLinkModal.open();
    }
    
    addExternalLink(){
        this.showExternalLinkModal.dismiss();
        this.showRecordDetails = true; 
    }
    
    showBrowseOption() {
       this.showAudioRecordingBtn=false;
       this.showVideoRecordingBtn=false;
       this.showExternalLinkBtn=false;
       this.showBrowseBtn=true;
        
       this.showRecordDetails = false;
    }
    
    showRecordOption() {
        this.showUrl = false;
        this.showBrowse = false;
        this.showRecord=true;
        
        this.showRecordDetails = false;
    }
    
    cancel(){
        this.router.navigate(['CreatorMyContents']);
    }
    
    ngOnInit() {
        CKEDITOR.replace('editor1');            
        $(".textarea").wysihtml5();
    }
    
    saveRecord() {
        CKEDITOR.instances.editor2.destroy();
        
        $('#recordContentModal').modal('hide');
        $('.modal-backdrop').remove();

        this.showRecordDetails = true;

        this.showUrl = false;
        this.showBrowse = false;
        this.showRecord = false;
    }
    
    //Recording Dialog
    open() {
        this.recordContentModal.open('lg');

        this.recordingUrl = "https://rtc.ibotsystems.com/stream-recorder/";

        CKEDITOR.replace('editor2');
        $(".textarea").wysihtml5();

    }

    openplayContentModal() {
        this.playContentModal.open();
    }
    
    isPrivate(){
        if(document.getElementById("isPrivate").checked){
            this.sharableReadFalse=false;
            this.sharableReadTrue=true;
        }else{
            this.sharableReadTrue=false;
            this.sharableReadFalse=true;
        }
    }
    
    createProgram(currentContentUUID){
        this.router.navigate(['AddNewProgram',{ contentUUID: currentContentUUID},'PublisherIntro',{ contentUUID: currentContentUUID}]);
    }
}