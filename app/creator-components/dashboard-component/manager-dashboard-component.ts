import {Component, EventEmitter, OnInit} from 'angular2/core';
import {CORE_DIRECTIVES, FORM_DIRECTIVES, NgClass, NgIf} from 'angular2/common';

import {HTTP_PROVIDERS}    from 'angular2/http';
import {Http, Response} from 'angular2/http';
import {Headers, RequestOptions} from 'angular2/http';
import {HTTP_BINDINGS} from 'angular2/http';
import 'rxjs/add/operator/map';
import {Injectable} from 'angular2/core';

import { Router,Location} from 'angular2/router';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from 'angular2/router';

import {NG_TABLE_DIRECTIVES} from '../../../node_modules/ng2-table/ng2-table';
import {NgTableSorting} from '../../../node_modules/ng2-table/components/table/ng-table-sorting.directive';
import {PAGINATION_DIRECTIVES} from 'ng2-bootstrap/ng2-bootstrap';

import { MODAL_DIRECTIVES } from 'ng2-bs3-modal/ng2-bs3-modal';


@Component({
  templateUrl: '/views/content-manager/dashboard/manageDashboardCharts.html',  
  directives: [NgClass, NgIf, CORE_DIRECTIVES, FORM_DIRECTIVES,ROUTER_DIRECTIVES,NG_TABLE_DIRECTIVES,NgTableSorting,PAGINATION_DIRECTIVES,MODAL_DIRECTIVES],
  providers: [HTTP_PROVIDERS,HTTP_BINDINGS]
})
export class ManagerDashboardComponent implements OnInit {

  constructor(private http:Http,private router:Router,private location: Location) {
    this.http=http;
    this.router=router;
    this.location = location;      
   
    this.getProgramsByStateChart();
    this.getProgramsCreatedChart();
    this.getProgramViewsChart();
    this.getPublishedProgramsChart();
    //this.getNewPrograms(this.page);    
  }
    
    /* table section starts*/
    /*getNewPrograms(page) {
        let body = JSON.stringify({ name });
        let headers = new Headers();
        let options = new RequestOptions({ headers: headers });
        return this.http
            .post('//' + $(location).attr('hostname') + ':4000/mock/managerdashboardNewProgram/' + page,
            body, options)
            .map(response => response.json())
            .subscribe(
            data => this.checkResponse(data),
            this.logError,
            () => console.log('Data fetching complete')
            );
    }

    checkResponse(response) {        
        this.rows = response.newPrograms;
        this.length = response.newPrograms.length;
    }

    private columns: Array<any> = [
        { title: 'Name', name: 'name' },
        { title: 'Author', name: 'author'},
        { title: 'Publish Date', name: 'publishDate' },
        { title: 'Actions', name: 'programUUID' }
    ];

    public config: any = {
        paging: true,
        sorting: { columns: [] },
    };

    public tableChanged: EventEmitter<any> = new EventEmitter();

    onChangeTable(config: any, page: any = config.paging) {
        //console.log(page);
        this.getNewPrograms(page.page);
    }*/
    //table end
    
  getProgramsByStateChart(){        
         this.http
            .post('//'+$(location).attr('hostname')+':4000/mock/managerDashboardCharts')
            .map(response => response.json())
            .subscribe(
             data => this.checkProgramsByStateCharts(data.clientResponse),
             this.logError,
            () => console.log('next tab')
         );
    }
      
  checkProgramsByStateCharts(result) {        
        var activeCustomersLabel = [];
        var activeCustomersValue = [];        
        this.chartVal=JSON.stringify(result.programsByState);
        this.parsedCharVal = JSON.parse(this.chartVal);        
        $.each(this.parsedCharVal, function(key, value){
            activeCustomersLabel.push(key);
            activeCustomersValue.push(value)            
        });
        //console.log(activeCustomersLabel);
        //console.log(activeCustomersValue);        
                var areaChartData = {
                  labels: activeCustomersLabel,
                  datasets: [
                    {
                      label: "Electronics",
                      fillColor: "rgba(210, 214, 222, 1)",
                      strokeColor: "rgba(210, 214, 222, 1)",
                      pointColor: "rgba(210, 214, 222, 1)",
                      pointStrokeColor: "#c1c7d1",
                      pointHighlightFill: "#fff",
                      pointHighlightStroke: "rgba(220,220,220,1)",
                      data: activeCustomersValue
                    }
                  ]
                };
                
                var barChartCanvas = $("#barChart1").get(0).getContext("2d");
                var barChart = new Chart(barChartCanvas);
                var barChartData = areaChartData;
                barChartData.datasets[0].fillColor = "#00a65a";
                barChartData.datasets[0].strokeColor = "#00a65a";
                barChartData.datasets[0].pointColor = "#00a65a";
                var barChartOptions = {
                  //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
                  scaleBeginAtZero: true,
                  //Boolean - Whether grid lines are shown across the chart
                  scaleShowGridLines: true,
                  //String - Colour of the grid lines
                  scaleGridLineColor: "rgba(0,0,0,.05)",
                  //Number - Width of the grid lines
                  scaleGridLineWidth: 1,
                  //Boolean - Whether to show horizontal lines (except X axis)
                  scaleShowHorizontalLines: true,
                  //Boolean - Whether to show vertical lines (except Y axis)
                  scaleShowVerticalLines: true,
                  //Boolean - If there is a stroke on each bar
                  barShowStroke: true,
                  //Number - Pixel width of the bar stroke
                  barStrokeWidth: 2,
                  //Number - Spacing between each of the X value sets
                      barValueSpacing: 5,
                      //Number - Spacing between data sets within X values
                      barDatasetSpacing: 1,
                      //String - A legend template
                      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
                      //Boolean - whether to make the chart responsive
                      responsive: true,
                      maintainAspectRatio: true
                    };
            
                    barChartOptions.datasetFill = false;
                    barChart.Bar(barChartData, barChartOptions);
      
        }
        
   //getMyProgramsCreatedChart    
    getProgramsCreatedChart(){        
         this.http
            .post('//'+$(location).attr('hostname')+':4000/mock/managerDashboardCharts')
            .map(response => response.json())
            .subscribe(
             data => this.checkProgramsCreatedChart(data.clientResponse),
             this.logError,
            () => console.log('next tab')
         );
    }
      
     checkProgramsCreatedChart(result) {        
        var activeCustomersLabel = [];
        var activeCustomersValue = [];        
        this.chartVal=JSON.stringify(result.programsCreated);
        this.parsedCharVal = JSON.parse(this.chartVal);        
        $.each(this.parsedCharVal, function(key, value){
            activeCustomersLabel.push(key);
            activeCustomersValue.push(value)            
        });
        var areaChartData = {
          labels: activeCustomersLabel,
          datasets: [
            {
              label: "Electronics",
              fillColor: "rgba(210, 214, 222, 1)",
              strokeColor: "rgba(210, 214, 222, 1)",
              pointColor: "rgba(210, 214, 222, 1)",
              pointStrokeColor: "#c1c7d1",
              pointHighlightFill: "#fff",
              pointHighlightStroke: "rgba(220,220,220,1)",
              data: activeCustomersValue
            }
          ]
        };
        
        var barChartCanvas = $("#barChart2").get(0).getContext("2d");
        var barChart = new Chart(barChartCanvas);
        var barChartData = areaChartData;
        barChartData.datasets[0].fillColor = "#00a65a";
        barChartData.datasets[0].strokeColor = "#00a65a";
        barChartData.datasets[0].pointColor = "#00a65a";
        var barChartOptions = {          
          scaleBeginAtZero: true,          
          scaleShowGridLines: true,          
          scaleGridLineColor: "rgba(0,0,0,.05)",          
          scaleGridLineWidth: 1,          
          scaleShowHorizontalLines: true,          
          scaleShowVerticalLines: true,          
          barShowStroke: true,          
          barStrokeWidth: 2,          
              barValueSpacing: 5,          
              barDatasetSpacing: 1,          
              legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",          
              responsive: true,
              maintainAspectRatio: true
            };    
            barChartOptions.datasetFill = false;
            barChart.Bar(barChartData, barChartOptions);      
        }
    
        //getMyProgramViewsChart
        getProgramViewsChart(){        
         this.http
            .post('//'+$(location).attr('hostname')+':4000/mock/managerDashboardCharts')
            .map(response => response.json())
            .subscribe(
             data => this.checkProgramViewsChart(data.clientResponse),
             this.logError,
            () => console.log('next tab')
         );
        }
      
     checkProgramViewsChart(result) {        
        var activeCustomersLabel = [];
        var activeCustomersValue = [];        
        this.chartVal=JSON.stringify(result.programViews);
        this.parsedCharVal = JSON.parse(this.chartVal);        
        $.each(this.parsedCharVal, function(key, value){
            activeCustomersLabel.push(key);
            activeCustomersValue.push(value)            
        });
         
        var areaChartData = {
          labels: activeCustomersLabel,
          datasets: [
            {
              label: "Electronics",
              fillColor: "rgba(210, 214, 222, 1)",
              strokeColor: "rgba(210, 214, 222, 1)",
              pointColor: "rgba(210, 214, 222, 1)",
              pointStrokeColor: "#c1c7d1",
              pointHighlightFill: "#fff",
              pointHighlightStroke: "rgba(220,220,220,1)",
              data: activeCustomersValue
            }
          ]
        };
        
        var barChartCanvas = $("#barChart3").get(0).getContext("2d");
        var barChart = new Chart(barChartCanvas);
        var barChartData = areaChartData;
        barChartData.datasets[0].fillColor = "#00a65a";
        barChartData.datasets[0].strokeColor = "#00a65a";
        barChartData.datasets[0].pointColor = "#00a65a";
        var barChartOptions = {          
          scaleBeginAtZero: true,          
          scaleShowGridLines: true,          
          scaleGridLineColor: "rgba(0,0,0,.05)",          
          scaleGridLineWidth: 1,          
          scaleShowHorizontalLines: true,          
          scaleShowVerticalLines: true,          
          barShowStroke: true,          
          barStrokeWidth: 2,          
              barValueSpacing: 5,          
              barDatasetSpacing: 1,          
              legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",          
              responsive: true,
              maintainAspectRatio: true
            };    
            barChartOptions.datasetFill = false;
            barChart.Bar(barChartData, barChartOptions);      
        }
        
        //programViews start
    
        getPublishedProgramsChart(){        
         this.http
            .post('//'+$(location).attr('hostname')+':4000/mock/managerDashboardCharts')
            .map(response => response.json())
            .subscribe(
             data => this.checkPublishedProgramsChart(data.clientResponse),
             this.logError,
            () => console.log('next tab')
         );
        }
      
     checkPublishedProgramsChart(result) {        
        var activeCustomersLabel = [];
        var activeCustomersValue = [];        
        this.chartVal=JSON.stringify(result.publishedPrograms);
        this.parsedCharVal = JSON.parse(this.chartVal);        
        $.each(this.parsedCharVal, function(key, value){
            activeCustomersLabel.push(key);
            activeCustomersValue.push(value)            
        });
         
        var areaChartData = {
          labels: activeCustomersLabel,
          datasets: [
            {
              label: "Electronics",
              fillColor: "rgba(210, 214, 222, 1)",
              strokeColor: "rgba(210, 214, 222, 1)",
              pointColor: "rgba(210, 214, 222, 1)",
              pointStrokeColor: "#c1c7d1",
              pointHighlightFill: "#fff",
              pointHighlightStroke: "rgba(220,220,220,1)",
              data: activeCustomersValue
            }
          ]
        };
        
        var barChartCanvas = $("#barChart4").get(0).getContext("2d");
        var barChart = new Chart(barChartCanvas);
        var barChartData = areaChartData;
        barChartData.datasets[0].fillColor = "#00a65a";
        barChartData.datasets[0].strokeColor = "#00a65a";
        barChartData.datasets[0].pointColor = "#00a65a";
        var barChartOptions = {          
          scaleBeginAtZero: true,          
          scaleShowGridLines: true,          
          scaleGridLineColor: "rgba(0,0,0,.05)",          
          scaleGridLineWidth: 1,          
          scaleShowHorizontalLines: true,          
          scaleShowVerticalLines: true,          
          barShowStroke: true,          
          barStrokeWidth: 2,          
              barValueSpacing: 5,          
              barDatasetSpacing: 1,          
              legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",          
              responsive: true,
              maintainAspectRatio: true
            };    
            barChartOptions.datasetFill = false;
            barChart.Bar(barChartData, barChartOptions);      
        }
        
    editProgram(){
        //console.log(this);
        this.router.navigate(['ManagerEditProgram','ManagerEditProgramIntro']);
   }
}