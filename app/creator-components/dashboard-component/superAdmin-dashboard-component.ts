import {Component, EventEmitter, OnInit} from 'angular2/core';
import {CORE_DIRECTIVES, FORM_DIRECTIVES, NgClass, NgIf} from 'angular2/common';

import {HTTP_PROVIDERS}    from 'angular2/http';
import {Http, Response} from 'angular2/http';
import {Headers, RequestOptions} from 'angular2/http';
import {HTTP_BINDINGS} from 'angular2/http';
import 'rxjs/add/operator/map';
import {Injectable} from 'angular2/core';

import { Router,Location} from 'angular2/router';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from 'angular2/router';


@Component({  
  templateUrl: '/views/superAdmin/dashboard/superAdminDashboardCharts.html',  
  directives: [NgClass, NgIf, CORE_DIRECTIVES, FORM_DIRECTIVES,ROUTER_DIRECTIVES],
  providers: [HTTP_PROVIDERS,HTTP_BINDINGS]
})
export class SuperAdminDashboardComponent implements OnInit {

  constructor(private http:Http,private router:Router,private location: Location) {
    this.http=http;
    this.router=router;
    this.location = location;      
   
    this.getActiveCustomersChart();
    this.getActiveUsersChart();
    this.getProgramsCreatedChart();
    this.getProgramViewsChart();
    this.getPublishedProgramsChart();    
  }
    
  getActiveCustomersChart(){        
         this.http
            .post('//'+$(location).attr('hostname')+':4000/mock/superAdminDashboardCharts')
            .map(response => response.json())
            .subscribe(
             data => this.checkActiveCustomersCharts(data.clientResponse),
             this.logError,
            () => console.log('next tab')
         );
    }
      
  checkActiveCustomersCharts(result) {        
        var activeCustomersLabel = [];
        var activeCustomersValue = [];        
        this.chartVal=JSON.stringify(result.activeCustomers);
        this.parsedCharVal = JSON.parse(this.chartVal);        
        $.each(this.parsedCharVal, function(key, value){
            activeCustomersLabel.push(key);
            activeCustomersValue.push(value)            
        });
        console.log(activeCustomersLabel);
        console.log(activeCustomersValue);        
                var areaChartData = {
                  labels: activeCustomersLabel,
                  datasets: [
                    {
                      label: "Electronics",
                      fillColor: "rgba(210, 214, 222, 1)",
                      strokeColor: "rgba(210, 214, 222, 1)",
                      pointColor: "rgba(210, 214, 222, 1)",
                      pointStrokeColor: "#c1c7d1",
                      pointHighlightFill: "#fff",
                      pointHighlightStroke: "rgba(220,220,220,1)",
                      data: activeCustomersValue
                    }
                  ]
                };
                
                var barChartCanvas = $("#barChart1").get(0).getContext("2d");
                var barChart = new Chart(barChartCanvas);
                var barChartData = areaChartData;
                barChartData.datasets[0].fillColor = "#00a65a";
                barChartData.datasets[0].strokeColor = "#00a65a";
                barChartData.datasets[0].pointColor = "#00a65a";
                var barChartOptions = {
                  //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
                  scaleBeginAtZero: true,
                  //Boolean - Whether grid lines are shown across the chart
                  scaleShowGridLines: true,
                  //String - Colour of the grid lines
                  scaleGridLineColor: "rgba(0,0,0,.05)",
                  //Number - Width of the grid lines
                  scaleGridLineWidth: 1,
                  //Boolean - Whether to show horizontal lines (except X axis)
                  scaleShowHorizontalLines: true,
                  //Boolean - Whether to show vertical lines (except Y axis)
                  scaleShowVerticalLines: true,
                  //Boolean - If there is a stroke on each bar
                  barShowStroke: true,
                  //Number - Pixel width of the bar stroke
                  barStrokeWidth: 2,
                  //Number - Spacing between each of the X value sets
                      barValueSpacing: 5,
                      //Number - Spacing between data sets within X values
                      barDatasetSpacing: 1,
                      //String - A legend template
                      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
                      //Boolean - whether to make the chart responsive
                      responsive: true,
                      maintainAspectRatio: true
                    };
            
                    barChartOptions.datasetFill = false;
                    barChart.Bar(barChartData, barChartOptions);
      
        }
        
    //getMyProgramsCreatedChart    
    getActiveUsersChart(){        
         this.http
            .post('//'+$(location).attr('hostname')+':4000/mock/superAdminDashboardCharts')
            .map(response => response.json())
            .subscribe(
             data => this.checkActiveUsersChart(data.clientResponse),
             this.logError,
            () => console.log('next tab')
         );
    }
      
     checkActiveUsersChart(result) {        
        var activeCustomersLabel = [];
        var activeCustomersValue = [];        
        this.chartVal=JSON.stringify(result.activeUsers);
        this.parsedCharVal = JSON.parse(this.chartVal);        
        $.each(this.parsedCharVal, function(key, value){
            activeCustomersLabel.push(key);
            activeCustomersValue.push(value)            
        });
        var areaChartData = {
          labels: activeCustomersLabel,
          datasets: [
            {
              label: "Electronics",
              fillColor: "rgba(210, 214, 222, 1)",
              strokeColor: "rgba(210, 214, 222, 1)",
              pointColor: "rgba(210, 214, 222, 1)",
              pointStrokeColor: "#c1c7d1",
              pointHighlightFill: "#fff",
              pointHighlightStroke: "rgba(220,220,220,1)",
              data: activeCustomersValue
            }
          ]
        };
        
        var barChartCanvas = $("#barChart2").get(0).getContext("2d");
        var barChart = new Chart(barChartCanvas);
        var barChartData = areaChartData;
        barChartData.datasets[0].fillColor = "#00a65a";
        barChartData.datasets[0].strokeColor = "#00a65a";
        barChartData.datasets[0].pointColor = "#00a65a";
        var barChartOptions = {          
          scaleBeginAtZero: true,          
          scaleShowGridLines: true,          
          scaleGridLineColor: "rgba(0,0,0,.05)",          
          scaleGridLineWidth: 1,          
          scaleShowHorizontalLines: true,          
          scaleShowVerticalLines: true,          
          barShowStroke: true,          
          barStrokeWidth: 2,          
              barValueSpacing: 5,          
              barDatasetSpacing: 1,          
              legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",          
              responsive: true,
              maintainAspectRatio: true
            };    
            barChartOptions.datasetFill = false;
            barChart.Bar(barChartData, barChartOptions);      
        }
    
        //getProgramsCreatedChart
        getProgramsCreatedChart(){        
         this.http
            .post('//'+$(location).attr('hostname')+':4000/mock/superAdminDashboardCharts')
            .map(response => response.json())
            .subscribe(
             data => this.checkProgramsCreatedChart(data.clientResponse),
             this.logError,
            () => console.log('next tab')
         );
        }
      
     checkProgramsCreatedChart(result) {        
        var activeCustomersLabel = [];
        var activeCustomersValue = [];        
        this.chartVal=JSON.stringify(result.programsCreated);
        this.parsedCharVal = JSON.parse(this.chartVal);        
        $.each(this.parsedCharVal, function(key, value){
            activeCustomersLabel.push(key);
            activeCustomersValue.push(value)            
        });
         
        var areaChartData = {
          labels: activeCustomersLabel,
          datasets: [
            {
              label: "Electronics",
              fillColor: "rgba(210, 214, 222, 1)",
              strokeColor: "rgba(210, 214, 222, 1)",
              pointColor: "rgba(210, 214, 222, 1)",
              pointStrokeColor: "#c1c7d1",
              pointHighlightFill: "#fff",
              pointHighlightStroke: "rgba(220,220,220,1)",
              data: activeCustomersValue
            }
          ]
        };
        
        var barChartCanvas = $("#barChart3").get(0).getContext("2d");
        var barChart = new Chart(barChartCanvas);
        var barChartData = areaChartData;
        barChartData.datasets[0].fillColor = "#00a65a";
        barChartData.datasets[0].strokeColor = "#00a65a";
        barChartData.datasets[0].pointColor = "#00a65a";
        var barChartOptions = {          
          scaleBeginAtZero: true,          
          scaleShowGridLines: true,          
          scaleGridLineColor: "rgba(0,0,0,.05)",          
          scaleGridLineWidth: 1,          
          scaleShowHorizontalLines: true,          
          scaleShowVerticalLines: true,          
          barShowStroke: true,          
          barStrokeWidth: 2,          
              barValueSpacing: 5,          
              barDatasetSpacing: 1,          
              legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",          
              responsive: true,
              maintainAspectRatio: true
            };    
            barChartOptions.datasetFill = false;
            barChart.Bar(barChartData, barChartOptions);      
        }
        
        //programViews start
    
        getProgramViewsChart(){        
         this.http
            .post('//'+$(location).attr('hostname')+':4000/mock/superAdminDashboardCharts')
            .map(response => response.json())
            .subscribe(
             data => this.checkProgramViewsChart(data.clientResponse),
             this.logError,
            () => console.log('next tab')
         );
        }
      
     checkProgramViewsChart(result) {        
        var activeCustomersLabel = [];
        var activeCustomersValue = [];        
        this.chartVal=JSON.stringify(result.programViews);
        this.parsedCharVal = JSON.parse(this.chartVal);        
        $.each(this.parsedCharVal, function(key, value){
            activeCustomersLabel.push(key);
            activeCustomersValue.push(value)            
        });
         
        var areaChartData = {
          labels: activeCustomersLabel,
          datasets: [
            {
              label: "Electronics",
              fillColor: "rgba(210, 214, 222, 1)",
              strokeColor: "rgba(210, 214, 222, 1)",
              pointColor: "rgba(210, 214, 222, 1)",
              pointStrokeColor: "#c1c7d1",
              pointHighlightFill: "#fff",
              pointHighlightStroke: "rgba(220,220,220,1)",
              data: activeCustomersValue
            }
          ]
        };
        
        var barChartCanvas = $("#barChart4").get(0).getContext("2d");
        var barChart = new Chart(barChartCanvas);
        var barChartData = areaChartData;
        barChartData.datasets[0].fillColor = "#00a65a";
        barChartData.datasets[0].strokeColor = "#00a65a";
        barChartData.datasets[0].pointColor = "#00a65a";
        var barChartOptions = {          
          scaleBeginAtZero: true,          
          scaleShowGridLines: true,          
          scaleGridLineColor: "rgba(0,0,0,.05)",          
          scaleGridLineWidth: 1,          
          scaleShowHorizontalLines: true,          
          scaleShowVerticalLines: true,          
          barShowStroke: true,          
          barStrokeWidth: 2,          
              barValueSpacing: 5,          
              barDatasetSpacing: 1,          
              legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",          
              responsive: true,
              maintainAspectRatio: true
            };    
            barChartOptions.datasetFill = false;
            barChart.Bar(barChartData, barChartOptions);      
        }
    
        //5th chart
    
    //getpublishedProgramsChart start
    
        getPublishedProgramsChart(){        
         this.http
            .post('//'+$(location).attr('hostname')+':4000/mock/superAdminDashboardCharts')
            .map(response => response.json())
            .subscribe(
             data => this.checkPublishedProgramsChart(data.clientResponse),
             this.logError,
            () => console.log('next tab')
         );
        }
      
     checkPublishedProgramsChart(result) {        
        var activeCustomersLabel = [];
        var activeCustomersValue = [];        
        this.chartVal=JSON.stringify(result.publishedPrograms);
        this.parsedCharVal = JSON.parse(this.chartVal);        
        $.each(this.parsedCharVal, function(key, value){
            activeCustomersLabel.push(key);
            activeCustomersValue.push(value)            
        });
         
        var areaChartData = {
          labels: activeCustomersLabel,
          datasets: [
            {
              label: "Electronics",
              fillColor: "rgba(210, 214, 222, 1)",
              strokeColor: "rgba(210, 214, 222, 1)",
              pointColor: "rgba(210, 214, 222, 1)",
              pointStrokeColor: "#c1c7d1",
              pointHighlightFill: "#fff",
              pointHighlightStroke: "rgba(220,220,220,1)",
              data: activeCustomersValue
            }
          ]
        };
        
        var barChartCanvas = $("#barChart5").get(0).getContext("2d");
        var barChart = new Chart(barChartCanvas);
        var barChartData = areaChartData;
        barChartData.datasets[0].fillColor = "#00a65a";
        barChartData.datasets[0].strokeColor = "#00a65a";
        barChartData.datasets[0].pointColor = "#00a65a";
        var barChartOptions = {          
          scaleBeginAtZero: true,          
          scaleShowGridLines: true,          
          scaleGridLineColor: "rgba(0,0,0,.05)",          
          scaleGridLineWidth: 1,          
          scaleShowHorizontalLines: true,          
          scaleShowVerticalLines: true,          
          barShowStroke: true,          
          barStrokeWidth: 2,          
              barValueSpacing: 5,          
              barDatasetSpacing: 1,          
              legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",          
              responsive: true,
              maintainAspectRatio: true
            };    
            barChartOptions.datasetFill = false;
            barChart.Bar(barChartData, barChartOptions);      
        }
   
}