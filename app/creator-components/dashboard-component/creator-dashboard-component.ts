import {Component, EventEmitter, OnInit} from 'angular2/core';
import {CORE_DIRECTIVES, FORM_DIRECTIVES, NgClass, NgIf} from 'angular2/common';

import {HTTP_PROVIDERS}    from 'angular2/http';
import {Http, Response} from 'angular2/http';
import {Headers, RequestOptions} from 'angular2/http';
import {HTTP_BINDINGS} from 'angular2/http';
import 'rxjs/add/operator/map';
import {Injectable} from 'angular2/core';

import { Router,Location} from 'angular2/router';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from 'angular2/router';

import {NG_TABLE_DIRECTIVES} from '../../../node_modules/ng2-table/ng2-table';
import {NgTableSorting} from '../../../node_modules/ng2-table/components/table/ng-table-sorting.directive';
import {PAGINATION_DIRECTIVES} from 'ng2-bootstrap/ng2-bootstrap';


@Component({
  templateUrl: '/views/content-creator/dashboard/dashboardCharts.html',  
  directives: [NgClass, NgIf, CORE_DIRECTIVES, FORM_DIRECTIVES,ROUTER_DIRECTIVES,NG_TABLE_DIRECTIVES,NgTableSorting,PAGINATION_DIRECTIVES],
  providers: [HTTP_PROVIDERS,HTTP_BINDINGS]
})
export class CreatorDashboardComponent implements OnInit {

  constructor(private http:Http,private router:Router,private location: Location) {
    this.http=http;
    this.router=router;
    this.location = location;      
   
    this.getMyProgramsByStateChart();
    this.getMyProgramsCreatedChart();
    this.getMyProgramViewsChart();
    this.getMyPublishedProgramsChart();
    //this.getMyScriptedPrograms(this.page);
     //this.getPublishedProgramsChart();
  }
    
 /* table section starts*/
    /*getMyScriptedPrograms(page) {
        let body = JSON.stringify({ name });
        let headers = new Headers();
        let options = new RequestOptions({ headers: headers });
        return this.http
            .post('//' + $(location).attr('hostname') + ':4000/mock/dashboardScriptedProgram/' + page,
            body, options)
            .map(response => response.json())
            .subscribe(
            data => this.checkResponse(data),
            this.logError,
            () => console.log('Data fetching complete')
            );
    }

    checkResponse(response) {        
        this.rows = response.myScriptedPrograms;
        this.length = response.myScriptedPrograms.length;
    }

    private columns: Array<any> = [
        { title: 'Program Name', name: 'programName' },
        { title: 'Assistant', name: 'assistant'},
        { title: 'Publish Date', name: 'publishDate' },
        { title: 'Actions', name: 'programUUID' }
    ];

    public config: any = {
        paging: true,
        sorting: { columns: [] },
    };

    public tableChanged: EventEmitter<any> = new EventEmitter();

    onChangeTable(config: any, page: any = config.paging) {
        console.log(page);
        this.getMyScriptedPrograms(page.page);
    }*/
    //table end
    
    
  getMyProgramsByStateChart(){        
         this.http
            .post('//'+$(location).attr('hostname')+':4000/mock/creatorDashboardCharts')
            .map(response => response.json())
            .subscribe(
             data => this.checkMyProgramsByStateCharts(data.clientResponse),
             this.logError,
            () => console.log('next tab')
         );
    }
      
  checkMyProgramsByStateCharts(result) {        
        var activeCustomersLabel = [];
        var activeCustomersValue = [];        
        this.chartVal=JSON.stringify(result.myProgramsByState);
        this.parsedCharVal = JSON.parse(this.chartVal);
        //var keys = Object.keys(this.parsedCharVal);
        $.each(this.parsedCharVal, function(key, value){
            activeCustomersLabel.push(key);
            activeCustomersValue.push(value)            
        });
        console.log(activeCustomersLabel);
        console.log(activeCustomersValue);
        //console.log(this.chartVal);
          /*var bar = new Morris.Bar({
          element: 'bar-chart',
          resize: true,          
          data:this.parsedCharVal,
          barColors: ['#00a65a', '#f56954'],
          xkey: 'y',
          ykeys: ['a'],
          labels: ['CPU', 'DISK'],
          hideHover: 'auto'
        }); */ 
                var areaChartData = {
                  labels: activeCustomersLabel,
                  datasets: [
                    {
                      label: "Electronics",
                      fillColor: "rgba(210, 214, 222, 1)",
                      strokeColor: "rgba(210, 214, 222, 1)",
                      pointColor: "rgba(210, 214, 222, 1)",
                      pointStrokeColor: "#c1c7d1",
                      pointHighlightFill: "#fff",
                      pointHighlightStroke: "rgba(220,220,220,1)",
                      data: activeCustomersValue
                    }
                  ]
                };
                
                var barChartCanvas = $("#barChart1").get(0).getContext("2d");
                var barChart = new Chart(barChartCanvas);
                var barChartData = areaChartData;
                barChartData.datasets[0].fillColor = "#00a65a";
                barChartData.datasets[0].strokeColor = "#00a65a";
                barChartData.datasets[0].pointColor = "#00a65a";
                var barChartOptions = {
                  //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
                  scaleBeginAtZero: true,
                  //Boolean - Whether grid lines are shown across the chart
                  scaleShowGridLines: true,
                  //String - Colour of the grid lines
                  scaleGridLineColor: "rgba(0,0,0,.05)",
                  //Number - Width of the grid lines
                  scaleGridLineWidth: 1,
                  //Boolean - Whether to show horizontal lines (except X axis)
                  scaleShowHorizontalLines: true,
                  //Boolean - Whether to show vertical lines (except Y axis)
                  scaleShowVerticalLines: true,
                  //Boolean - If there is a stroke on each bar
                  barShowStroke: true,
                  //Number - Pixel width of the bar stroke
                  barStrokeWidth: 2,
                  //Number - Spacing between each of the X value sets
                      barValueSpacing: 5,
                      //Number - Spacing between data sets within X values
                      barDatasetSpacing: 1,
                      //String - A legend template
                      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
                      //Boolean - whether to make the chart responsive
                      responsive: true,
                      maintainAspectRatio: true
                    };
            
                    barChartOptions.datasetFill = false;
                    barChart.Bar(barChartData, barChartOptions);
      
        }
        
    //getMyProgramsCreatedChart    
    getMyProgramsCreatedChart(){        
         this.http
            .post('//'+$(location).attr('hostname')+':4000/mock/creatorDashboardCharts')
            .map(response => response.json())
            .subscribe(
             data => this.checkMyProgramsCreatedChart(data.clientResponse),
             this.logError,
            () => console.log('next tab')
         );
    }
      
     checkMyProgramsCreatedChart(result) {        
        var activeCustomersLabel = [];
        var activeCustomersValue = [];        
        this.chartVal=JSON.stringify(result.myProgramsCreated);
        this.parsedCharVal = JSON.parse(this.chartVal);        
        $.each(this.parsedCharVal, function(key, value){
            activeCustomersLabel.push(key);
            activeCustomersValue.push(value)            
        });
        var areaChartData = {
          labels: activeCustomersLabel,
          datasets: [
            {
              label: "Electronics",
              fillColor: "rgba(210, 214, 222, 1)",
              strokeColor: "rgba(210, 214, 222, 1)",
              pointColor: "rgba(210, 214, 222, 1)",
              pointStrokeColor: "#c1c7d1",
              pointHighlightFill: "#fff",
              pointHighlightStroke: "rgba(220,220,220,1)",
              data: activeCustomersValue
            }
          ]
        };
        
        var barChartCanvas = $("#barChart2").get(0).getContext("2d");
        var barChart = new Chart(barChartCanvas);
        var barChartData = areaChartData;
        barChartData.datasets[0].fillColor = "#00a65a";
        barChartData.datasets[0].strokeColor = "#00a65a";
        barChartData.datasets[0].pointColor = "#00a65a";
        var barChartOptions = {          
          scaleBeginAtZero: true,          
          scaleShowGridLines: true,          
          scaleGridLineColor: "rgba(0,0,0,.05)",          
          scaleGridLineWidth: 1,          
          scaleShowHorizontalLines: true,          
          scaleShowVerticalLines: true,          
          barShowStroke: true,          
          barStrokeWidth: 2,          
              barValueSpacing: 5,          
              barDatasetSpacing: 1,          
              legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",          
              responsive: true,
              maintainAspectRatio: true
            };    
            barChartOptions.datasetFill = false;
            barChart.Bar(barChartData, barChartOptions);      
        }
    
        //getMyProgramViewsChart
        getMyProgramViewsChart(){        
         this.http
            .post('//'+$(location).attr('hostname')+':4000/mock/creatorDashboardCharts')
            .map(response => response.json())
            .subscribe(
             data => this.checkMyProgramViewsChart(data.clientResponse),
             this.logError,
            () => console.log('next tab')
         );
        }
      
     checkMyProgramViewsChart(result) {        
        var activeCustomersLabel = [];
        var activeCustomersValue = [];        
        this.chartVal=JSON.stringify(result.myProgramViews);
        this.parsedCharVal = JSON.parse(this.chartVal);        
        $.each(this.parsedCharVal, function(key, value){
            activeCustomersLabel.push(key);
            activeCustomersValue.push(value)            
        });
         
        var areaChartData = {
          labels: activeCustomersLabel,
          datasets: [
            {
              label: "Electronics",
              fillColor: "rgba(210, 214, 222, 1)",
              strokeColor: "rgba(210, 214, 222, 1)",
              pointColor: "rgba(210, 214, 222, 1)",
              pointStrokeColor: "#c1c7d1",
              pointHighlightFill: "#fff",
              pointHighlightStroke: "rgba(220,220,220,1)",
              data: activeCustomersValue
            }
          ]
        };
        
        var barChartCanvas = $("#barChart3").get(0).getContext("2d");
        var barChart = new Chart(barChartCanvas);
        var barChartData = areaChartData;
        barChartData.datasets[0].fillColor = "#00a65a";
        barChartData.datasets[0].strokeColor = "#00a65a";
        barChartData.datasets[0].pointColor = "#00a65a";
        var barChartOptions = {          
          scaleBeginAtZero: true,          
          scaleShowGridLines: true,          
          scaleGridLineColor: "rgba(0,0,0,.05)",          
          scaleGridLineWidth: 1,          
          scaleShowHorizontalLines: true,          
          scaleShowVerticalLines: true,          
          barShowStroke: true,          
          barStrokeWidth: 2,          
              barValueSpacing: 5,          
              barDatasetSpacing: 1,          
              legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",          
              responsive: true,
              maintainAspectRatio: true
            };    
            barChartOptions.datasetFill = false;
            barChart.Bar(barChartData, barChartOptions);      
        }
        
        //programViews start
    
        getMyPublishedProgramsChart(){        
         this.http
            .post('//'+$(location).attr('hostname')+':4000/mock/creatorDashboardCharts')
            .map(response => response.json())
            .subscribe(
             data => this.checkMyPublishedProgramsChart(data.clientResponse),
             this.logError,
            () => console.log('next tab')
         );
        }
      
     checkMyPublishedProgramsChart(result) {        
        var activeCustomersLabel = [];
        var activeCustomersValue = [];        
        this.chartVal=JSON.stringify(result.myPublishedPrograms);
        this.parsedCharVal = JSON.parse(this.chartVal);        
        $.each(this.parsedCharVal, function(key, value){
            activeCustomersLabel.push(key);
            activeCustomersValue.push(value)            
        });
         
        var areaChartData = {
          labels: activeCustomersLabel,
          datasets: [
            {
              label: "Electronics",
              fillColor: "rgba(210, 214, 222, 1)",
              strokeColor: "rgba(210, 214, 222, 1)",
              pointColor: "rgba(210, 214, 222, 1)",
              pointStrokeColor: "#c1c7d1",
              pointHighlightFill: "#fff",
              pointHighlightStroke: "rgba(220,220,220,1)",
              data: activeCustomersValue
            }
          ]
        };
        
        var barChartCanvas = $("#barChart4").get(0).getContext("2d");
        var barChart = new Chart(barChartCanvas);
        var barChartData = areaChartData;
        barChartData.datasets[0].fillColor = "#00a65a";
        barChartData.datasets[0].strokeColor = "#00a65a";
        barChartData.datasets[0].pointColor = "#00a65a";
        var barChartOptions = {          
          scaleBeginAtZero: true,          
          scaleShowGridLines: true,          
          scaleGridLineColor: "rgba(0,0,0,.05)",          
          scaleGridLineWidth: 1,          
          scaleShowHorizontalLines: true,          
          scaleShowVerticalLines: true,          
          barShowStroke: true,          
          barStrokeWidth: 2,          
              barValueSpacing: 5,          
              barDatasetSpacing: 1,          
              legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",          
              responsive: true,
              maintainAspectRatio: true
            };    
            barChartOptions.datasetFill = false;
            barChart.Bar(barChartData, barChartOptions);      
        }
    
    editProgram(){
        console.log(this);
        this.router.navigate(['CreatorEditProgram','CreatorEditContent']);
   }
    
}