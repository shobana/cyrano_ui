import {Component, EventEmitter, OnInit} from 'angular2/core';
import {CORE_DIRECTIVES, FORM_DIRECTIVES, NgClass, NgIf} from 'angular2/common';

import {HTTP_PROVIDERS}    from 'angular2/http';
import {Http, Response} from 'angular2/http';
import {Headers, RequestOptions} from 'angular2/http';
import {HTTP_BINDINGS} from 'angular2/http';
import 'rxjs/add/operator/map';
import {Injectable} from 'angular2/core';

import { Router,Location} from 'angular2/router';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from 'angular2/router';


@Component({  
  templateUrl: '/views/viewer/dashboard/viewerDashboardCharts.html',  
  directives: [NgClass, NgIf, CORE_DIRECTIVES, FORM_DIRECTIVES,ROUTER_DIRECTIVES],
  providers: [HTTP_PROVIDERS,HTTP_BINDINGS]
})
export class ViewerDashboardComponent implements OnInit {

  constructor(private http:Http,private router:Router,private location: Location) {
    this.http=http;
    this.router=router;
    this.location = location;      
   
    this.getMyProgramsByStateChart();        
  }
    
  getMyProgramsByStateChart(){        
         this.http
            .post('//'+$(location).attr('hostname')+':4000/mock/viewsDashboardCharts')
            .map(response => response.json())
            .subscribe(
             data => this.checkMyProgramsByStateCharts(data.clientResponse),
             this.logError,
            () => console.log('next tab')
         );
    }
      
  checkMyProgramsByStateCharts(result) {        
        var activeCustomersLabel = [];
        var activeCustomersValue = [];        
        this.chartVal=JSON.stringify(result.myProgramViews);
        this.parsedCharVal = JSON.parse(this.chartVal);        
        $.each(this.parsedCharVal, function(key, value){
            activeCustomersLabel.push(key);
            activeCustomersValue.push(value)            
        });
                
        var areaChartData = {
          labels: activeCustomersLabel,
          datasets: [
            {
              label: "Electronics",
              fillColor: "rgba(210, 214, 222, 1)",
              strokeColor: "rgba(210, 214, 222, 1)",
              pointColor: "rgba(210, 214, 222, 1)",
              pointStrokeColor: "#c1c7d1",
              pointHighlightFill: "#fff",
              pointHighlightStroke: "rgba(220,220,220,1)",
              data: activeCustomersValue
            }
          ]
        };
        
        var barChartCanvas = $("#barChart1").get(0).getContext("2d");
        var barChart = new Chart(barChartCanvas);
        var barChartData = areaChartData;
        barChartData.datasets[0].fillColor = "#00a65a";
        barChartData.datasets[0].strokeColor = "#00a65a";
        barChartData.datasets[0].pointColor = "#00a65a";
        var barChartOptions = {                  
          scaleBeginAtZero: true,                  
          scaleShowGridLines: true,                  
          scaleGridLineColor: "rgba(0,0,0,.05)",                  
          scaleGridLineWidth: 1,                  
          scaleShowHorizontalLines: true,                  
          scaleShowVerticalLines: true,                  
          barShowStroke: true,                  
          barStrokeWidth: 2,                  
              barValueSpacing: 5,                      
              barDatasetSpacing: 1,                      
              legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",                      
              responsive: true,
              maintainAspectRatio: true
            };            
            barChartOptions.datasetFill = false;
            barChart.Bar(barChartData, barChartOptions);
        }
}