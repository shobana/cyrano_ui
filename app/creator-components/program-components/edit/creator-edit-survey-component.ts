import { Component, EventEmitter, OnInit } from 'angular2/core';
import {CORE_DIRECTIVES, FORM_DIRECTIVES, NgClass, NgIf, NgFor} from 'angular2/common';

import {HTTP_PROVIDERS}    from 'angular2/http';
import {Http, Response} from 'angular2/http';
import {Headers, RequestOptions} from 'angular2/http';
import {HTTP_BINDINGS} from 'angular2/http';
import 'rxjs/add/operator/map';

import { Router } from 'angular2/router';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from 'angular2/router';

import {NG_TABLE_DIRECTIVES} from '../../../../node_modules/ng2-table/ng2-table';
import {NgTableSorting} from '../../../../node_modules/ng2-table/components/table/ng-table-sorting.directive';
import {PAGINATION_DIRECTIVES} from 'ng2-bootstrap/ng2-bootstrap';

import { MODAL_DIRECTIVES } from 'ng2-bs3-modal/ng2-bs3-modal';

@Component({
    selector: 'ngTable, [ngTable]',
    inputs: ['rows', 'columns', 'config'],
    outputs: ['tableChanged'],
    templateUrl: 'views/content-creator/programs/edit/creatorEditSurvey.html',
    directives: [NG_TABLE_DIRECTIVES, PAGINATION_DIRECTIVES, NgClass, NgIf, CORE_DIRECTIVES, FORM_DIRECTIVES, ROUTER_DIRECTIVES, NgTableSorting, NgClass, MODAL_DIRECTIVES],
    providers: [HTTP_PROVIDERS, HTTP_BINDINGS]
})

export class CreatorEditSurveyComponent implements OnInit {

    constructor(private http: Http, private router: Router) {
        this.http = http;
        this.router = router;
        this.page = 1;
        this.showMultiSelect = false;
        this.showMultiSelectEdit = true;
        this.showFreetext = false;
        this.showFreetextEdit = false;
        this.loadManageSurveyTable(this.page);

        this.QuestionDescriptionEdit = 'Town Hall Anual Meet Question One'
        this.OptionEdit1 = 'Test Option One'
        this.OptionEdit2 = 'Sample Option two'
        this.OptionEdit3 = 'Both Option One & Option Two '
        this.OptionEdit4 = 'None of Above'
        //this.QuestionType = "Free Text";
    }

    loadManageSurveyTable(page) {
        let body = JSON.stringify({ name });
        let headers = new Headers();
        let options = new RequestOptions({ headers: headers });
        return this.http
            .post('//' + $(location).attr('hostname') + ':4000/mock/manageSurveyList/' + page,
            body, options)
            .map(response => response.json())
            .subscribe(
            data => this.checkResponse(data),
            this.logError,
            () => console.log('Data fetching complete')
            );
    }

    checkResponse(response) {
        console.log(response);
        this.rows = response.QuestionVO;
        this.length = response.QuestionVO.length;
    }

    private columns: Array<any> = [
        { title: 'Type', name: 'type', sort: 'asc' },
        { title: 'Question', name: 'question' },
        { title: 'Correct Option', name: 'correctOption' },
        { title: 'Option 1', name: 'option1' },
        { title: 'Option 2', name: 'option2' },
        { title: 'Option 3', name: 'option3' },
        { title: 'Option 4', name: 'option4' },
        { title: 'Action' }
    ];

    public config: any = {
        paging: true,
        sorting: { columns: [] },
    };

    // Outputs (Events)
    public tableChanged: EventEmitter<any> = new EventEmitter();

    onChangeTable(config: any, page: any = config.paging) {
        console.log(page);
        this.loadManageSurveyTable(page.page);
    }

    changeSort(data: any, config: any) {
        console.log(this.config.sorting.columns);
    }

    moceNextOrPrevTabSurvey(result, tableFrom) {
        console.log("result --" + result);
        this.response = result;
        if (result == 'Ok') {
            if (tableFrom === 'SEGMENT') {
                this.router.navigate(['/CreatorHome', 'CreatorEditProgram', 'CreatorEditContent']);
            }
            if (tableFrom === 'FOOTER') {
                this.router.navigate(['/CreatorHome', 'CreatorEditProgram', 'CreatorEditPreview']);
            }

        }
    }

    moveNextOrPreviousSurvey(tableName) {
        let moveTableSurvey = tableName;
        console.log(moveTableSurvey);
        let body = JSON.stringify({ name });
        let headers = new Headers();
        let options = new RequestOptions({ headers: headers });
        this.http
            .post('//' + $(location).attr('hostname') + ':4000/mock/login',
            body, options)
            .map(response => response.json())
            .subscribe(
            data => this.moceNextOrPrevTabSurvey(data.status, moveTableSurvey),
            this.logError,
            () => console.log('next tab')
            );
    }

    public types: Type[] = [
        { "id": 1, "name": "Select Type" },
        { "id": 2, "name": "Multiple Choice" },
        { "id": 3, "name": "Free Text" }
    ];

    onSelect(productName) {
        if (productName == 'Select Type') {
            this.showMultiSelect = false;
            this.showFreetext = false;
        }
        if (productName == 'Multiple Choice') {
            this.showMultiSelect = true;
            this.showFreetext = false;
        }
        if (productName == 'Free Text') {
            this.showMultiSelect = false;
            this.showFreetext = true;
        }
    }

    onSelectEdit(productName) {
        if (productName == 'Select Type') {
            this.showMultiSelectEdit = false;
            this.showFreetextEdit = false;
        }
        if (productName == 'Multiple Choice') {
            this.showMultiSelectEdit = true;
            this.showFreetextEdit = false;
        }
        if (productName == 'Free Text') {
            this.showMultiSelectEdit = false;
            this.showFreetextEdit = true;
        }
    }
}
