import { Component } from 'angular2/core';
import { CORE_DIRECTIVES } from 'angular2/common';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS, RouteParams } from 'angular2/router';


import { CreatorEditProgramIntroComponent } from './creator-edit-programintro-component';
import { CreatorEditThemeHeaderComponent } from './creator-edit-theme-header-component';
import { CreatorEditContentComponent } from './creator-edit-content-component';
import { CreatorEditSurveyComponent } from './creator-edit-survey-component';
import { CreatorEditThemeFooterComponent } from './creator-edit-theme-footer-component';
import { CreatorEditPreviewComponent } from './creator-edit-preview-component';
import { CreatorEditPublishProgramComponent } from './creator-edit-publishprogram-component';

import {Location,Router} from "angular2/router";

@Component({ 
  templateUrl: 'views/content-creator/programs/edit/creatorEditProgram.html',
  directives: [ROUTER_DIRECTIVES]
})

@RouteConfig([
  { path: '/creatoreditprogramintro', name: 'CreatorEditProgramIntro', component: CreatorEditProgramIntroComponent},
  { path: '/creatoreditthemeheader', name: 'CreatorEditThemeHeader', component: CreatorEditThemeHeaderComponent},
  { path: '/creatoreditsegment', name: 'CreatorEditContent', component: CreatorEditContentComponent},
  { path: '/creatoreditsurvey', name: 'CreatorEditSurvey', component: CreatorEditSurveyComponent},
  { path: '/creatoreditthemefooter', name: 'CreatorEditThemeFooter', component: CreatorEditThemeFooterComponent},
  { path: '/creatoreditpreview', name: 'CreatorEditPreview', component: CreatorEditPreviewComponent}
  { path: '/creatoreditpublishprogram', name: 'CreatorEditPublishProgram', component: CreatorEditPublishProgramComponent}
])
    
export class CreatorEditProgramComponent implements onInit {
    
    constructor(private location:Location,private router:Router, private params: RouteParams) {
        this.location=location; 
        this.router=router; 
        this.ckFromScriptedProg = params.get('from');
        this.contentUUID = params.get('contentUUID');
        console.log(this.contentUUID);
    }
    
    ngOnInit(){
       //console.log("edit path--"+this.location.path());      
    }
    
    getLinkStyle(path) {
        return this.location.path().indexOf(path) > -1;
    }
      
    close(){
        this.router.navigate(['CreatorMyPrograms']); 
    }
}
