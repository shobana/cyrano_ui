import { Component } from 'angular2/core';
import { CORE_DIRECTIVES } from 'angular2/common';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS, RouteParams } from 'angular2/router';


import { PublisherIntroComponent } from './publisherintro-component';
import { ThemeHeaderComponent } from './theme-header-component';
import { SegmentComponent } from './segment-component';
import { SurveyComponent } from './survey-component';
import { ThemeFooterComponent } from './theme-footer-component';
import { PreviewComponent } from './preview-component';
import { PublishprogramComponent } from './publishprogram-component';

import {Location,Router} from "angular2/router";

@Component({ 
  templateUrl: 'views/content-creator/programs/addNewProgram.html',
  directives: [ROUTER_DIRECTIVES]
})

@RouteConfig([
  { path: '/publisherIntro/:contentUUID', name: 'PublisherIntro', component: PublisherIntroComponent},
  { path: '/themeHeader', name: 'ThemeHeader', component: ThemeHeaderComponent},
  { path: '/segment/:contentUUID', name: 'Segment', component: SegmentComponent},
  { path: '/survey', name: 'Survey', component: SurveyComponent},
  { path: '/themefooter', name: 'ThemeFooter', component: ThemeFooterComponent},
  { path: '/preview', name: 'Preview', component: PreviewComponent}
  { path: '/publishprogram', name: 'Publishprogram', component: PublishprogramComponent}
])
    
export class CreatorAddNewProgramComponent {
    
    constructor(private location:Location,private router:Router, private params: RouteParams) {
        this.location=location; 
        this.router=router; 
        
        this.ckFromScriptedProg = params.get('from');
        this.contentUUID = params.get('contentUUID');  
    }
    
    getLinkStyle(path) {
        return this.location.path().indexOf(path) > -1;
    }
      
     close(){
        this.router.navigate(['CreatorMyPrograms']); 
    }
    
    selectContentFromScript(){
        this.router.navigate(['ShowContentBeforeAddNewProgram',{ 'from':'scriptedProgram'}]);
    }
}
