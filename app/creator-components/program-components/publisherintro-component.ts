import { Component,View, Input, Output, EventEmitter, AfterViewInit} from 'angular2/core';

import { Router ,RouteParams} from 'angular2/router';

import {HTTP_PROVIDERS}    from 'angular2/http';
import {Http, Response} from 'angular2/http';
import {Headers, RequestOptions} from 'angular2/http';
import {HTTP_BINDINGS} from 'angular2/http';
import 'rxjs/add/operator/map';
import {ImageCropperComponent, CropperSettings} from '../../../node_modules/ng2-img-cropper/src/imageCropper';
import { MODAL_DIRECTIVES } from 'ng2-bs3-modal/ng2-bs3-modal';

@Component({ 
selector: 'test-app',
  templateUrl: 'views/content-creator/programs/programintro.html',
    providers: [HTTP_PROVIDERS,HTTP_BINDINGS],
    directives: [ImageCropperComponent, MODAL_DIRECTIVES]
})
    
export class PublisherIntroComponent implements AfterViewInit{
        
    data: any;
    cropperSettings: CropperSettings;
        
    constructor (private router:Router,private http:Http, private params: RouteParams) {
        this.router=router;
        this.http=http;
        
        //getting contentUUID value from state params
        this.contentUUID = params.get('contentUUID');
        this.ckFromScriptedProg = params.get('from');
        
        this.cropperSettings = new CropperSettings();
        this.cropperSettings.width = 100;
        this.cropperSettings.height = 100;
        this.cropperSettings.croppedWidth =100;
        this.cropperSettings.croppedHeight = 100;
        this.cropperSettings.canvasWidth = 400;
        this.cropperSettings.canvasHeight = 300;
 
        this.data = {};
        
        this.isImgUploaded = true;
        this.isImgEditable = false;
    }
    
    checkNextTab(result, tableFrom) {
        console.log("result --"+result);
        this.response=result;
         if(result=='Ok'){
            if(tableFrom==='THEME'){
                this.router.navigate(['/CreatorHome','AddNewProgram',{contentUUID:this.contentUUID,'from':this.ckFromScriptedProg},'Segment',{contentUUID:this.contentUUID,'from':this.ckFromScriptedProg}]);    
            }
             
         }
     }

    moveToThemeTab(tableName) {
        let moveTableTo = tableName;
        console.log(moveTableTo);        
        let body = JSON.stringify({ name });
        let headers = new Headers();
        let options = new RequestOptions({ headers: headers });
        this.http
            .post('//'+$(location).attr('hostname')+':4000/mock/login',
             body,options)
            .map(response => response.json())
            .subscribe(
             data => this.checkNextTab(data.status, moveTableTo),
             this.logError,
            () => console.log('next tab')
         );
     }   
    
    readUrl($event): void {
        this.postFile($event.target);
    }
    
    postFile(inputValue: any): void {

    var formData = new FormData();
    formData.append("name", "Name");
    formData.append("file",  inputValue.files[0]);
    console.log(inputValue.files[0].name);
        this.filename=inputValue.files[0].name;
    if (inputValue.files && inputValue.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(200);
            };

            reader.readAsDataURL(inputValue.files[0]);
      }
      /*this.http.post(this.url +,
       formData ,
         {
            headers: this.headers

         });*/
     }
    
     changeBtnShow(){
        this.isImgUploaded = false;
        this.isImgEditable = true;     
     }
    
    /*ngAfterViewInit() {
            $(function () { 
            console.log('jquery');                
            $('#FileUpload1').change(function () {
                console.log('upload');
                $('#Image2').hide();
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#Image2').show();
                    $('#Image2').attr("src", e.target.result);
                   $('#Image2').Jcrop({
                        onChange: SetCoordinates,
                        onSelect: SetCoordinates
                    });
                }
                reader.readAsDataURL($(this)[0].files[0]);
            });
         
            $('#btnCrop').click(function () {
                console.log('btnCrop');
                var x1 = $('#imgX1').val();
                var y1 = $('#imgY1').val();
                var width = $('#imgWidth').val();
                var height = $('#imgHeight').val();
                var canvas = $("#canvas")[0];
                var context = canvas.getContext('2d');
                var img = new Image();
                img.onload = function () {
                    canvas.height = height;
                    canvas.width = width;
                    context.drawImage(img, x1, y1, width, height, 0, 0, width, height);
                    $('#imgCropped').val(canvas.toDataURL());
                };
                img.src = $('#Image1').attr("src");
            });
        });
        function SetCoordinates(c) {
            console.log('SetCoordinates');
            $('#imgX1').val(c.x);
            $('#imgY1').val(c.y);
            $('#imgWidth').val(c.w);
            $('#imgHeight').val(c.h);
            $('#btnCrop').show();
        };
    }*/
}
