import { Component,ViewChild } from 'angular2/core';
import {Component, EventEmitter, OnInit} from 'angular2/core';
import {CORE_DIRECTIVES, FORM_DIRECTIVES, NgClass, NgIf} from 'angular2/common';

import {PAGINATION_DIRECTIVES} from 'ng2-bootstrap/ng2-bootstrap';
import {NG_TABLE_DIRECTIVES} from '/node_modules/ng2-table/ng2-table';
import {NgTableSorting} from '/node_modules/ng2-table/components/table/ng-table-sorting.directive';

import {HTTP_PROVIDERS}    from 'angular2/http';
import {Http, Response} from 'angular2/http';
import {Headers, RequestOptions} from 'angular2/http';
import {HTTP_BINDINGS} from 'angular2/http';
import 'rxjs/add/operator/map';

import { Router ,RouteParams} from 'angular2/router';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from 'angular2/router';

import { MODAL_DIRECTIVES } from 'ng2-bs3-modal/ng2-bs3-modal';

import {DND_PROVIDERS, DND_DIRECTIVES} from 'ng2-dnd/ng2-dnd';

@Component({
    selector: 'ngTable, [ngTable]',
    inputs: ['rows', 'columns', 'config'],
    outputs: ['tableChanged'],
    templateUrl: 'views/content-creator/programs/contentsForCreateProgram.html',
    directives: [NG_TABLE_DIRECTIVES, PAGINATION_DIRECTIVES, NgClass, NgIf, CORE_DIRECTIVES, FORM_DIRECTIVES, NgTableSorting, NgClass,MODAL_DIRECTIVES,ROUTER_DIRECTIVES,DND_DIRECTIVES],
    providers: [HTTP_PROVIDERS, HTTP_BINDINGS]
})

export class ShowContentBeforeAddNewProgram {
    
    constructor(private http: Http, private router: Router, private params: RouteParams) {
        this.http = http;
        this.router = router;        
        this.page = 1;
        this.ckFromScriptedProg = params.get('from');
        this.loadManageProgramsListTable(this.page);
    }    
   
    /* select content grid start*/
    loadManageProgramsListTable(page) {
        let body = JSON.stringify({ name });
        let headers = new Headers();
        let options = new RequestOptions({ headers: headers });
        return this.http
            .post('//' + $(location).attr('hostname') + ':4000/mock/manageContent/' + page,
            body, options)
            .map(response => response.json())
            .subscribe(
            data => this.checkResponse(data),
            this.logError,
            () => console.log('Data fetching complete')
            );
    }

    checkResponse(response) {
        this.rows = response.myContentsList;
        this.length = response.total;
    }

    public config: any = {
        paging: true,
        sorting: { columns: [] },
    };
    
    // Outputs (Events)
    public tableChanged: EventEmitter<any> = new EventEmitter();

    onChangeTable(config: any, page: any = config.paging) {
        console.log(page);
        this.loadManageProgramsListTable(page.page);
    }   
    /* select content grid ends*/
    
    loadAddNewProgramContent(){        
        this.router.navigate(['AddNewProgram',{ contentUUID: 'NA'},'Segment',{ contentUUID: 'NA'}]);    
    }    
    loadAddNewProgramIntro(){        
        this.router.navigate(['AddNewProgram',{ contentUUID: 'NA'},'PublisherIntro',{ contentUUID: 'NA'}]);    
    }    
    loadMyProgramGrid(){
        this.router.navigate(['CreatorMyPrograms']);
    }
    
    loadAddNewScriptedProgramContent(){        
        this.router.navigate(['AddNewProgram',{'from':'scriptedProgram', contentUUID: 'NA'},'Segment',{ 'from':'scriptedProgram',contentUUID: 'NA'}]);    
    }    
    loadAddNewScriptedProgramIntro(){        
        this.router.navigate(['AddNewProgram',{ 'from':'scriptedProgram', contentUUID: 'NA'},'PublisherIntro',{ 'from':'scriptedProgram',contentUUID: 'NA'}]);    
    }    
    loadMyScriptedProgramGrid(){
        this.router.navigate(['MyScriptedPrograms']);
    }
}
