import { Component,ViewChild } from 'angular2/core';
import {Component, EventEmitter, OnInit} from 'angular2/core';
import {CORE_DIRECTIVES, FORM_DIRECTIVES, NgClass, NgIf} from 'angular2/common';

import {PAGINATION_DIRECTIVES} from 'ng2-bootstrap/ng2-bootstrap';
import {NG_TABLE_DIRECTIVES} from '/node_modules/ng2-table/ng2-table';
import {HTTP_PROVIDERS}    from 'angular2/http';
import {Http, Response} from 'angular2/http';
import {Headers, RequestOptions} from 'angular2/http';
import {HTTP_BINDINGS} from 'angular2/http';
import 'rxjs/add/operator/map';
import { Router } from 'angular2/router';
import {NgTableSorting} from '/node_modules/ng2-table/components/table/ng-table-sorting.directive';
import {CORE_DIRECTIVES, FORM_DIRECTIVES, NgClass, NgFor} from 'angular2/common';

import { MODAL_DIRECTIVES } from 'ng2-bs3-modal/ng2-bs3-modal';

@Component({
    selector: 'ngTable, [ngTable]',
    inputs: ['rows', 'columns', 'config'],
    outputs: ['tableChanged'],
    templateUrl: 'views/content-creator/programs/themeHeader.html',
    directives: [NG_TABLE_DIRECTIVES, PAGINATION_DIRECTIVES, NgClass, NgIf, CORE_DIRECTIVES, FORM_DIRECTIVES, NgTableSorting, NgClass,MODAL_DIRECTIVES],
    providers: [HTTP_PROVIDERS, HTTP_BINDINGS]
})

export class ThemeHeaderComponent implements OnInit {
   
    @ViewChild('recordContentModal')
    recordContentModal: ModalComponent;
    
    constructor(private http: Http, private router: Router) {
        this.http = http;
        this.page = 1;
        this.loadManageProgramsListTable(this.page);
        this.loadManageProgramsListTableCurrent(this.page);

        this.showAudioElements = true;
        this.showAudio = true;
        this.showVideo = true;
        this.showVideoElements = false;
        this.showUrl = false;
        this.showBrowse = false;
        this.showRecord=false;
        
        this.defaultAudioBtn = false;
        this.styledAudioBtn = true;
        this.defaultVideoBtn = true;
        this.styledVideoBtn = false;
        
        this.ckeditorContent="This is my textarea to be replaced with CKEditor.";
        
        this.showCurrentHeaderSec = false;
        this.showSelectHeaderSec = true;
        
        this.showSaveBtnFlag=false;
        
        this.showRecordDetails = false;
    }

    provideCurrentHeaderSec() {
        this.showCurrentHeaderSec = true;
        this.showSelectHeaderSec = false;
        $('#showAddNewHeaderSecId').hide();
        this.showSaveBtnFlag=false;
    }

    provideSelectHeaderSec() {
        this.showCurrentHeaderSec = false;
        this.showSelectHeaderSec = true;
        $('#showAddNewHeaderSecId').hide();
        this.showSaveBtnFlag=false;
    }

    provideAddNewHeaderSec() {
        this.showCurrentHeaderSec = false;
        this.showSelectHeaderSec = false;
        $('#showAddNewHeaderSecId').show();
        this.showAudioElements = true;
        $('#showAudioElementsId').show();
        this.showSaveBtnFlag=true;
    }

    showAudioElement() {
        this.showAudioElements = true;
        $('#showAudioElementsId').show();
        this.showOr = false;
        this.showVideo = true;
        this.showVideoElements = false;
        $('#showVideoElementsId').hide();
        this.showAudioUrl = false;
        this.showAudioBrowse = false;
       
        //Btn Side CSS 
        this.defaultAudioBtn = false;
        this.styledAudioBtn = true;
        this.defaultVideoBtn = true;
        this.styledVideoBtn = false;
    }

    showVideoElement() {
        this.showAudio = true;
        this.showAudioElements = false;
        $('#showAudioElementsId').hide();
        this.showOr = false;
        this.showVideo = true;
        this.showVideoElements = true;
        $('#showVideoElementsId').show();
      
        //Btn Side CSS 
        this.defaultAudioBtn = true;
        this.styledAudioBtn = false;
        this.defaultVideoBtn = false;
        this.styledVideoBtn = true;
    }

    showInitialSetup() {
        this.showAudioElements = false;
        $('#showAudioElementsId').hide();
        this.showAudio = true;
        this.showOr = true;
        this.showVideo = true;
        this.showVideoElements = false;
        $('#showVideoElementsId').hide();
    }

     showUrlOption() {
        this.showUrl = true;
        this.showBrowse = false;
        this.showRecord=false;
         
        this.showRecordDetails = false;
    }

    showBrowseOption() {
        this.showUrl = false;
        this.showBrowse = true;
        this.showRecord=false;
        
        this.showRecordDetails = false;
    }

    showRecordOption() {
        this.showUrl = false;
        this.showBrowse = false;
        this.showRecord=true;
        
        this.showRecordDetails = false;
    }
    
    /* current selected grid starts*/
    loadManageProgramsListTableCurrent(page) {
        let body = JSON.stringify({ name });
        let headers = new Headers();
        let options = new RequestOptions({ headers: headers });
        return this.http
            .post('//' + $(location).attr('hostname') + ':4000/mock/manageSelectedThemes/' + page,
            body, options)
            .map(response => response.json())
            .subscribe(
            data => this.checkResponseCurrent(data),
            this.logError,
            () => console.log('Data fetching complete')
            );
    }

    checkResponseCurrent(response) {
        this.rowsCurrent = response.myThemesList;
        this.lengthCurrent = response.total;
    }
        
    /*public config:any = {
      paging: true,
      sorting: {columns: []},
    };*/

    // Outputs (Events)
    public tableChanged: EventEmitter<any> = new EventEmitter();

    onChangeTable(config: any, page: any = config.paging) {
        console.log(page);
        this.loadManageProgramsListTableCurrent(page.page);
    }
    /* current selected grid ends*/
    
    /* selected grid starts*/
    loadManageProgramsListTable(page) {
        let body = JSON.stringify({ name });
        let headers = new Headers();
        let options = new RequestOptions({ headers: headers });
        return this.http
            .post('//' + $(location).attr('hostname') + ':4000/mock/manageThemes/' + page,
            body, options)
            .map(response => response.json())
            .subscribe(
            data => this.checkResponse(data),
            this.logError,
            () => console.log('Data fetching complete')
            );
    }

    checkResponse(response) {
        this.rows = response.myThemesList;
        this.length = response.total;
    }

    public config: any = {
        paging: true,
        sorting: { columns: [] },
    };

    // Outputs (Events)
    public tableChanged: EventEmitter<any> = new EventEmitter();

    onChangeTable(config: any, page: any = config.paging) {
        console.log(page);
        this.loadManageProgramsListTable(page.page);
    }
    /* selected grid ends*/
    moceNextOrPrevTab(result, tableFrom) {
        console.log("result --" + result);
        this.response = result;
        if (result == 'Ok') {
            if (tableFrom === 'INTRO') {
                this.router.navigate(['/CreatorHome', 'AddNewProgram', 'PublisherIntro']);
            }
            if (tableFrom === 'SEGMENT') {
                this.router.navigate(['/CreatorHome', 'AddNewProgram', 'Segment']);
            }

        }
    }

    moveNextOrPrevious(tableName) {
        let moveTable = tableName;
        console.log(moveTable);
        let body = JSON.stringify({ name });
        let headers = new Headers();
        let options = new RequestOptions({ headers: headers });
        this.http
            .post('//' + $(location).attr('hostname') + ':4000/mock/login',
            body, options)
            .map(response => response.json())
            .subscribe(
            data => this.moceNextOrPrevTab(data.status, moveTable),
            this.logError,
            () => console.log('next tab')
            );
    }

    ngOnInit() {

    }
    
    saveRecord() {
        $('#recordContentModal').modal('hide');
        $('.modal-backdrop').remove();

        this.showRecordDetails = true;

        this.showUrl = false;
        this.showBrowse = false;
        this.showRecord = false;
    }

    //Recording Dialog
     openRecordContentModal() {
         this.recordContentModal.open('lg');
         
         this.recordingUrl="https://rtc.ibotsystems.com/stream-recorder/";
         
     }
    
}
