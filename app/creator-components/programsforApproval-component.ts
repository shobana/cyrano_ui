import {Component, EventEmitter, OnInit} from 'angular2/core';
import {CORE_DIRECTIVES, FORM_DIRECTIVES, NgClass, NgIf} from 'angular2/common';

import {HTTP_PROVIDERS}    from 'angular2/http';
import {Http, Response} from 'angular2/http';
import {Headers, RequestOptions} from 'angular2/http';
import {HTTP_BINDINGS} from 'angular2/http';
import 'rxjs/add/operator/map';
import {Injectable} from 'angular2/core';

import { Router,Location} from 'angular2/router';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from 'angular2/router';

import {NG_TABLE_DIRECTIVES} from '../../node_modules/ng2-table/ng2-table';
import {NgTableSorting} from '../../node_modules/ng2-table/components/table/ng-table-sorting.directive';
import {PAGINATION_DIRECTIVES} from 'ng2-bootstrap/ng2-bootstrap';

import { MODAL_DIRECTIVES } from 'ng2-bs3-modal/ng2-bs3-modal';


@Component({ 
  selector: 'ngTable, [ngTable]',
  inputs: ['rows', 'columns', 'config'],
  outputs: ['tableChanged'],   
  templateUrl: '/views/content-manager/programsforApproval.html',  
  directives: [NgClass, NgIf, CORE_DIRECTIVES, FORM_DIRECTIVES,ROUTER_DIRECTIVES,NG_TABLE_DIRECTIVES,NgTableSorting,PAGINATION_DIRECTIVES,MODAL_DIRECTIVES],
  providers: [HTTP_PROVIDERS,HTTP_BINDINGS]
})
export class ProgramsforApprovalComponent implements OnInit {

  constructor(private http:Http,private router:Router,private location: Location) {
    this.http=http;
    this.router=router;
    this.location = location;
    this.getNewPrograms(this.page);    
  }
    
    /* table section starts*/
    getNewPrograms(page) {
        let body = JSON.stringify({ name });
        let headers = new Headers();
        let options = new RequestOptions({ headers: headers });
        return this.http
            .post('//' + $(location).attr('hostname') + ':4000/mock/managerdashboardNewProgram/' + page,
            body, options)
            .map(response => response.json())
            .subscribe(
            data => this.checkResponse(data),
            this.logError,
            () => console.log('Data fetching complete')
            );
    }

    checkResponse(response) {        
        this.rows = response.newPrograms;
        this.length = response.newPrograms.length;
    }

    private columns: Array<any> = [
        { title: 'Name', name: 'name' },
        { title: 'Author', name: 'author'},
        { title: 'Publish Date', name: 'publishDate' },
        { title: 'Actions', name: 'programUUID' }
    ];

    public config: any = {
        paging: true,
        sorting: { columns: [] },
    };

    public tableChanged: EventEmitter<any> = new EventEmitter();

    onChangeTable(config: any, page: any = config.paging) {
        //console.log(page);
        this.getNewPrograms(page.page);
    }
    //table end
        
    editProgram(){
        //console.log(this);
        this.router.navigate(['ManagerEditProgram','ManagerEditProgramIntro']);
   }
}