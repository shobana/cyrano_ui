import {Component, EventEmitter, OnInit} from 'angular2/core';
import {CORE_DIRECTIVES, FORM_DIRECTIVES, NgClass, NgIf} from 'angular2/common';
import {PAGINATION_DIRECTIVES} from 'ng2-bootstrap/ng2-bootstrap';

import {NG_TABLE_DIRECTIVES} from '../../node_modules/ng2-table/ng2-table';

import {HTTP_PROVIDERS}    from 'angular2/http';
import {Http, Response} from 'angular2/http';
import {Headers, RequestOptions} from 'angular2/http';
import {HTTP_BINDINGS} from 'angular2/http';
import 'rxjs/add/operator/map';
import {Injectable} from 'angular2/core';

import { Router } from 'angular2/router';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from 'angular2/router';
import {NgTableSorting} from '../../node_modules/ng2-table/components/table/ng-table-sorting.directive';
import {CORE_DIRECTIVES, FORM_DIRECTIVES, NgClass, NgFor} from 'angular2/common';

import { MODAL_DIRECTIVES } from 'ng2-bs3-modal/ng2-bs3-modal';
import {TruncatePipe} from '../truncate/truncate';

//import {UiSortableComponent} from '../../node_modules/angular-ui-sortable/dist/sortable';
//import {Draggable} from '../../dist/js/rx-draggable/directives/draggable';
///,UiSortableComponent
//import {UiSortableComponent} from '../../dist/js/ui-sortable/sortable';

@Component({
  selector: 'ngTable, [ngTable]',
  inputs: ['rows', 'columns', 'config'],
  outputs: ['tableChanged'],
  templateUrl: '/views/content-creator/programs/managePrograms.html',  
  directives: [NG_TABLE_DIRECTIVES, PAGINATION_DIRECTIVES, NgClass, NgIf, CORE_DIRECTIVES, FORM_DIRECTIVES,ROUTER_DIRECTIVES,NgTableSorting, NgClass,MODAL_DIRECTIVES],
  providers: [HTTP_PROVIDERS,HTTP_BINDINGS],
  pipes: [TruncatePipe]
})
// background-image: url(https://cdn.rawgit.com/Reactive-Extensions/rx.angular.js/master/examples/draganddrop/logo.png);   
export class CreatorMyProgramsComponent implements OnInit {

   //items: Array<string>;
  // sortableOptions: Object;

    /*this.items = [
      'Item 1',
      'Item 2',
      'Item 3',
      'Item 4',
      'Item 5'
    ];*/
    
    /*public rows:Array<any> = [];
  public columns:Array<any> = [
    {title: 'Name', name: 'name'},
    {title: 'Position', name: 'position', sort: false},
    {title: 'Office', name: 'office', sort: 'asc'},
    {title: 'Extn.', name: 'ext', sort: 'desc'},
    {title: 'Start date', name: 'startDate'},
    {title: 'Salary', name: 'salary'}
  ];
  public page:number = 1;
  public itemsPerPage:number = 10;
  public maxSize:number = 5;
  public numPages:number = 1;
  public length:number = 0;

  public config:any = {
    paging: true,
    sorting: {columns: []},
    filtering: {filterString: '', columnName: 'position'}
  };

  private data:Array<any> = TableData;

  constructor(private http:Http,private router:Router) {
    this.length = this.data.length;
    this.http=http;
    this.router=router;
    this.loadData();
  }

  ngOnInit() {
    this.onChangeTable(this.config, null);
  }

  changePage(page:any, data:Array<any> = this.data):Array<any> {
   /* console.log(page);
    let start = (page.page - 1) * page.itemsPerPage;
    let end = page.itemsPerPage > -1 ? (start + page.itemsPerPage) : data.length;
    return data.slice(start, end);*/
    
    /*console.log(page);
    console.log("Page number --"+page.page);

    let body = JSON.stringify({ name });
    let headers = new Headers();
    let options = new RequestOptions({ headers: headers });
    this.http
        .post('//'+$(location).attr('hostname')+':4000/mock/list/'+page.page  ,
           body,options)
            .map(response => response.json())
            .subscribe(
             data => this.rows=data,
             this.logError,
            () => console.log('Data fetching complete')
    );
    
    return this.rows;
  }

  changeSort(data:any, config:any) {
    if (!config.sorting) {
      return data;
    }

    // simple sorting
    return data.sort((previous:any, current:any) => {
      let columns = this.config.sorting.columns || [];
      for (let i = 0; i < columns.length; i++) {
        let columnName = columns[i].name;

        if (previous[columnName] > current[columnName]) {
          return columns[i].sort === 'desc' ? -1 : 1;
        }
        if (previous[columnName] < current[columnName]) {
          return columns[i].sort === 'asc' ? -1 : 1;
        }
      }
      return 0;
    });
  }

  changeFilter(data:any, config:any):any {
    if (!config.filtering) {
      return data;
    }

    let filteredData:Array<any> = data.filter((item:any) =>
      item[config.filtering.columnName].match(this.config.filtering.filterString));

    return filteredData;
  }

  onChangeTable(config:any, page:any = config.paging) {
    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
    }
    if (config.sorting) {
      Object.assign(this.config.sorting, config.sorting);
    }

    let filteredData = this.changeFilter(this.data, this.config);
    let sortedData = this.changeSort(filteredData, this.config);
    this.rows = page && config.paging ? this.changePage(page, sortedData) : sortedData;
    this.length = sortedData.length;
      
     //this.programsList=JSON.stringify(this.rows);
     this.programsList=this.rows;
     //this.programsList=JSON.parse(this.rows);
      console.log("this.programsList"+this.programsList);
  }
    
     });
         
    }*/
  // Table values
  constructor(private http:Http,private router:Router) {
    this.http=http;
    this.router=router;
    this.page=1;
    this.loadManageProgramsListTable(this.page);
    this._tempSelectedDivIds = [];
      
    $('#myCarousel').carousel({
            interval: 0;
    })
      
  }
    
   addNewProgram(){
        console.log(this);
        //this.router.navigate(['AddNewProgram','PublisherIntro']);
        //this.router.navigate(['AddNewProgram',{ contentUUID: 'NA'},'PublisherIntro',{ contentUUID: 'NA'}]);
        this.router.navigate(['ShowContentBeforeAddNewProgram']);
   }
  
  editProgram(){
        console.log(this);
        this.router.navigate(['CreatorEditProgram','CreatorEditProgramIntro']);
   }
  
  loadManageProgramsListTable(page){
    let body = JSON.stringify({ name });
    let headers = new Headers();
    let options = new RequestOptions({headers: headers });
    return this.http
        .post('//'+$(location).attr('hostname')+':4000/mock/manageProgramsList/'+page,
           body,options)
            .map(response => response.json())
            .subscribe(
               data => this.checkResponse(data),
             this.logError,
            () => console.log('Data fetching complete')
    );
  } 
       
  checkResponse(response){
     this.rows= response.programsList;
     this.length =response.total;
  }
        
   private columns:Array<any> = [
    {title: 'Name', name: 'programName'},
    {title: 'Channels', name: 'channel'},
    {title: 'State', name: 'state'},
    {title: 'Private ?', name: 'isPrivate'},    
    {title: 'Tag', name: 'tag'},
    {title: 'Publish Date', name: 'publishDate'},    
    {title: 'Expiry Date', name: 'expiryDate'},
    {title: 'Updated On', name: 'updatedOn'},
    {title: 'Updated By', name: 'updatedBy'},
    {title: 'Action'}    
  ];
    
  public config:any = {
    paging: true,
    sorting: {columns: []},
  };

  // Outputs (Events)
  public tableChanged:EventEmitter<any> = new EventEmitter();

   onChangeTable(config:any, page:any = config.paging) {
        console.log(page);
        this.loadManageProgramsListTable(page.page);
   }
    
   changeSort(data:any, config:any) {
       console.log(this.config.sorting.columns);
    /*if (!config.sorting) {
      return data;
    }

    // simple sorting
    return data.sort((previous:any, current:any) => {
      let columns = this.config.sorting.columns || [];
      for (let i = 0; i < columns.length; i++) {
        let columnName = columns[i].name;

        if (previous[columnName] > current[columnName]) {
          return columns[i].sort === 'desc' ? -1 : 1;
        }
        if (previous[columnName] < current[columnName]) {
          return columns[i].sort === 'asc' ? -1 : 1;
        }
      }
      return 0;
    });*/
  }
  /*public set columns(values:Array<any>) {
    values.forEach((value) => {
      let column = this._columns.find((col) => col.name === value.name);
      if (column) {
        Object.assign(column, value);
      }
      if (!column) {
        this._columns.push(value);
      }
    });
  }

  public get columns() {
    return this._columns;
  }

  public get configColumns() {
    let sortColumns:Array<any> = [];

    this.columns.forEach((column) => {
      if (column.sort) {
        sortColumns.push(column);
      }
    });

    return {columns: sortColumns};
  }

  /*onChangeTable(column:any) {
    this.columns = [column];
    this.tableChanged.emit({sorting: this.configColumns});
  }*/
  
  viewProgramComments(programUUID,status){
    this.router.navigate(['/CreatorHome','ViewProgramComments', {programUUID:programUUID,status:status}]);     
  }
    
  selectChannel(event){
    $("#testDivId1").removeClass("carouselSelect");         
    this.imgContentId = event.target.id;      
    this.added=false;
    this.checkFlag = false;
      
     if($.inArray( this.imgContentId, this._tempSelectedDivIds ) != -1){         
         this._tempSelectedDivIds.splice($.inArray(this.imgContentId, this._tempSelectedDivIds),1);          
         
         if($("#"+this.imgContentId).hasClass('carouselSelect')) {      
              $("#"+this.imgContentId).removeClass("carouselSelect");              
            }else{        
                    $("#"+this.imgContentId).addClass("carouselSelect");
                $.each(this._tempSelectedDivIds, function( index, value ) {                      
                    $("#"+value).removeClass("carouselSelect");          
                });
              
                this._tempSelectedDivIds.push(this.imgContentId)               
            }
         console.log("irukku..");
         this.added=true;  
    }
       
    if (!this.added) {
      console.log('illai');
      $("#"+this.imgContentId).addClass("carouselSelect");
      $.each(this._tempSelectedDivIds, function( index, value ) {
          $("#"+value).removeClass("carouselSelect");          
      });
      this._tempSelectedDivIds.push(this.imgContentId)
    }
  }
    
  ngOnInit() {
    $("#testDivId1").addClass("carouselSelect");
  }
    
}