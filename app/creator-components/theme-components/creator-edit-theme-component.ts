import {Component, EventEmitter, OnInit,ViewChild} from 'angular2/core';

import {HTTP_PROVIDERS}    from 'angular2/http';
import {Http, Response} from 'angular2/http';
import {Headers, RequestOptions} from 'angular2/http';
import {HTTP_BINDINGS} from 'angular2/http';
import 'rxjs/add/operator/map';

import { Router } from 'angular2/router';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from 'angular2/router';

import { MODAL_DIRECTIVES } from 'ng2-bs3-modal/ng2-bs3-modal';

@Component({
    templateUrl: '/views/content-creator/theme/editHeaderFooter.html',
    directives: [ROUTER_DIRECTIVES,MODAL_DIRECTIVES],
    providers: [HTTP_PROVIDERS, HTTP_BINDINGS]
})

export class CreatorEditThemeComponent implements OnInit,,AfterViewInit {

   @ViewChild('recordContentModal')
    recordContentModal: ModalComponent;
    
    @ViewChild('playContentModal')
    playContentModal: ModalComponent;
    
    @ViewChild('showExternalLinkModal')
    showExternalLinkModal: ModalComponent;
    
    constructor(private http: Http, private router: Router) {
        this.http = http;
        this.router = router;
        
        this.ckeditorContent="This is my textarea to be replaced with CKEditor.";
        
        this.getContentDetails();
        
        this.showAudioRecordingBtn=true;
        this.showRecordDetails = true;
    }

    getContentDetails(){
         this.http
            .get('app/mock/getThemeDetails.json')
            .map(response => response.json())
            .subscribe(
             data => this.checkResponse(data),
             this.logError,
            () => console.log('Success')
         );
    }

    checkResponse(result) {
        this.themeVO=result.themeVO;
        this.themeName=this.themeVO.themeName;
        this.description=this.themeVO.description;
     }
    
    showAudioRecordOption() {
        this.showAudioRecordingBtn=true;
        this.showVideoRecordingBtn=false;
        this.showExternalLinkBtn=false;
        this.showBrowseBtn=false;
        
         this.showRecordDetails = false;
    }
    
    showVideoRecordOption() {
        this.showAudioRecordingBtn=false;
        this.showVideoRecordingBtn=true;
        this.showExternalLinkBtn=false;
        this.showBrowseBtn=false;
        
         this.showRecordDetails = false;
    }

    //External Lnk Functionalities-Start
    showExternalLinkOption() {
       this.showAudioRecordingBtn=false;
       this.showVideoRecordingBtn=false;
       this.showExternalLinkBtn=true;
       this.showBrowseBtn=false;
        
         this.showRecordDetails = false;
    }
    
    showExternalLnikModal(){
         this.showExternalLinkModal.open();
    }
    
    addExternalLink(){
        this.showExternalLinkModal.dismiss();
        this.showRecordDetails = true; 
    }

    //Browse Option Functionalities-Start
    showBrowseOption() {
       this.showAudioRecordingBtn=false;
       this.showVideoRecordingBtn=false;
       this.showExternalLinkBtn=false;
       this.showBrowseBtn=true;
        
       this.showRecordDetails = false;
    }
    
    cancel(){
        this.router.navigate(['CreatorMyThemes']);
    }
    
    ngOnInit() {

    }
    
    saveRecord() {
        $('#recordContentModal').modal('hide');
        $('.modal-backdrop').remove();

        this.showRecordDetails = true;

        this.showUrl = false;
        this.showBrowse = false;
        this.showRecord = false;
    }
    
    //Recording Dialog
     openRecordingModal() {
         this.recordContentModal.open('lg');
       
         this.recordingUrl="https://rtc.ibotsystems.com/stream-recorder/";
     }
    
    openplayContentModal() {
        this.playContentModal.open();
    }
}