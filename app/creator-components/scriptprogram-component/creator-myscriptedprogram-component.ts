import {Component, EventEmitter, OnInit} from 'angular2/core';
import {CORE_DIRECTIVES, FORM_DIRECTIVES, NgClass, NgIf} from 'angular2/common';
import {PAGINATION_DIRECTIVES} from 'ng2-bootstrap/ng2-bootstrap';

import {NG_TABLE_DIRECTIVES} from '../../../node_modules/ng2-table/ng2-table';

import {HTTP_PROVIDERS}    from 'angular2/http';
import {Http, Response} from 'angular2/http';
import {Headers, RequestOptions} from 'angular2/http';
import {HTTP_BINDINGS} from 'angular2/http';
import 'rxjs/add/operator/map';
import {Injectable} from 'angular2/core';

import { Router,Location} from 'angular2/router';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from 'angular2/router';
import {NgTableSorting} from '../../../node_modules/ng2-table/components/table/ng-table-sorting.directive';
import {CORE_DIRECTIVES, FORM_DIRECTIVES, NgClass, NgFor} from 'angular2/common';

import { MODAL_DIRECTIVES } from 'ng2-bs3-modal/ng2-bs3-modal';

@Component({
  selector: 'ngTable, [ngTable]',
  inputs: ['rows', 'columns', 'config'],
  outputs: ['tableChanged'],
  templateUrl: '/views/content-creator/scriptedprogram/myScriptedPrograms.html',  
  directives: [NG_TABLE_DIRECTIVES, PAGINATION_DIRECTIVES, NgClass, NgIf, CORE_DIRECTIVES, FORM_DIRECTIVES,ROUTER_DIRECTIVES,NgTableSorting, NgClass,MODAL_DIRECTIVES],
  providers: [HTTP_PROVIDERS,HTTP_BINDINGS]
})
export class CreatorMyScriptedProgramsComponent implements OnInit {

  constructor(private http:Http,private router:Router,private location: Location) {
    this.http=http;
    this.router=router;
    this.location = location;
      
    this.page=1;
    this.loadManageProgramsListTable(this.page);
    this.loadAddUsersTable(this.page);
    this._tempSelectedDivIds = [];
  }
    
  loadManageProgramsListTable(page){
    let body = JSON.stringify({ name });
    let headers = new Headers();
    let options = new RequestOptions({headers: headers });
    return this.http
        .post('//'+$(location).attr('hostname')+':4000/mock/myScriptedPrograms/'+page,
           body,options)
            .map(response => response.json())
            .subscribe(
               data => this.checkResponse(data),
             this.logError,
            () => console.log('Data fetching complete')
    );
  } 
       
  checkResponse(response){
     this.rows= response.programsList;
     this.length =response.total;
  }
        
   private columns:Array<any> = [
    {title: 'Name', name: 'programName'},
    {title: 'Channels', name: 'channel'},
    {title: 'State', name: 'state'},
    {title: 'Private ?', name: 'isPrivate'}, 
    {title: 'Tag', name: 'tag'},    
    {title: 'Assistant', name: 'assistant'},
    {title: 'Author ', name: 'author'},
    {title: 'Publish Date', name: 'publishDate'},
    {title: 'Expiry Date', name: 'expiryDate'},
    {title: 'Updated On', name: 'updatedOn'},
    {title: 'Updated By', name: 'updatedBy'},
    {title: 'Action'}    
  ];
    
  public config:any = {
    paging: true,
    sorting: {columns: []},
  };

  // Outputs  (Events)
  public tableChanged:EventEmitter<any> = new EventEmitter();

   onChangeTable(config:any, page:any = config.paging) {
        console.log(page);
        this.loadManageProgramsListTable(page.page);
   }
    
   changeSort(data:any, config:any) {
       console.log(this.config.sorting.columns);
  }

    private usersListTableColumns:Array<any> = [
    {title: 'Select', name: 'UsersUUID'},
    {title: 'Name', name: 'name'},
    {title: 'Email', name: 'email'},
    {title: 'Title', name: 'title'},
    {title: 'Phone', name: 'phone'},
    {title: 'Status', name: 'status'}    
   ];
    
   loadAddUsersTable(page) {
       let body = JSON.stringify({ name });
       let headers = new Headers();
       let options = new RequestOptions({ headers: headers });
       return this.http
           .post('//' + $(location).attr('hostname') + ':4000/mock/manageUsersList/' + page,
           body, options)
           .map(response => response.json())
           .subscribe(
           data => this.checkUserResponse(data),
           this.logError,
           () => console.log('Data fetching complete')
           );
    } 
    
   checkUserResponse(response) {
       this.usersList = response.UsersList;
       this.length = response.total;
   }
    
   addNewScriptedProgram(){
       $('#createProgramScriptModal').modal('hide');
       $('.modal-backdrop').remove();
       this.router.navigate(['AddNewProgram',{'from':'scriptedProgram','contentUUID':'NA'},'PublisherIntro',{'contentUUID':'NA'}]);
       //this.router.navigate(['ShowContentBeforeAddNewProgram', {'from':'scriptedProgram'}]);
   }
    
   editScriptedProgram(){        
        this.router.navigate(['CreatorEditProgram',{'from':'scriptedProgram'},'CreatorEditContent',{'from':'scriptedProgram'}]);
   }
    
   viewProgramComments(programUUID,status){
    this.router.navigate(['/CreatorHome','ViewProgramComments', {programUUID:programUUID,status:status}]);     
  }
    
  selectChannel(event){
    $("#testDivId1").removeClass("carouselSelect");         
    this.imgContentId = event.target.id;      
    this.added=false;
    this.checkFlag = false;
      
     if($.inArray( this.imgContentId, this._tempSelectedDivIds ) != -1){         
         this._tempSelectedDivIds.splice($.inArray(this.imgContentId, this._tempSelectedDivIds),1);          
         
         if($("#"+this.imgContentId).hasClass('carouselSelect')) {      
              $("#"+this.imgContentId).removeClass("carouselSelect");              
            }else{        
                    $("#"+this.imgContentId).addClass("carouselSelect");
                $.each(this._tempSelectedDivIds, function( index, value ) {                      
                    $("#"+value).removeClass("carouselSelect");          
                });
              
                this._tempSelectedDivIds.push(this.imgContentId)               
            }
         console.log("irukku..");
         this.added=true;  
    }
       
    if (!this.added) {
      console.log('illai');
      $("#"+this.imgContentId).addClass("carouselSelect");
      $.each(this._tempSelectedDivIds, function( index, value ) {
          $("#"+value).removeClass("carouselSelect");          
      });
      this._tempSelectedDivIds.push(this.imgContentId)
    }
  }
    
  ngOnInit() {
    $("#testDivId1").addClass("carouselSelect");
  }
}