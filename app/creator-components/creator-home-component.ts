import { Component } from 'angular2/core';
import { CORE_DIRECTIVES } from 'angular2/common';
import { Router,RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from 'angular2/router';

import { MODAL_DIRECTIVES } from 'ng2-bs3-modal/ng2-bs3-modal';

/*Common-Component*/
import { ProfileManagementComponent } from '../common-components/profile-management-component';
/*Change Password*/
import { ChangePasswordComponent } from '../common-components/change-password-component';
/*merge account*/
import { MergeAccountComponent } from '../common-components/merge-account-component';
/*Role Permissions Matrix*/
import { RolePermissionsMatrixComponent } from '../common-components/rolepermission-component';
/*CC-My Programs-Component*/
import { CreatorMyProgramsComponent } from './creator-manageprograms-component';
import { ShowContentBeforeAddNewProgram } from './program-components/showContents-beforeAddnewprogram-component';
import { CreatorAddNewProgramComponent } from './program-components/addnewprogram-component';
import { CreatorEditProgramComponent } from './program-components/edit/creator-edit-program-component';
/*CC-My ScriptedPrograms-Component*/
import { CreatorMyScriptedProgramsComponent } from './scriptprogram-component/creator-myscriptedprogram-component';
/*CC-My Contents-Component*/
import { CreatorMyContentsComponent } from '../creator-components/content-components/manageprogram-contents-component';
import { CreatorAddNewContentComponent } from '../creator-components/content-components/creator-add-content-component';
import { CreatorEditContentComponent } from '../creator-components/content-components/creator-edit-content-component';
/*CC-My Themes-Component*/
import { CreatorMyThemesComponent } from '../creator-components/theme-components/creator-mythemes-component';
import { CreatorAddNewThemeComponent } from '../creator-components/theme-components/creator-add-theme-component';
import { CreatorEditThemeComponent } from '../creator-components/theme-components/creator-edit-theme-component';

import { ProgramstoRecordComponent } from './programstoRecord-component';

import { ProgramsforApprovalComponent } from './programsforApproval-component';
/*CC-My Dashboard-Component*/
import { CreatorDashboardComponent } from './dashboard-component/creator-dashboard-component';
/*CM-My Dashboard-Component*/
import { ManagerDashboardComponent } from './dashboard-component/manager-dashboard-component';
/*View-My Dashboard-Component*/
import { ViewerDashboardComponent } from './dashboard-component/viewer-dashboard-component';
/*CM -All Programs*/
import { ManagerProgramManagementComponent } from '../manager-components/program/manager-program-management-component';
import { ManagerEditProgramComponent } from '../manager-components/program/edit/manager-edit-program-component';
/*CM-AllContents-Component*/
import { ManagerContentManagementComponent } from '../manager-components/content/manager-content-management-component';
import { ManagerEditContentComponent } from '../manager-components/content/manager-edit-content-component';
/*CM-AllThemes-Component*/
import { ManagerThemeManagementComponent } from '../manager-components/theme/manager-theme-management-component';
import { ManagerEditThemeComponent } from '../manager-components/theme/manager-edit-theme-component';
 /*CM-AllSurveys-Component*/
import { ManagerSurveyManagementComponent } from '../manager-components/survey/manager-survey-management-component';
/*Viewer-Home*/
import { ViewerInboxComponent } from '../viewer-components/home/viewer-inbox-component';
import { ViewProgramCommentsComponent } from '../viewer-components/home/view-programcomments-component';
/*SA-Dashboard-Component*/
import { SuperAdminDashboardComponent } from './dashboard-component/superAdmin-dashboard-component';
/*SA-Customer-Component*/
import { SuperAdminManageCustomersComponent } from '../superAdmin/customer/superadmin-manage-customers-component';
import { SuperAdminAddNewCustomerComponent } from '../superAdmin/customer/superadmin-addnew-customer-component';
import { SuperAdminEditCustomerComponent } from '../superAdmin/customer/superadmin-edit-customer-component';
/*SA-User-Component*/
import { SuperAdminManageUsersComponent } from '../superAdmin/user/superadmin-manageUsers-component';
import { SuperAdminAddUsersComponent } from '../superAdmin/user/superadmin-addUsers-component';
import { SuperAdminEditUserComponent } from '../superAdmin/user/superadmin-edit-user-component';
import { SuperAdminBulkUploadUsersComponent } from '../superAdmin/user/superadmin-bulkupload-users-component';
import { SuperAdminInviteUserComponent } from '../superAdmin/user/superadmin-inviteusers-component';
/*Admin-My dashboard-Component*/
import { AdminDashboardComponent } from './dashboard-component/admin-dashboard-component';
import { CustomerAdminManageCustomersComponent } from '../customeradmin-components/customer/customeradmin-managecustomers-component';
import { CustomerAdminAddUsersComponent } from '../customeradmin-components/user/customeradmin-adduser-component';
/*Admin ManageChannels*/
import { CustomerAdminManageChannelsComponent } from '../customeradmin-components/channels/customeradmin-managechannels-component';
import { CustomerAdminAddChannelsComponent } from '../customeradmin-components/channels/customeradmin-addchannels-component';
import { CustomerAdminEditChannelsComponent } from '../customeradmin-components/channels/edit/customeradmin-editchannels-component';
/*Message-Components*/
import { MessagesListComponent } from '../messages-components/messageslist-component';
import { ComposeMessageComponent } from '../messages-components/compose-message-component';
import { ViewMessageComponent } from '../messages-components/viewmessage-component';
import { EditMessageComponent } from '../messages-components/editmessage-component';
import { ContactsComponent } from '../messages-components/contacts-component';
/*Calls*/
import { ManageCallsComponent } from '../calls/manageCalls-component';
import { MakeCallsComponent } from '../calls/make-calls-component';
import { ChooseAudioCallUsersComponent } from '../calls/choose-audioCallsUsers-component';

@Component({ 
  templateUrl: 'views/common/creatorHomePage.html',
  directives: [ROUTER_DIRECTIVES,MODAL_DIRECTIVES]
})
  
@RouteConfig([
   /*Common-Profile Management*/
  { path: '/profileManagement/...', name: 'ProfileManagement', component: ProfileManagementComponent},
  /*Change Password*/
  {path: '/changePassword', name: 'ChangePassword', component: ChangePasswordComponent},
    /*Merge Account*/
  {path: '/mergeAccount/:function', name: 'MergeAccount', component: MergeAccountComponent},
  {path: '/rolePermissionMatrix', name: 'RolePermissionsMatrix', component: RolePermissionsMatrixComponent},
  /*CC-My Programs*/
  { path: '/myPrograms', name: 'CreatorMyPrograms', component: CreatorMyProgramsComponent},
  { path: '/showContentBeforeAddNewProgram', name: 'ShowContentBeforeAddNewProgram', component: ShowContentBeforeAddNewProgram},
  { path: '/addNewProgram/:contentUUID/...', name: 'AddNewProgram', component: CreatorAddNewProgramComponent},
  { path: '/creatoredirprogram/...', name: 'CreatorEditProgram', component: CreatorEditProgramComponent},
  /*CC-My Scripted Programs*/
  { path: '/myScriptedPrograms', name: 'MyScriptedPrograms', component: CreatorMyScriptedProgramsComponent},
  /*CC-My Contents*/
  { path: '/myContents', name: 'CreatorMyContents', component: CreatorMyContentsComponent},
  { path: '/addNewContent', name: 'CreatorAddNewContent', component: CreatorAddNewContentComponent},
  { path: '/editContent/:contentUUID', name: 'CreatorEditContent', component: CreatorEditContentComponent},
  /*CC-My Themes*/
  { path: '/myThemes', name: 'CreatorMyThemes', component: CreatorMyThemesComponent},
  { path: '/addNewTheme', name: 'CreatorAddNewTheme', component: CreatorAddNewThemeComponent},
  { path: '/editTheme/:themeUUID', name: 'CreatorEditTheme', component: CreatorEditThemeComponent},
  /*CC-My Scripted Programs*/
  { path: '/programstoRecord', name: 'ProgramstoRecord', component: ProgramstoRecordComponent},
  
  { path: '/programsforApproval', name: 'ProgramsforApproval', component: ProgramsforApprovalComponent},
  /*CC-My Scripted Programs*/
  { path: '/myDashboardCharts', name: 'MyDashboardCharts', component: CreatorDashboardComponent},
  /*CM-My Scripted Programs*/
  { path: '/manageDashboardCharts', name: 'ManageDashboardCharts', component: ManagerDashboardComponent},
  /*CM-My Scripted Programs*/
  { path: '/viewerDashboardCharts', name: 'ViewerDashboardCharts', component: ViewerDashboardComponent},
  /*CM-My Scripted Programs*/
  { path: '/adminDashboardCharts', name: 'AdminDashboardCharts', component: AdminDashboardComponent},
  /*CM-My Scripted Programs*/
  { path: '/superAdminDashboardCharts', name: 'SuperAdminDashboardCharts', component: SuperAdminDashboardComponent},  
  /*CM-AllProgrames*/
  { path: '/allPrograms', name: 'ManagerAllProgramsManagement', component: ManagerProgramManagementComponent},
  { path: '/managereditprogram/...', name: 'ManagerEditProgram', component: ManagerEditProgramComponent},
  /*CM-AllContents*/
  { path: '/contentManagement', name: 'ManagerContentManagement', component: ManagerContentManagementComponent},
  { path: '/updateContent/:contentUUID', name: 'ManagerEditContent', component: ManagerEditContentComponent},
  /*CM-AllThemes*/
  { path: '/themeManagement', name: 'ManagerThemeManagement', component: ManagerThemeManagementComponent},
  { path: '/updateTheme/:themeUUID', name: 'ManagerEditTheme', component: ManagerEditThemeComponent},
  /*CM-AllSurveys*/
  { path: '/surveyLibrary', name: 'ManagerSurveyManagement', component: ManagerSurveyManagementComponent},
  /*Viewer-Home*/
  { path: '/inbox/...', name: 'Inbox', component: ViewerInboxComponent},
  { path: '/viewProgramComments/:programUUID/:status', name: 'ViewProgramComments', component: ViewProgramCommentsComponent },
  /*SA-Customer*/
  { path: '/manageCustomers', name: 'SuperAdminManageCustomers', component: SuperAdminManageCustomersComponent},
  { path: '/addNewCustomer', name: 'SuperAdminAddNewCustomer', component: SuperAdminAddNewCustomerComponent},
  { path: '/editCustomer/:customerUUID/:status', name: 'SuperAdminEditCustomer', component: SuperAdminEditCustomerComponent},
  /*SA-User*/
  { path: '/manageUsers/:roleName', name: 'SuperAdminManageUsers', component: SuperAdminManageUsersComponent},
  { path: '/addNewUser', name: 'SuperAdminAddUsers', component: SuperAdminAddUsersComponent},
  { path: '/editUser/:userUUID/:roleName', name: 'SuperAdminEditUser', component: SuperAdminEditUserComponent},
  { path: '/bulkUpload/:roleName', name: 'SuperAdminBulkUploadUsers', component: SuperAdminBulkUploadUsersComponent},
  { path: '/inviteUser/:roleName', name: 'SuperAdminInviteUser', component: SuperAdminInviteUserComponent},
  /*CA-Customer*/
  { path: '/manageCustomersList', name: 'CustomerAdminManageCustomers', component: CustomerAdminManageCustomersComponent},
  { path: '/addUser', name: 'CustomerAdminAddUsers', component: CustomerAdminAddUsersComponent},
  { path: '/manageChannels', name: 'CustomerAdminManageChannels', component: CustomerAdminManageChannelsComponent},
  { path: '/addChannel/...', name: 'CustomerAdminAddChannels', component: CustomerAdminAddChannelsComponent} , 
  { path: '/editChannel/:channelUUID/:isExternalFlag/...', name: 'CustomerAdminEditChannels', component: CustomerAdminEditChannelsComponent} ,
  /*Message Management*/
  { path: '/messagesList', name: 'MessagesList', component: MessagesListComponent},
  { path: '/composeMessage', name: 'ComposeMessage', component: ComposeMessageComponent} ,
  { path: '/viewMessage/:messageUUID', name: 'ViewMessage', component: ViewMessageComponent} ,
  { path: '/editMessage/:messageUUID', name: 'EditMessage', component: EditMessageComponent} ,
  { path: '/contacts', name: 'Contacts', component: ContactsComponent} ,
  /*Calls*/
  { path: '/manageCalls', name: 'ManageCalls', component: ManageCallsComponent},
  { path: '/makeCalls', name: 'MakeCalls', component: MakeCallsComponent},
  { path: '/chooseAudioCallUsers', name: 'ChooseAudioCallUsers', component: ChooseAudioCallUsersComponent}
])
    
export class CreatorHomeComponent {

    constructor(private router:Router) {
        this.router=router;
        this.showSubMenusOfPublish();
        
        this.showSubMenusPublish=true; 
        this.showSubMenusProduce=false;
        this.showSubMenusSetup=false;
        this.showViewersSubMenusOfHomeFlag=false;
        this.selectHome = false;
        this.selectPublish = true;
        this.selectProduce = false;
        this.selectSetup = false;
        this.selectSA = false;  
    }
    
    showSubMenusOfPublish(){
        this.showSubMenusPublish=true; 
        this.showSubMenusProduce=false;
        this.showViewersSubMenusOfHomeFlag=false;
        this.showSubMenusSetup = false;
        this.showSubMenusSuperAdmin = false;
        this.router.navigate(['CreatorHome','CreatorMyPrograms']);   
    }
    
    showSubMenusOfProduce(){
        this.showSubMenusPublish=false; 
        this.showSubMenusProduce=true;  
        this.showViewersSubMenusOfHomeFlag=false;
        this.showSubMenusSetup = false;
        this.showSubMenusSuperAdmin = false;
        this.router.navigate(['CreatorHome','ManagerAllProgramsManagement']);     
    }

    showViewersSubMenusOfHome(){
        this.showViewersSubMenusOfHomeFlag=true;
        this.showSubMenusPublish=false; 
        this.showSubMenusProduce=false;
        this.showSubMenusSetup = false; 
        this.showSubMenusSuperAdmin = false;
        this.router.navigate(['CreatorHome','Inbox','NewPrograms']);     
        
    }
    
    showSubMenusOfSetup(){
        this.showViewersSubMenusOfHomeFlag=false;
        this.showSubMenusPublish=false; 
        this.showSubMenusProduce=false;
        this.showSubMenusSetup = true;
        this.showSubMenusSuperAdmin = false; 
        this.router.navigate(['CreatorHome','AdminDashboardCharts']);     
        
    }
    
    showSubMenusOfSuperAdmin(){
        this.showViewersSubMenusOfHomeFlag=false;
        this.showSubMenusPublish=false; 
        this.showSubMenusProduce=false;
        this.showSubMenusSetup = false;
        this.showSubMenusSuperAdmin = true;
        this.router.navigate(['CreatorHome','SuperAdminDashboardCharts']);     
        
    }
    
    loadMergeAcc(){
       this.router.navigate(['CreatorHome','MergeAccount',{'function':'merge'}]);
    }
    
    logout(){
        localStorage.removeItem('currentAccessToken');
        this.router.navigateByUrl('/login'); 
    }
    
    selectedPublish(){
        this.selectHome = false;
        this.selectPublish = true;
        this.selectProduce = false;
        this.selectSetup = false;
        this.selectSA = false;  
    }
    selectedHome(){
        this.selectHome = true;
        this.selectPublish = false;
        this.selectProduce = false;
        this.selectSetup = false;
        this.selectSA = false;  
    }
    selectedProduce(){
        this.selectHome = false;
        this.selectPublish = false;
        this.selectProduce = true;
        this.selectSetup = false;
        this.selectSA = false;  
    }
    selectedSetup(){
        this.selectHome = false;
        this.selectPublish = false;
        this.selectProduce = false;
        this.selectSetup = true;
        this.selectSA = false;  
    }
    selectedSA(){
        this.selectHome = false;
        this.selectPublish = false;
        this.selectProduce = false;
        this.selectSetup = false;
        this.selectSA = true;  
    }
}
