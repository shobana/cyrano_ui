System.register(["angular2/core", "./autocomplete-list"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, autocomplete_list_1;
    var AutocompleteDirective;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (autocomplete_list_1_1) {
                autocomplete_list_1 = autocomplete_list_1_1;
            }],
        execute: function() {
            let AutocompleteDirective = class AutocompleteDirective {
                constructor(el, dcl) {
                    this.el = el;
                    this.dcl = dcl;
                    this.selected = new core_1.EventEmitter();
                    this.term = "";
                    this.listCmp = undefined;
                    this.refreshTimer = undefined;
                    this.searchInProgress = false;
                    this.searchRequired = false;
                }
                onKey(event) {
                    if (!this.refreshTimer) {
                        this.refreshTimer = setTimeout(() => {
                            if (!this.searchInProgress) {
                                this.doSearch();
                            }
                            else {
                                this.searchRequired = true;
                            }
                        }, 200);
                    }
                    this.term = event.target.value;
                    if (this.term === "" && this.listCmp) {
                        this.removeList();
                    }
                }
                doSearch() {
                    this.refreshTimer = undefined;
                    if (this.search && this.term !== "") {
                        this.searchInProgress = true;
                        this.search(this.term)
                            .then((res) => {
                            this.searchInProgress = false;
                            if (this.searchRequired) {
                                this.searchRequired = false;
                                this.doSearch();
                            }
                            else {
                                this.diplayList(res);
                            }
                        })
                            .catch(err => {
                            console.log("search error:", err);
                            this.removeList();
                        });
                    }
                }
                diplayList(list) {
                    if (!this.listCmp) {
                        this.dcl.loadNextToLocation(autocomplete_list_1.AutocompleteList, this.el)
                            .then(cmp => {
                            this.listCmp = cmp;
                            this.updateList(list);
                            (this.listCmp.instance).selected
                                .subscribe(selectedItem => {
                                this.selected.emit(selectedItem);
                            });
                        });
                    }
                    else {
                        this.updateList(list);
                    }
                }
                updateList(list) {
                    if (this.listCmp) {
                        (this.listCmp.instance).list = list;
                    }
                }
                removeList() {
                    this.searchInProgress = false;
                    this.searchRequired = false;
                    if (this.listCmp) {
                        this.listCmp.dispose();
                        this.listCmp = undefined;
                    }
                }
            };
            __decorate([
                core_1.Input("ng2-autocomplete"), 
                __metadata('design:type', Function)
            ], AutocompleteDirective.prototype, "search", void 0);
            __decorate([
                core_1.Output("ng2AutocompleteOnSelect"), 
                __metadata('design:type', Object)
            ], AutocompleteDirective.prototype, "selected", void 0);
            AutocompleteDirective = __decorate([
                core_1.Directive({
                    selector: "[ng2-autocomplete]",
                    host: {
                        "(keyup)": "onKey($event)"
                    }
                }), 
                __metadata('design:paramtypes', [core_1.ElementRef, core_1.DynamicComponentLoader])
            ], AutocompleteDirective);
            exports_1("AutocompleteDirective", AutocompleteDirective);
        }
    }
});
//# sourceMappingURL=autocomplete.js.map