System.register(['angular2/core', 'angular2/common', './sanitize', './autocomplete-container', './options.class'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, core_2, common_1, sanitize_1, autocomplete_container_1, options_class_1;
    var Autocomplete;
    function setProperty(renderer, elementRef, propName, propValue) {
        renderer.setElementProperty(elementRef.nativeElement, propName, propValue);
    }
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
                core_2 = core_1_1;
            },
            function (common_1_1) {
                common_1 = common_1_1;
            },
            function (sanitize_1_1) {
                sanitize_1 = sanitize_1_1;
            },
            function (autocomplete_container_1_1) {
                autocomplete_container_1 = autocomplete_container_1_1;
            },
            function (options_class_1_1) {
                options_class_1 = options_class_1_1;
            }],
        execute: function() {
            let Autocomplete = class Autocomplete {
                constructor(cd, element, renderer, loader) {
                    this.cd = cd;
                    this.element = element;
                    this.renderer = renderer;
                    this.loader = loader;
                    this.autocompleteLoading = new core_2.EventEmitter();
                    this.autocompleteNoResults = new core_2.EventEmitter();
                    this.autocompleteOnSelect = new core_2.EventEmitter();
                    this.autocompleteAsync = null;
                    this.autocompleteLatinize = true;
                    this.autocompleteSingleWords = true;
                    this.autocompleteWordDelimiters = ' ';
                    this.autocompletePhraseDelimiters = '\'"';
                    this._matches = [];
                    this.placement = 'bottom-left';
                }
                get matches() {
                    return this._matches;
                }
                debounce(func, wait) {
                    let timeout;
                    let args;
                    let timestamp;
                    let waitOriginal = wait;
                    return function () {
                        // save details of latest call
                        args = [].slice.call(arguments, 0);
                        timestamp = Date.now();
                        // this trick is about implementing of 'autocompleteWaitMs'
                        // in this case we have adaptive 'wait' parameter
                        // we should use standard 'wait'('waitOriginal') in case of
                        // popup is opened, otherwise - 'autocompleteWaitMs' parameter
                        wait = this.container ? waitOriginal : this.autocompleteWaitMs;
                        // this is where the magic happens
                        let later = function () {
                            // how long ago was the last call
                            let last = Date.now() - timestamp;
                            // if the latest call was less that the wait period ago
                            // then we reset the timeout to wait for the difference
                            if (last < wait) {
                                timeout = setTimeout(later, wait - last);
                            }
                            else {
                                timeout = null;
                                func.apply(this, args);
                            }
                        };
                        // we only need to set the timer now if one isn't already running
                        if (!timeout) {
                            timeout = setTimeout(later, wait);
                        }
                    };
                }
                processMatches() {
                    this._matches = [];
                    if (this.cd.model.toString().length >= this.autocompleteMinLength) {
                        // If singleWords, break model here to not be doing extra work on each iteration
                        let normalizedQuery = (this.autocompleteLatinize ? sanitize_1.AutocompleteUtils.latinize(this.cd.model) : this.cd.model).toString().toLowerCase();
                        normalizedQuery = this.autocompleteSingleWords ? sanitize_1.AutocompleteUtils.tokenize(normalizedQuery, this.autocompleteWordDelimiters, this.autocompletePhraseDelimiters) : normalizedQuery;
                        for (let i = 0; i < this.autocomplete.length; i++) {
                            let match;
                            if (typeof this.autocomplete[i] === 'object' &&
                                this.autocomplete[i][this.autocompleteOptionField]) {
                                match = this.autocompleteLatinize ? sanitize_1.AutocompleteUtils.latinize(this.autocomplete[i][this.autocompleteOptionField].toString()) : this.autocomplete[i][this.autocompleteOptionField].toString();
                            }
                            if (typeof this.autocomplete[i] === 'string') {
                                match = this.autocompleteLatinize ? sanitize_1.AutocompleteUtils.latinize(this.autocomplete[i].toString()) : this.autocomplete[i].toString();
                            }
                            if (!match) {
                                console.log('Invalid match type', typeof this.autocomplete[i], this.autocompleteOptionField);
                                continue;
                            }
                            if (this.testMatch(match.toLowerCase(), normalizedQuery)) {
                                this._matches.push(this.autocomplete[i]);
                                if (this._matches.length > this.autocompleteOptionsLimit - 1) {
                                    break;
                                }
                            }
                        }
                    }
                }
                testMatch(match, test) {
                    let spaceLength;
                    if (typeof test === 'object') {
                        spaceLength = test.length;
                        for (let i = 0; i < spaceLength; i += 1) {
                            if (test[i].length > 0 && match.indexOf(test[i]) < 0) {
                                return false;
                            }
                        }
                        return true;
                    }
                    else {
                        return match.indexOf(test) >= 0;
                    }
                }
                finalizeAsyncCall() {
                    this.autocompleteLoading.emit(false);
                    this.autocompleteNoResults.emit(this.cd.model.toString().length >=
                        this.autocompleteMinLength && this.matches.length <= 0);
                    if (this.cd.model.toString().length <= 0 || this._matches.length <= 0) {
                        this.hide();
                        return;
                    }
                    if (this.container && this._matches.length > 0) {
                        // This improves the speedas it won't have to be done for each list item
                        let normalizedQuery = (this.autocompleteLatinize ? sanitize_1.AutocompleteUtils.latinize(this.cd.model) : this.cd.model).toString().toLowerCase();
                        this.container.query = this.autocompleteSingleWords ? sanitize_1.AutocompleteUtils.tokenize(normalizedQuery, this.autocompleteWordDelimiters, this.autocompletePhraseDelimiters) : normalizedQuery;
                        this.container.matches = this._matches;
                    }
                    if (!this.container && this._matches.length > 0) {
                        this.show(this._matches);
                    }
                }
                ngOnInit() {
                    this.autocompleteOptionsLimit = this.autocompleteOptionsLimit || 20;
                    this.autocompleteMinLength = this.autocompleteMinLength || 1;
                    this.autocompleteWaitMs = this.autocompleteWaitMs || 0;
                    // async should be false in case of array
                    if (this.autocompleteAsync === null && typeof this.autocomplete !== 'function') {
                        this.autocompleteAsync = false;
                    }
                    // async should be true for any case of function
                    if (typeof this.autocomplete === 'function') {
                        this.autocompleteAsync = true;
                    }
                    if (this.autocompleteAsync === true) {
                        this.debouncer = this.debounce(() => {
                            if (typeof this.autocomplete === 'function') {
                                this.autocomplete().then((matches) => {
                                    this._matches = [];
                                    if (this.cd.model.toString().length >= this.autocompleteMinLength) {
                                        for (let i = 0; i < matches.length; i++) {
                                            this._matches.push(matches[i]);
                                            if (this._matches.length > this.autocompleteOptionsLimit - 1) {
                                                break;
                                            }
                                        }
                                    }
                                    this.finalizeAsyncCall();
                                });
                            }
                            // source is array
                            if (typeof this.autocomplete === 'object' && this.autocomplete.length) {
                                this.processMatches();
                                this.finalizeAsyncCall();
                            }
                        }, 100);
                    }
                }
                onChange(e) {
                    if (this.container) {
                        // esc
                        if (e.keyCode === 27) {
                            this.hide();
                            return;
                        }
                        // up
                        if (e.keyCode === 38) {
                            this.container.prevActiveMatch();
                            return;
                        }
                        // down
                        if (e.keyCode === 40) {
                            this.container.nextActiveMatch();
                            return;
                        }
                        // enter
                        if (e.keyCode === 13) {
                            this.container.selectActiveMatch();
                            return;
                        }
                    }
                    this.autocompleteLoading.emit(true);
                    if (this.autocompleteAsync === true) {
                        this.debouncer();
                    }
                    if (this.autocompleteAsync === false) {
                        this.processMatches();
                        this.finalizeAsyncCall();
                    }
                }
                changeModel(value) {
                    let valueStr = ((typeof value === 'object' && this.autocompleteOptionField) ? value[this.autocompleteOptionField] : value).toString();
                    this.cd.viewToModelUpdate(valueStr);
                    setProperty(this.renderer, this.element, 'value', valueStr);
                    this.hide();
                }
                show(matches) {
                    let options = new options_class_1.AutocompleteOptions({
                        placement: this.placement,
                        animation: false
                    });
                    let binding = core_2.Injector.resolve([
                        new core_2.Provider(options_class_1.AutocompleteOptions, { useValue: options })
                    ]);
                    this.popup = this.loader
                        .loadNextToLocation(autocomplete_container_1.AutocompleteContainer, this.element, binding)
                        .then((componentRef) => {
                        componentRef.instance.position(this.element);
                        this.container = componentRef.instance;
                        this.container.parent = this;
                        // This improves the speedas it won't have to be done for each list item
                        let normalizedQuery = (this.autocompleteLatinize ? sanitize_1.AutocompleteUtils.latinize(this.cd.model) : this.cd.model).toString().toLowerCase();
                        this.container.query = this.autocompleteSingleWords ? sanitize_1.AutocompleteUtils.tokenize(normalizedQuery, this.autocompleteWordDelimiters, this.autocompletePhraseDelimiters) : normalizedQuery;
                        this.container.matches = matches;
                        this.container.field = this.autocompleteOptionField;
                        this.element.nativeElement.focus();
                        return componentRef;
                    });
                }
                hide() {
                    if (this.container) {
                        this.popup.then((componentRef) => {
                            componentRef.dispose();
                            this.container = null;
                            return componentRef;
                        });
                    }
                }
            };
            __decorate([
                core_2.Output(), 
                __metadata('design:type', core_2.EventEmitter)
            ], Autocomplete.prototype, "autocompleteLoading", void 0);
            __decorate([
                core_2.Output(), 
                __metadata('design:type', core_2.EventEmitter)
            ], Autocomplete.prototype, "autocompleteNoResults", void 0);
            __decorate([
                core_2.Output(), 
                __metadata('design:type', core_2.EventEmitter)
            ], Autocomplete.prototype, "autocompleteOnSelect", void 0);
            __decorate([
                core_2.Input(), 
                __metadata('design:type', Object)
            ], Autocomplete.prototype, "autocomplete", void 0);
            __decorate([
                core_2.Input(), 
                __metadata('design:type', Number)
            ], Autocomplete.prototype, "autocompleteMinLength", void 0);
            __decorate([
                core_2.Input(), 
                __metadata('design:type', Number)
            ], Autocomplete.prototype, "autocompleteWaitMs", void 0);
            __decorate([
                core_2.Input(), 
                __metadata('design:type', Number)
            ], Autocomplete.prototype, "autocompleteOptionsLimit", void 0);
            __decorate([
                core_2.Input(), 
                __metadata('design:type', String)
            ], Autocomplete.prototype, "autocompleteOptionField", void 0);
            __decorate([
                core_2.Input(), 
                __metadata('design:type', Boolean)
            ], Autocomplete.prototype, "autocompleteAsync", void 0);
            __decorate([
                core_2.Input(), 
                __metadata('design:type', Boolean)
            ], Autocomplete.prototype, "autocompleteLatinize", void 0);
            __decorate([
                core_2.Input(), 
                __metadata('design:type', Boolean)
            ], Autocomplete.prototype, "autocompleteSingleWords", void 0);
            __decorate([
                core_2.Input(), 
                __metadata('design:type', String)
            ], Autocomplete.prototype, "autocompleteWordDelimiters", void 0);
            __decorate([
                core_2.Input(), 
                __metadata('design:type', String)
            ], Autocomplete.prototype, "autocompletePhraseDelimiters", void 0);
            __decorate([
                core_2.HostListener('keyup', ['$event']), 
                __metadata('design:type', Function), 
                __metadata('design:paramtypes', [KeyboardEvent]), 
                __metadata('design:returntype', void 0)
            ], Autocomplete.prototype, "onChange", null);
            Autocomplete = __decorate([
                core_2.Directive({
                    selector: 'autocomplete[ngModel], [ngModel][autocomplete]'
                }), 
                __metadata('design:paramtypes', [common_1.NgModel, core_1.ElementRef, core_2.Renderer, core_2.DynamicComponentLoader])
            ], Autocomplete);
            exports_1("Autocomplete", Autocomplete);
        }
    }
});
//# sourceMappingURL=autocomplete.component.js.map