System.register(['angular2/core', 'angular2/common', './options.class', './position'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, common_1, options_class_1, position_1;
    var AutocompleteContainer;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (common_1_1) {
                common_1 = common_1_1;
            },
            function (options_class_1_1) {
                options_class_1 = options_class_1_1;
            },
            function (position_1_1) {
                position_1 = position_1_1;
            }],
        execute: function() {
            let AutocompleteContainer = class AutocompleteContainer {
                constructor(element, options) {
                    this.element = element;
                    this._matches = [];
                    Object.assign(this, options);
                }
                get matches() {
                    return this._matches;
                }
                set matches(value) {
                    this._matches = value;
                    if (this._matches.length > 0) {
                        this._active = this._matches[0];
                    }
                }
                set field(value) {
                    this._field = value;
                }
                position(hostEl) {
                    this.display = 'block';
                    this.top = '0px';
                    this.left = '0px';
                    let p = position_1.positionService
                        .positionElements(hostEl.nativeElement, this.element.nativeElement.children[0], this.placement, false);
                    this.top = p.top + 'px';
                    this.left = p.left + 'px';
                }
                selectActiveMatch() {
                    this.selectMatch(this._active);
                }
                prevActiveMatch() {
                    let index = this.matches.indexOf(this._active);
                    this._active = this.matches[index - 1 < 0 ? this.matches.length - 1 : index - 1];
                }
                nextActiveMatch() {
                    let index = this.matches.indexOf(this._active);
                    this._active = this.matches[index + 1 > this.matches.length - 1 ? 0 : index + 1];
                }
                selectActive(value) {
                    this._active = value;
                }
                isActive(value) {
                    return this._active === value;
                }
                selectMatch(value, e = null) {
                    if (e) {
                        e.stopPropagation();
                        e.preventDefault();
                    }
                    this.parent.changeModel(value);
                    this.parent.autocompleteOnSelect.emit({
                        item: value
                    });
                    return false;
                }
                hightlight(item, query) {
                    let itemStr = (typeof item === 'object' && this._field ? item[this._field] : item).toString();
                    //let itemStrHelper:string = (this.parent.autocompleteLatinize ? AutocompleteUtils.latinize(itemStr) : itemStr).toLowerCase();
                    let itemStrHelper = itemStr.toLowerCase();
                    let startIdx;
                    let tokenLen;
                    // Replaces the capture string with the same string inside of a "strong" tag
                    if (typeof query === 'object') {
                        let queryLen = query.length;
                        for (let i = 0; i < queryLen; i += 1) {
                            // query[i] is already latinized and lower case
                            startIdx = itemStrHelper.indexOf(query[i]);
                            tokenLen = query[i].length;
                            if (startIdx >= 0 && tokenLen > 0) {
                                itemStr = itemStr.substring(0, startIdx) + '<strong>' + itemStr.substring(startIdx, startIdx + tokenLen) + '</strong>' + itemStr.substring(startIdx + tokenLen);
                                itemStrHelper = itemStrHelper.substring(0, startIdx) + '        ' + ' '.repeat(tokenLen) + '         ' + itemStrHelper.substring(startIdx + tokenLen);
                            }
                        }
                    }
                    else if (query) {
                        // query is already latinized and lower case
                        startIdx = itemStrHelper.indexOf(query);
                        tokenLen = query.length;
                        if (startIdx >= 0 && tokenLen > 0) {
                            itemStr = itemStr.substring(0, startIdx) + '<strong>' + itemStr.substring(startIdx, startIdx + tokenLen) + '</strong>' + itemStr.substring(startIdx + tokenLen);
                        }
                    }
                    return itemStr;
                }
            };
            AutocompleteContainer = __decorate([
                core_1.Component({
                    selector: 'autocomplete-container',
                    directives: [common_1.CORE_DIRECTIVES],
                    template: `
                  <ul class="dropdown-menu"
                      [ngStyle]="{top: top, left: left, display: display}"
                      style="display: block">
                    <li *ngFor="#match of matches"
                        [class.active]="isActive(match)"
                        (mouseenter)="selectActive(match)">
                        <a href="#" (click)="selectMatch(match, $event)" tabindex="-1" [innerHtml]="hightlight(match, query)"></a>
                    </li>
                  </ul>
              `,
                    /*TEMPLATE[Ng2BootstrapConfig.theme],*/
                    encapsulation: core_1.ViewEncapsulation.None
                }), 
                __metadata('design:paramtypes', [core_1.ElementRef, options_class_1.AutocompleteOptions])
            ], AutocompleteContainer);
            exports_1("AutocompleteContainer", AutocompleteContainer);
        }
    }
});
//# sourceMappingURL=autocomplete-container.js.map