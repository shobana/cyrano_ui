System.register(['./latin-map'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var latin_map_1;
    var AutocompleteUtils;
    return {
        setters:[
            function (latin_map_1_1) {
                latin_map_1 = latin_map_1_1;
            }],
        execute: function() {
            class AutocompleteUtils {
                static latinize(str) {
                    return str.replace(/[^A-Za-z0-9\[\] ]/g, function (a) {
                        return AutocompleteUtils.latinMap[a] || a;
                    });
                }
                static escapeRegexp(queryToEscape) {
                    // Regex: capture the whole query string and replace it with the string that will be used to match
                    // the results, for example if the capture is 'a' the result will be \a
                    return queryToEscape.replace(/([.?*+^$[\]\\(){}|-])/g, '\\$1');
                }
                static tokenize(str, wordRegexDelimiters = ' ', phraseRegexDelimiters = '') {
                    let regexStr = '(?:[' + phraseRegexDelimiters + '])([^' + phraseRegexDelimiters + ']+)(?:[' + phraseRegexDelimiters + '])|([^' + wordRegexDelimiters + ']+)';
                    let preTokenized = str.split(new RegExp(regexStr, 'g'));
                    let result = [];
                    let preTokenizedLength = preTokenized.length;
                    let token;
                    let replacePhraseDelimiters = new RegExp('[' + phraseRegexDelimiters + ']+', 'g');
                    for (let i = 0; i < preTokenizedLength; i += 1) {
                        token = preTokenized[i];
                        if (token && token.length && token !== wordRegexDelimiters) {
                            result.push(token.replace(replacePhraseDelimiters, ''));
                        }
                    }
                    return result;
                }
            }
            AutocompleteUtils.latinMap = latin_map_1.latinMap;
            exports_1("AutocompleteUtils", AutocompleteUtils);
        }
    }
});
//# sourceMappingURL=sanitize.js.map