/**
 * Created by Tareq Boulakjar. from angulartypescript.com
 */
System.register(['angular2/core', 'angular2/common', './autocomplete-container', './autocomplete.component'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, common_1, autocomplete_container_1, autocomplete_component_1;
    var AUTOCOMPLETE_DIRECTIVES, Angular2Autocomplete;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (common_1_1) {
                common_1 = common_1_1;
            },
            function (autocomplete_container_1_1) {
                autocomplete_container_1 = autocomplete_container_1_1;
            },
            function (autocomplete_component_1_1) {
                autocomplete_component_1 = autocomplete_component_1_1;
            }],
        execute: function() {
            exports_1("AUTOCOMPLETE_DIRECTIVES", AUTOCOMPLETE_DIRECTIVES = [autocomplete_component_1.Autocomplete, autocomplete_container_1.AutocompleteContainer]);
            /*Angular 2 Autocomplete Example*/
            let Angular2Autocomplete = class Angular2Autocomplete {
                constructor() {
                    this.selectedCar = '';
                    this.asyncSelectedCar = '';
                    this.autocompleteLoading = false;
                    this.autocompleteNoResults = false;
                    this.carsExample1 = ['BMW', 'Audi', 'Mercedes', 'Porsche', 'Volkswagen', 'Opel', 'Maserati', 'Volkswagen', 'BMW Serie 1', 'BMW Serie 2'];
                    this.carsExample2 = [
                        { id: 1, name: 'BMW' },
                        { id: 2, name: 'Audi' },
                        { id: 3, name: 'Mercedes' },
                        { id: 4, name: 'Porsche' },
                        { id: 5, name: 'Volkswagen' },
                        { id: 6, name: 'Opel' },
                        { id: 7, name: 'Maserati' },
                        { id: 8, name: 'Volkswagen' },
                        { id: 9, name: 'BMW Serie 1' },
                        { id: 10, name: 'BMW Serie 2' },
                    ];
                }
                getCurrentContext() {
                    return this;
                }
                getAsyncData(context) {
                    if (this._previousContext === context) {
                        return this._cachedResult;
                    }
                    this._previousContext = context;
                    let f = function () {
                        let p = new Promise((resolve) => {
                            setTimeout(() => {
                                let query = new RegExp(context.asyncSelectedCar, 'ig');
                                return resolve(context.carsExample1.filter((state) => {
                                    return query.test(state);
                                }));
                            }, 500);
                        });
                        return p;
                    };
                    this._cachedResult = f;
                    return this._cachedResult;
                }
                changeAutocompleteLoading(e) {
                    this.autocompleteLoading = e;
                }
                changeAutocompleteNoResults(e) {
                    this.autocompleteNoResults = e;
                }
                autocompleteOnSelect(e) {
                    console.log(`Selected value: ${e.item}`);
                }
            };
            Angular2Autocomplete = __decorate([
                core_1.Component({
                    selector: 'my-app',
                    template: `
                <div class='container-fluid'>
                    <h3>Angular 2 Autocomplete Example</h3>
                    <h4>The Selected Car: {{selectedCar}}</h4>
                    <input [(ngModel)]="selectedCar"
                           [autocomplete]="carsExample2"
                           (autocompleteOnSelect)="autocompleteOnSelect($event)"
                           [autocompleteOptionField]="'name'"
                           class="form-control">

                    <h3>Asynchronous results</h3>
                    <h4>Model: {{asyncSelectedCar}}</h4>
                    <input [(ngModel)]="asyncSelectedCar"
                           [autocomplete]="getAsyncData(getCurrentContext())"
                           (autocompleteLoading)="changeAutocompleteLoading($event)"
                           (autocompleteNoResults)="changeAutocompleteNoResults($event)"
                           (autocompleteOnSelect)="autocompleteOnSelect($event)"
                           [autocompleteOptionsLimit]="7"
                           placeholder="Locations loaded with timeout"
                           class="form-control">
                    <div [hidden]="autocompleteLoading!==true">
                        <i class="glyphicon glyphicon-refresh ng-hide" style=""></i>
                    </div>
                    <div [hidden]="autocompleteNoResults!==true" class="" style="">
                        <i class="glyphicon glyphicon-remove"></i> Empty Query !
                    </div>
                </div>
               `,
                    directives: [AUTOCOMPLETE_DIRECTIVES, common_1.CORE_DIRECTIVES, common_1.FORM_DIRECTIVES],
                }), 
                __metadata('design:paramtypes', [])
            ], Angular2Autocomplete);
            exports_1("Angular2Autocomplete", Angular2Autocomplete);
        }
    }
});
//# sourceMappingURL=autocomplete-example.js.map