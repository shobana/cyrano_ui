System.register([], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var AutocompleteOptions;
    return {
        setters:[],
        execute: function() {
            class AutocompleteOptions {
                constructor(options) {
                    Object.assign(this, options);
                }
            }
            exports_1("AutocompleteOptions", AutocompleteOptions);
        }
    }
});
//# sourceMappingURL=options.class.js.map